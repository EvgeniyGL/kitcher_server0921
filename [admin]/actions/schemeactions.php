<?php
class SchemeActions{
	
	static function defaultGenerator($type,$default=false){
		if ($type=="int"){
			return ((int)$default);
		}elseif ($type=="timestamp" && $default=="now()"){
			return time();
		}elseif ($type=="datetime" && $default=="now()"){
			return date("Y-m-d H:i:s");
		}else{
			return "";
		}
	}
	
	static function add($db,$schemeName){
		$model = $db->$schemeName;
		$mech = new Mech\Mech($schemeName);
		$scheme = $mech->mech;
		
		$data = [];
		if (isset($scheme['fields']['_type'])) {$data['_type']=1;}
		
		/*if (isset($scheme->fields['pid'])){
			if ($paramUrl=="" || is_numeric($paramUrl))	{$data['pid'] =(int)$paramUrl;	}
		}
		
		if (isset($scheme->fields['puid'])){
			$data['puid']= $paramUrl;
		}*/
		
		$req = $_POST;
		//print_r($req);
		
		unset($req['_table']);
		
		
		if ( isset($req['pid']) && !isset($scheme['fields']['pid']) ){
			unset($req['pid']);
		}
		
		
		if (isset($req['id']) && $req['id']==""){
			unset($req['id']);
		}
		
		
		
		
		
		
		
		foreach($req as $key=>$val)
		{
			$type = $scheme['fields'][$key]['type']??"";
			$default = $scheme['fields'][$key]['default']??"";
			
			//if ($val=="" && $default!=""){
			//	$val = defaultGenerator($type,$default);
			//}
			
			if (substr($key,0,3)=='sf-'){
				if ($key=='sf-id')
				{
					//$data['pid'] = (int)$val;
				}
			}elseif(is_array($val)){
				$key = $key.".";
				$val = json_encode($val);
				$data[$key] = $val;
			}else{
				$data[$key] = $val;
			}
			
			
			if (substr($type,0,8)=="datetime"){
				if (substr($val,2,1)=="." && substr($val,5,1)=="." ){
					$dt = explode(" ",$val);
					$d = explode(".",$dt[0]);
					
					$vl = $d[2]."-".$d[1]."-".$d[0];
					
					if (count($dt)==2){
						$t = explode(":",$dt[1]);
						$vl.= " ".$t[0].":".$t[1].":".$t[2];
					}
					
					$data[$key] =$vl;
				}
			}
			elseif ($type=="timestamp"){
				if ($val!="" && !is_numeric($val)){
					$d = DateTime::createFromFormat('d.m.Y H:i:s', $val);
					if ($d === false) {
						$val = 0;
					} else {
						$data[$key] = $d->getTimestamp();
					}
				}
			}
			elseif ($type=="password"){
				if ($val==""){
					$data[$key] = "!";
				}else{
					$data[$key] = password_hash($val,PASSWORD_BCRYPT);
				}
			}
			
			
			if (in_array($type,['int','bigint']) && $val==''){
				$data[$key] = 0;
			}
		}
		
		
		foreach($scheme['fields'] as $k=>$v){
			if ($k!="id" && !isset($data[$k]) && isset($v['default'])){
				
				$data[$k] = self::defaultGenerator($v['type'],$v['default']);
			}
			
			
			$type = $v['type']??"";
			if ($type=='bool'){
				if ($data[$k]=="on"){
					$data[$k] = 1;
				}else{
					$data[$k] = 0;
				}
			}
			
		}
		
		
		
		
		
		
		
		
		//if ($paramUrl=="" || is_numeric($paramUrl))	{$data['pid'] =(int)$paramUrl;	}
		//else										{$data['puid']= $paramUrl;		}
		
		
		$data+=self::loadFiles();
		
		
		if (isset($data['photo_url']) && !isset($data['photo']) && $data['photo_url']!="" ){
			$photoData = file_get_contents($data['photo_url']);
			$photoFile = $data['photo_url'];
			$photoFile = str_replace("https://","",$photoFile);
			$pr = explode("?",$photoFile,2);
			$photoFile = $pr[0];
			$photoFile = str_replace("/","_",$photoFile);
			
			$photoFile = strtolower($photoFile);
			
			$saveFile = date("YmdHis")."_".$photoFile;
			
			$ok = file_put_contents($_SERVER['DOCUMENT_ROOT']."/media/".$saveFile,$photoData);
			
			if (!$ok){
				//print $saveFile;
			}

			$data['photo'] = $saveFile;
		}
		
		//print "<pre>";
		//print_r($data); 
		//print "</pre>";
		
		unset($data['files']);
		
		$id = $model->add($data);
		
		
		if ($id==0){
			print Http\Response::JsonERR([
				'error'=>$model->lastError()
			]);
		}else{
			print Http\Response::JsonOK([
				'id'=>$id
			]);
		}
			
		
		
		/*if ($id==0){
			print "ERROR:".$model->lastError();
		}else{
			$obj = $model->get($id);
			$bid = $obj['pid']??$obj['puid']??"";
			if ($bid==0){$bid="";}
			//header("Location: /".$baseUrl."/".$bid);		die();
			
			if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']!=""){
				$rf = explode("?",$_SERVER['HTTP_REFERER'],2);
				$url = $rf[0];
				$url = str_replace("http://","",$url);
				$url = str_replace("https://","",$url);
				
				$urls = explode("/",$url);
				unset($urls[0]);
				$url = "/".implode("/",$urls);
				
				//header("Location: ".$url);		die();
				
				//print $url;
				
			}else{
				//header("Location: /".$baseUrl."/".$bid);		die();
			}
		}*/
	}
	
	
	static function addfolder($db,$schemeName){
		$model = $db->$schemeName;
		$mech = new Mech\Mech($schemeName);
		$scheme = $mech->mech;
		
		//function actionAddfolder($scheme,$model,$paramUrl,){
		$data = [];
		if (isset($scheme->fields['type'])) {$data['type'] 	= 0 ;}
		
		/*if ($paramUrl==="")
		{
			if (isset($scheme->fields['pid'])) 	{$data['pid'] 	= 0 ;}
			if (isset($scheme->fields['puid'])) {$data['puid'] 	= 0 ;}
		}
		elseif (is_numeric($paramUrl))
		{
			$data['pid'] =(int)$paramUrl;
		}
		else
		{
			$data['puid']= $paramUrl;
		}*/
			
	
		$req = $_POST;
		
		$req['pid'] = (int)$req['pid']??0;
		
		
		
		foreach($req as $key=>$val)
		{
			if (substr($key,0,3)=='sf-')
			{
				if ($key=='sf-id')
				{
					//$data['pid'] = (int)$val;
				}
			}
			elseif(is_array($val))
			{
				$key = $key.".";
				$val = json_encode($val);
				$data[$key] = $val;
			}
			else
			{
				$data[$key] = $val;
			}
		}
	
		
		$id = $model->add($data);

		print json_encode([
			'result'=>'OK',
			'id'=>$id,
		]);		
	}
	
	
	static function setroworder($db,$schemeName){
		//$model = $db->$schemeName;
		//$mech = new Mech\Mech($schemeName);
		//$scheme = $mech->mech;
		
		//$havePid = isset($scheme['fields']['pid']);
		
		//print_r($_POST);
		
		$orderStr = trim($_POST['roworder']);
		$parent_object_id = trim($_POST['parent_object_id']);
		$pair = explode("@",$orderStr);
		
		$table = $pair[0];
		$row_order = $pair[1];

		$ids = explode(" ",$row_order);
		$i=0;
		foreach($ids as $id){
			if ($id!=""){
				$i++;
				$data = [
					'_order'=>$i
				];
				
				/*if ($havePid){
					$data['pid'] = $parent_object_id;
				}else{
					$data['_id'] = $parent_object_id;
				}*/
				
				
				$db->$schemeName->edit($id,$data);
			}
		}
		
		print json_encode([
			'result'=>'OK',
		]);		
	}
	
	static function remove($db,$schemeName){
		//$model = $db->$schemeName;
		//$mech = new Mech\Mech($schemeName);
		//$scheme = $mech->mech;
		
		
		$id = (int)$_GET['id'];
		$db->$schemeName->kill($id);
		print json_encode([
			'result'=>'OK',
		]);	
	}
	
	
	static function randomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	static function xname($name)
	{
		$ext = strtolower(pathinfo($name, PATHINFO_EXTENSION));
		$name = strtolower(pathinfo($name, PATHINFO_BASENAME));
		$n = explode(".",$name);
		
		$name = $n[0];
		
		
		$name = str_replace(" ","_",$name);
		$name = str_replace(".","_",$name);
		$name = str_replace(",","_",$name);
		$name = str_replace(";","_",$name);
		$name = str_replace(":","_",$name);
		$name = str_replace("^","_",$name);
		$name = str_replace("/","_",$name);
		$name = str_replace("\\","_",$name);
		$name = substr($name,0,80);
		$newname = $name."__".self::randomString(5).".".$ext;
		
		return $newname;
	}
	
	static function fileExt($filename){
		$ext="";
		
		if(strpos($filename,".")>0){
			$parts = explode(".",$filename);
			$ext = strtolower($parts[count($parts)-1]);
		}
		
		return $ext;
	}
	
	static function loadFiles()
	{
		$uploads_dir = $_SERVER['DOCUMENT_ROOT'].'/media';
		
		/*$folder="";
		$subdir = date("Ym");	// per folder!	
		if (file_exists($uploads_dir."/".$subdir)){
			$folder=$subdir."/";
		}else{
			if (mkdir($uploads_dir."/".$subdir)){
				$folder=$subdir."/";
			}
		}*/
		
		$folder="";
		
		//print $uploads_dir;
		
		$data=[];
		
		//print "<pre style='background:yellow'>"; 	print_r($_FILES);	print "</pre>";
		
		$videos=[];
		
		foreach($_FILES as $key=>$info)
		{
			if (is_array($info['error']))
			{
				//print "!";
				$ifiles=[];
				$cnt = count($info['error']);
				for($i=0; $i<$cnt; $i++)
				{
					//print "!";
					if ($info['error'][$i]==0) {			
						$newname = $folder.self::xname($info['name'][$i]);
						
						//print "!".$uploads_dir."/".$newname;
						
						if (move_uploaded_file($info['tmp_name'][$i], $uploads_dir."/".$newname))
						{$ifiles[$i]=$newname;}
						else
						{
							print "fail to move file: ".$uploads_dir."/".$newname;
						}
					}
				}
				
				$data[$key]=implode(";",$ifiles);
			}
			else
			{
				if ($info['error']==0) {			
					$newname = $folder.self::xname($info['name']);
					
					//print "<HR>".$uploads_dir."/".$newname."<HR>";
					
					if (move_uploaded_file($info['tmp_name'], $uploads_dir."/".$newname)){
						$data[$key]=$newname;
						$ext = self::fileExt($newname);
						
						if (in_array($ext,["mp4","avi","webm"])){
							$videos[] = $newname;
						}
					}else{
						print "fail to move file: ".$uploads_dir."/".$newname;
					}
				}
			}
			
			
		}
		
		
		if (is_array($videos) && count($videos))
		{
			$ff = shell_exec("whereis ffmpeg");
			$isff=false;
			if (substr($ff,0,7)=="ffmpeg:"){
				$ff = trim(substr($ff,7));
				if (strpos($ff,"/ffmpeg")>0){
					$isff=true;	
				}
			}
			
			
			foreach($videos as $video){
				$vfile = $uploads_dir."/".$video;
				$ifile = $uploads_dir."/".$video."_poster.jpg";
				
				
				$size = "640x360"; //16:9
				
				$cmd = "ffmpeg -i \"{$vfile}\" -qscale:v 10 -ss 00:00:01 -f image2 -s {$size} \"{$ifile}\"";
				//ffmpeg -i video_origine.avi gif_anime.gif
				
				shell_exec($cmd);			
			}
		}
		
		//print "<pre>";
		//print_r($data);
		
		return $data;
	}
	
	
	
	static function edit($db,$schemeName){
		$model = $db->$schemeName;
		
		//print "!!!";
		//print "[ {$schemeName} ]";
		
		$mech = new Mech\Mech($schemeName);
		$scheme = $mech->mech;
	
		$req = $_POST;
		
		//print "<pre>";		print_r($req);		print "</pre><HR>";
		
		if (isset($scheme['subtables'])){
			foreach($scheme['subtables'] as $k=>$v){
				unset($req[$k]);
			}
		}
		
		
		$id = $req['id']??"";
		if ($id!=""){
			unset($req['id']);
		}
	
		$data = [];
		foreach($req as $key=>$val)
		{
			if (substr($key,0,2)!="__")
			{	
				if (substr($key,0,3)=='sf-')
				{
					if ($key=='sf-id')
					{
						//$data['pid'] = (int)$val;
					}
				}
				elseif(is_array($val))
				{
					$fmt = $scheme['fields'][$key];
					if (isset($fmt['ref']) && isset($fmt['multiple']) && $fmt['multiple']==true){
						
						//$data[$key] = implode(";",$val);
						$data[$key] = json_encode($val, JSON_UNESCAPED_UNICODE);
						
					}else{
						//$key = $key.".";
						$val = json_encode($val, JSON_UNESCAPED_UNICODE);
						$data[$key] = $val;
					}
				
				}

				else
				{
					$data[$key] = addslashes($val);
				}
				
				//print "<pre>";
				//print_r($model);
				
				$type = $scheme['fields'][$key]['type']??"";
				//$type = $model->scheme['tablefields'][$key]??"";
				
				if (substr($type,0,8)=="datetime")
				{
					//print $type;
					if (substr($val,2,1)=="." && substr($val,5,1)=="." )
					{
						$dt = explode(" ",$val);
						$d = explode(".",$dt[0]);
						
						$vl = $d[2]."-".$d[1]."-".$d[0];
						
						if (count($dt)==2)
						{
							$t = explode(":",$dt[1]);
							$vl.= " ".$t[0].":".$t[1].":".$t[2];
						}
						
						//print "=".$vl;
						
						$data[$key] =$vl;
					}
				}elseif ($type=="timestamp"){
					
					if ($val!="" && !is_numeric($val)){
						$d = DateTime::createFromFormat('d.m.Y H:i:s', $val);
						if ($d === false) {
							$val = 0;
						} else {
							$data[$key] = $d->getTimestamp();
						}
					}
				}
				elseif ($type=="password"){
					if ($val==""){

					}else{
						$data[$key] = password_hash($val,PASSWORD_BCRYPT);
					}
				}
			
			
			}
		}
		
		
		$data+=self::loadFiles();
			//loadFiles();
		
		
		foreach($scheme['fields'] as $k=>$v){
			if (!in_array($k,["id","type"]) && !isset($data[$k]) && isset($v['default'])){
				//$data[$k] = self::defaultGenerator($v['type'],$v['default']);
			}
			
			
			$type = $v['type']??"";
			if ($type=='bool'){
				$val = $data[$k]??"";
				if ($val=="on"){
					$data[$k] = 1;
				}else{
					$data[$k] = 0;
				}
			}
			
		}
		
		unset($data['_table']);
		unset($data['sf-token']);
		unset($data['_action']);
		
		
		//$id = $_POST['sf-id'];
		//print "<pre>"; 	print_r($data);	print "</pre><HR>";
		
		unset($data['files']);
		
		$k = (int)$model->edit($id,$data);
		
		if ($k==1){
			print Http\Response::JsonOK();
		}else{
			print Http\Response::JsonERR();
		}
	}
	
	
	
	static function save($db,$schemeName){
		$id = $_POST['id']??"";
		if ($id==""){
			self::add($db,$schemeName);
		}else{
			self::edit($db,$schemeName);
		}
	}
	
	static function kill($db,$schemeName){
		$id = (int)$_POST['id']??"";
		$db->$schemeName->kill($id);
		print json_encode([
			'result'=>'OK',
		]);	
	}
	
	static function subtable($db,$schemeName){
		//$model = $db->$schemeName;
		$mech = new Mech\Mech($schemeName);
		$scheme = $mech->mech;
		
		$subtable = $_POST['_table'];
		
		$parent_object_id = $_POST['_id'];
		
		ob_start();
			Widgets\Subtable::table([
					'db'=>$db,
					'scheme'=>$mech->mech,
					'key'=>$subtable,
					'object'=>['id'=>$parent_object_id],
			]);
		$html = ob_get_clean();
		
		print Http\Response::JsonOK([
			'html'=>$html
		]);
		
	}
	
}
?>