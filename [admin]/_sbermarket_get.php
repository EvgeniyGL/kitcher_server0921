<?php
	require "_auth.php";
	
	
	
	
	function getCurrentSession0(){
		$apiUrl = "https://sbermarket.ru/api/shopping_session";
		$json = file_get_contents($apiUrl);
		$data = json_decode($json,true);
		print "<pre>";
		print_r($data);

		return $data['shopping_session']??false;
	}
	
	function getCurrentSession(){
		require "config.php";
		$db = Norm\Norm::init($config);
		
		$user = $db->sysusers->get([
			'sessionid' => session_id(),
		]);	
		
		$userSettings = trim($user['settings']??"");
		
		if ($userSettings!==""){
			$userSettings = str_replace('\"','"',$userSettings);
			
			//print $userSettings;
			
			
			$data = json_decode($userSettings,true);
			
			//print "<HR><pre>";
			//print_r($data);

			return $data['shopping_session']??false;
		}else{
			return false;
		}
	}
	
	
	
	
	
	
	function getActualStore($slug){
		$session = getCurrentSession();
		$availableStores = $session['available_stores']??[];
		
		foreach($availableStores as $store){
			$storeSlug = $store['retailer_slug']??false;
			
			if ($storeSlug==$slug){
				return $store;
			}			
		}
	}

	function getProductByUrl($url){
		
		if (substr($url,0,22)=="https://sbermarket.ru/"){
		
			/*$shopids=[
				"lenta"=>132,
				"metro"=>103,
				"auchan"=>270,
			];*/
			
			$relativePath = str_replace("https://sbermarket.ru/","",$url);
			
			$pair = explode("/",$relativePath,2);
			$shopName = $pair[0];
			$itemName = $pair[1];
			
			//$shopid = $shopids[$shopName];
			$shop = getActualStore($shopName);
			$shopid = $shop['id']??0;

			$apiUrl = "https://sbermarket.ru/api/stores/{$shopid}/products/{$itemName}";
			
			//print $apiUrl;
			//print "<pre>";
			//print_r($shop);
			
			$cfile = str_replace("https://","",$apiUrl);
			$cfile = str_replace("/","~",$cfile);
			$cfile = "goods_cache/".$cfile."__".date("Y-m-d_H-i").".json";
			
			if (!file_exists($cfile)){
				$page = file_get_contents($apiUrl);
				file_put_contents($cfile,$page);
			}
			
			
			$json = file_get_contents($cfile);
			
			$data = json_decode($json,true);
			
			
			
			$name = $data['product']['name'];
			$price = $data['product']['offer']['price'];
			$stock_rate = $data['product']['offer']['stock_rate'];
			$max_stock_rate = $data['product']['offer']['max_stock_rate'];
			$image_url = $data['product']['images']['0']['product_url'];
			
			$stock_state = floor($stock_rate/($max_stock_rate/100));
			
			$r=[
				'product_id'=>$data['product']['offer']['id'],
				'store_id'=>$data['product']['offer']['store_id'],
				'retailer_id'=>$data['product']['offer']['retailer_id'],
				'name'=>$name,
				'price'=>$price,
				'stock_state'=>$stock_state,
				'photo'=>$image_url,
				'shop'=>$shopName,
				'shopid'=>$shopid,
				'packaging'=>$data['product']['offer']['grams_per_unit'], 
				'url'=>$url,
				'json_url'=>$apiUrl,
			];
			
			return $r;
		
		}
	}
	
	
	$url = $_GET['url'];
	
	$product = getProductByUrl($url);
	
	header("Content-type: application/json; charset=utf-8");
	print json_encode($product,JSON_UNESCAPED_UNICODE);
	die();

?>