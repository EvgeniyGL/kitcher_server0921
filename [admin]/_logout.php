<?php
	require_once "config.php";
	$db = Norm\Norm::init($config);
	
	if (session_status() == PHP_SESSION_NONE){
		session_start();
	}
	
	$sessionid = session_id();
	
	$user = $db->sysusers->get([
		'sessionid' => $sessionid,
	]);	
	
	$userId = $user['id'];
	$db->sysusers->edit($userId,[
		'sessionid' => '',
	]);
					
	header("Location: /auth");
	die();

	
	


?>