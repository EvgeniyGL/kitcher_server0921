<?php
	$pageTitle = "All objects";					
?>
<style>
	.panelActions{
		position:fixed;
		width:100%;
		top:0;
		left:0;
		z-index:1;
		background:#fff;
		height:50px;
		padding:0px 20px 0px 20px;
		box-shadow:0 0 6px #ccc; 
		padding-left:72px;
	}
	
	.panelActions table{
		border-collapse: collapse;
		width:100%;
	}
	
	.panelActions table td{
		padding:0;
	}
	
	.panelActions .panelBtn{
		height:50px;
		min-width:50px;
		display:inline-block;
		background:#fff;
		padding:0px 10px 0px 10px;
		color:#777;
		text-decoration:none;
		text-align:center;
		line-height:50px;
		border-left:1px solid #eee;
		user-select: none;
	}
	
	.panelActions .panelBtn:first-child{
		border-left:none;
	}
	
	.panelActions .panelBtn:hover{
		text-decoration:none;
		background:#edf1f5;
		cursor:pointer;
		color:#000;
	}
	
	
	.crudTable{
		width:100%;
		background:white;
	}

	
	.crudTable thead tr th{
		text-align:center;
		background:#f9f9f9;
		border-bottom:1px solid #eee;
		font-size:13px;
		padding:5px;
		border-right:1px solid #eee;
	}
	
	.crudTable thead tr th:last-child{
		border-right:none;
	}
	
	
	.crudTable tbody tr td{
		font-size:13px;
		padding:3px;
		color:#666;
	}
	
	.crudTable tbody tr:hover td{
		background:#f0f0f0;
		color:#333;
		cursor:default;
	}
	
	
	
	
	
	.panelContent{
		margin:0 20px 20px 20px;
	}
	
	
	.crudTable tr td:nth-child(2n) 	{background:#f9f9f9;}
	crudTable tr td:nth-child(2n+1) 	{background:#f9f9f9;}

	crudTable tr th:nth-child(2n) 	{background:#f9f9f9;}
	crudTable tr th:nth-child(2n+1) 	{background:#ffffff;}

	crudTable tr:hover td:nth-child(2n) 	{background:#fff;}
	crudTable tr:hover td:nth-child(2n+1) 	{background:#f9f9f9;}

	crudTable tr th:nth-child(2n) 	{background:#f0f0f0;}
	crudTable tr th:nth-child(2n+1) 	{background:#f9f9f9;}

	.panelBreadcrumbs{
		margin:10px 0 10px 0;
		padding-left:22px;
	}
	
	
	.linkBlock{
		display:inline-block; 
		height:50px;
		width:200px;
		border:1px solid #eeeeee;
		margin:3px;
		padding:5px 20px 5px 20px;	
		color:#555;
		line-height:20px;
	}
	
	.linkBlock:hover{
		background:#EFF8FF;
		color:#333;
	}
	
	.linkBlock i{
		font-size:28px;
		margin-right:5px;
		vertical-align:middle;
	}
	
	.linkBlock div{
		display:inline-block; 
		font-size:14px;
		height:50px;
		margin-top:8px;
	}
	

	

</style>


<div class='panelContent'>

				<div style='background:white;padding:10px;margin-top:20px;'>
				
					<?php
						if ($route->arg1=='add')
						{
							print "add";
						}
						else
						{
							$i=0;
							
							$files = scandir("schemes/");
							$schemes=[];
							
							foreach($files as $file){
								if (substr($file,-5,5)==".mech"){
									$schemeNames = 
									$path = "schemes/".$file;
									if (file_exists($path)){
										//print $path."<BR>";
										$mech = new Mech\Mech($file);
										$schemeGroup=$mech->mech['group']??"default";
										$schemes[$schemeGroup][$mech->mech['scheme']] = $mech->mech;
									}
									
								}
								
							}
							
							
							
							
							
							/* $view=[];
							foreach($schemes as $key=>$val)
							{
								$view[$key] = $key;
							} */
							
							$menuJson=[];
							
							$menuJson[] = [
								'icon'=>"home",
								'link'=>"/",
								'name'=>"Главная",
							];
							
						
	
							foreach($schemes as $groupName=>$group)
							{
								print "<h5 style='font-size:16px;color:gray;'>{$groupName}</h5>";
								print "<div style='padding-left:30px;'>";
								foreach($group as $key=>$row)
								{
									$title = $row['title']??$key;
									
									$icon = $row['icon']??"wysiwyg";
									print "<a href='/{$key}' title='{$key}'>";
										print "<div class='linkBlock'>";
											print "<span class='material-icons' style='font-size:32px;float:left;margin-right:10px;'>{$icon}</span>";
											print " <div>{$title}</div>";
										print "</div>";
									print "</a>";
									
									
									$menuJson[] = [
										'icon'=>$icon,
										'link'=>$key,
										'name'=>$title,
									];
									
									
									$i++;
								}
								print "</div>";
								print "<BR>";
							
							}
							
							if (!file_exists("[admin]/menu.json")){
								$data = json_encode($menuJson,JSON_UNESCAPED_UNICODE);
								
								$data = str_replace("]","\n]",$data);
								$data = str_replace("[","[\n",$data);
								$data = str_replace("{","\t{",$data);
								$data = str_replace("},","},\n",$data);
								$data = str_replace('","','",'."\t\t".'"',$data);
								
								
								file_put_contents("[admin]/menu.json",$data);
							}
							
							
							
						}
					?>
				</div>
				

</div>