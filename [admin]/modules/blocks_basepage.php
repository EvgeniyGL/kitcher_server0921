<?php
	$block = $_GET['block']??"";
?><!doctype html>
<html>
	<head>
		<link href="/assets/bootstrap5/css/bootstrap.min.css" rel="stylesheet">

	</head>
	<body>
		<?php
			if ($block!=""){
				$filename = Path\Path::from(__dir__,"../views/blocks/{$block}.blade.php");
				//$filename = "blocks/{$block}.block.php";
				if (file_exists($filename)){
					include($filename);
				}
			}
		?>
		<script src="/assets/bootstrap5/js/bootstrap.bundle.min.js">
	</body>
	
</html>
