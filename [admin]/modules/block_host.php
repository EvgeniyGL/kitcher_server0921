<?php
	$srldata = $_GET['srldata'];
	$data = unserialize($srldata);
?><!doctype html>
<html>
	<head>
		<link href="/assets/bootstrap5/css/bootstrap.min.css" rel="stylesheet">
		<style>
			.lbe_block_devider{
				background:rgba(0,255,0,0.2);
				text-align:center;
				color:rgba(0,155,0,1);
				font-size:16px;
				font-weight:bold;
				user-select: none;
			}
			
			.lbe_block_devider:hover{
				background:rgba(0,255,0,0.4);
				cursor:help;
			}
		</style>
		<script src="/assets/jquery.min.js"></script>
		<script>
			var currentDevider;
			var currentBlock;
			var constructorMode=false;
			
			function lbe_add(trg){
				currentDevider = $(trg);
				
				$('#lbe_block_selector').html("Loading...");
				$('#lbe_block_selector').fadeIn('fast');
				$.post("block_viewer.php",{},function(data){
					$('#lbe_block_selector').html(data);
				});
			}
		
		
		
		</script>
	</head>
	<body>
		<?php //Array ( [0] => Array ( [block] => justmenu [data] => Array ( [aa] => 111 [bb] => 222 ) ) ) 
			foreach($data as $element){
				$block = $element['block']??"";
				$blockdata = $element['data']??[];
				if ($block!=""){
					$filename = Path\Path::from(__dir__,"../views/blocks/{$block}.blade.php");
					if (file_exists($filename)){
						ob_start();
							include($filename);
						$html = ob_get_clean();
						print Makeover\Makeover::html($html,$blockdata);
					}
				}
			}
		?>
	</body>
</html>
