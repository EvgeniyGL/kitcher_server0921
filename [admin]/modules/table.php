<?php
	require "table_funcs.php";
	
	//$method="";
	
	//print "<pre>";
	//print_r($route);
	//print "</pre>";
	

	
	/*if (strpos($route->arg2,"=")===false){
		$id = $route->arg2;	
		$method = "";
	}else{
		list($id,$method) = explode("=",$route->arg2);	
	}*/
	
	$tableName = $route->arg1??"";
	$id = $route->arg2??"";
	$method = $route->arg3??"";
	
	
	
	
	/*if (isset($scheme['actions'])){
		$response['#tools']="";	
		foreach($scheme['actions'] as $icon=>$link){
			if (strpos($icon,".")===false){
				$response['#tools'].= "<a href='#{$id}/{$link}'><div class='material-icons toolBtn'>{$icon}</div></a>";
			}else{
				$response['#tools'].= "<a href='#{$id}/{$link}'><div class='toolBtn'><img src='{$icon}'></div></a>";	
			}
		}
	}*/
	
	$response['#tools']="";
	
	$schemeTypes = $scheme['types']??false;
	if ($schemeTypes===false){
		$schemeTypes = [
			'0'=>"",
			'1'=>"",
		];
	}
		
		
	foreach($schemeTypes as $key=>$type){
		
		if ($key=='0'){
			$link = "addfolder";
			$icon = "folder_add32.png";
		}elseif ($key=='1'){
			$link = "add";
			$icon = "file_add32.png";
		}else{
			$link = "addtype_".$key;
			$icon = "filetype_add.png";
		}
		
		$response['#tools'].= "<a href='#{$id}/{$link}'><div class='toolBtn'><img src='/assets/icons/{$icon}'></div></a>";	
		
	}
	
	
	
	
	//print $id;
	$currentObject = $model->get($id);
	//$currentObject = [];
	$parentId = $currentObject['pid']??$currentObject['puid']??0;
	
	
	
	//print "ID:{$id}<BR>";
	//print "currentObject:<pre>";
	//print_r($currentObject);
	//print "</pre>";
	
	$search = $_GET['search']??"";
	//$q = $_SERVER['QUERY_STRING'];
	

	//if ( isset($scheme['fields']['pid']) || isset($scheme['fields']['puid']) ){ // древовидная ?
	
		
		if (substr($method,0,8)=="addtype_")	
		{
			$formType = "form_".(int)substr($method,8);
			FormItemType($formType,[],$schemeName,$model,"",$parentId,$scheme,$model,$db);
		}
		elseif ($method=="add")
		{
			printForm("form_add",[],$schemeName,$model,"",$parentId,$scheme,$model,$db);
		}
		elseif ($method=="addfolder")
		{
			printForm("form_addfolder",[]  ,$schemeName,$model,"",$parentId,$scheme,$model,$db);
		}
		elseif ($method=="folder")
		{
			printForm("form_editfolder",$currentObject,$schemeName,$model,"",$parentId,$scheme,$provider??"",$db);
		}
		else
		{
			
			//$view = Snowflake\SfLib::readConfig($model->path.$schemeName.".table.view");
			$view = $scheme['view']['table']??false;
			if (!$view){	
				foreach($scheme['fields'] as $key=>$val){$view[$key] = $key;}
			}
				
				
			if (isset($scheme['fields']['_type'])){ // для древовидных таблиц с типом
				$type = $currentObject['_type']??0;	
			}else{//для плоских таблиц без типа
				if (0==(int)$id){ 
					$type=0;
				}else{
					$type=1;
				}
			}
				
			
			if ($type==0){ //тип:папка
				print "<table class='widgetTable'>";
					//printTableHead($currentObject??[],$view,$baseUrl,$scheme->fields);
					printTableHead($currentObject??[],$view,"",$scheme['fields']);
					$sortable="";
					if (isset($scheme['fields']['_order'])){
						$parent_object_id = $currentObject['pid']??"";
						$sortable="class='sortable_table' table='{$schemeName}' parent_object_id='{$parent_object_id}'";
					}
				print "<tbody {$sortable}>";
				$i=0;
				
				
				$sort = $model->scheme->filesort??"asc";
				
				$filter = [];
				if ($search==""){
					if (isset($scheme['fields']['pid'])){
						$filter = ['pid'=>$id];
					}elseif (isset($scheme['fields']['puid'])){
						if ($id==""){$id=0;}
						$filter = ['puid'=>(string)$id];
					}
				}else{
					if (is_numeric($search)){
						$filter = ['id'=>$search];
					}else{
						$filter = ['name %LIKE%'=>$search];
					}
				}
				
				
				$opts = [];
				if (isset($scheme['fields']['_order'])){$opts['orderby']="_order";}
				
				$generator = $model->find($filter,$opts);
				/*
				,[
					//'filesort' => $sort,
					//'limit'		=> 20,
					//'offset'	=> 20,
				]
				*/
				
				foreach($generator as $row)
				{
					printTableRow($row,$view,$schemeName,$scheme['fields']);
					$i++;
				}
				print "</tbody></table>";
				
				if ($i==0)
				{
					print "<div style='text-align:center;margin-bottom:30px;'>";
						print "<span style='color:gray; font-size:12px;'><BR>";
						print "Этот каталог пуст. <BR><BR> Создать  <a href='#{$id}/addfolder'><img src='/assets/icons/folder32.png' height=20>каталог</a> или <a href='#{$id}/add'><img src='/assets/icons/file32.png' height=20>элемент</a>";
						//print "<BR><BR><a href='#action/{$schemeName}/remove?id={$id}'><img src='/assets/icons/remove32.png' height=20> Удалить</a>";
						if ($id!=""){
							print "<BR><BR><a href='#' onclick='javascript:if (confirm(`Уверены?`)){location.href=`#action/{$schemeName}/remove?id={$id}`;}'><img src='/assets/icons/remove32.png' height=20> Удалить</a>";
						}
						print "</span>";
					print "</div>";
				}
			}elseif ($type==1){	//тип:файл	
				printForm("form_edit",$currentObject,$schemeName,$model,"",$parentId,$scheme,$provider??"",$db);
				
			}else{//тип:custom
				FormItemType("form_".$type,$currentObject,$schemeName,$model,"",$parentId,$scheme,$provider??"",$db);
			}
			
		}
	
	/*}else{	// плоская таблица
		print "flat";
		if ($method=="add")
		{
			printForm("form_add",[],$schemeName,$model,$baseUrl,$parentId,$scheme,$model,$db);
		}
		else
		{
			//$id = $id??0;
			
			if ($id==0)
			{
				//$view = Snowflake\SfLib::readConfig($model->path.$schemeName.".table.view");
				$view = $scheme['view']['table']??false;
				if (!$view){	
					if (is_iterable($scheme['fields']))
					{
						foreach($scheme['fields'] as $key=>$val){$view[$key] = $key;}
					}
				}
					
				$currentObject = $currentObject??[];	
					
				print "<table class='widgetTable'>";
					printTableHead($currentObject,$view,$schemeName,$scheme['fields']);
					
					$sortable="";
					if (isset($scheme['fields']['_order'])){
						$parent_object_id = $currentObject['pid']??"";
						$sortable="class='sortable_table' table='{$schemeName}' parent_object_id='{$parent_object_id}'";
					}

				print "<tbody {$sortable}>";
				$i=0;
				
				
				$sort = $model->scheme->filesort??"asc";
				if ($search=="")
				{
					//$generator = $model->paginatedList(1);
					//$opts = [];
					//if (isset($scheme->fields['_order'])){$opts['orderby']="_order";}
					$generator = $model->find();
				}
				else
				{
					$generator = $model->search($search);
				}
				
				
				
				foreach($generator as $row)
				{
					printTableRow($row,$view,$schemeName,$scheme['fields']);
					$i++;
				}
				
				print "</tbody></table>";
				
				if ($i==0)
				{
					print "<div>";
						print "<span style='color:gray; font-size:12px;'>No records.  <a href='?add'>Add...</a></span>";
					print "</div>";
				}
			}
			else
			{
				printForm("form_edit",$currentObject,$schemeName,$model,$schemeName,$parentId,$scheme,$model,$db);
			}
		}
	}*/
?>