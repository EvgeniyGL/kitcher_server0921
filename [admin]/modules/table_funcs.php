<?php

function printTableHead($obj,$view,$baseUrl,$meta)
{
	//print_r($view);
	
	print "\n<thead>";
		print "\n<tr>";
			if ( ( isset($meta['pid']) || isset($meta['puid']) ) && isset($meta['_type']) )
			{
				print "\n<th width=30 style='background:white;border:none;'>";
					$bid = $obj['pid']??$obj['puid']??"";
					if ($bid!==""){
						if ($bid===0){$bid="";}
						//print "<a href='/{$baseUrl}/{$bid}'><img src='/assets/icons/undo.png' height=16></a>";
						print "<a href='#{$bid}'><img src='/assets/icons/undo.png' height=16></a>";
					}
				print "</th>";
			}
		
			if (isset($view['id'])){
				print "\n<th width=30>";
					print '#';
				print "</th>";
				unset($view['id']);
			}
			elseif (isset($view['uid'])){
				
				unset($view['uid']);
			}
			
			
			if (isset($view['name'])){
				print "<th style='min-width:300px;'>";
					print $view['name']['title']??$view['name'];
				print "</th>";
				unset($view['name']);
			}
			
			if (is_iterable($view))
			{
				foreach($view as $col=>$val)
				{
					print "<th>";
						print $val['title']??$col;
					print "</th>";
				}
			}
			
			//if (isset($meta['order'])){
			//	print "<th width=50><i class='fa fa-sort'></i></th>";
			//}
			
			//if ($id!="" && array_key_exists('_order', $row) ){
			if (isset($meta['_order'])){
				print "<th width=24></th>";
			}
			
			
		print "</tr>";
	print "</thead>";
}








	function printTableRow($row,$view,$schemeName,$meta)
	{		
		$id = $row['id']??$row['uid']??"";
		
		
		//$href = "#".$row['id'];
		$href = "/".$schemeName."#".$row['id'];
		
		$rowId="";
		if ($id!="" && array_key_exists('_order', $row) ){
			$rowId = "_id='{$id}'";
		}

		print "<tr {$rowId}>";
			if (isset($meta['pid']) || isset($meta['puid']))	
			{
				print "<td style='padding:0;text-align:center;'>";
					$bid = $row['id']??$row['uid']??"";
					//print "<a href='/{$baseUrl}/{$bid}'>";
					print "<a href='{$href}'>";
						$rowType = $row['_type']??1;
						if ($rowType=='0')
						{
							print "<img class='rowIcon' src='/assets/icons/folder_closed32.png'>";
						}
						else
						{
							if (isset($row['photo']) && $row['photo']!=""){
								if (in_array(substr($row['photo'],-4,4),[".mp4",".avi","webm"])){
									print "<img class='rowIcon' src='/media/{$row['photo']}_poster.jpg' style='height:50px;'>";	
								}else{
									print "<img class='rowIcon' src='/media/{$row['photo']}' style='height:50px;'>";	
								}
								
							}else{
								if ($rowType==1){
									print "<img class='rowIcon' src='/assets/icons/file32.png'>";	
								}else{
									print "<img class='rowIcon' src='/assets/icons/filetype.png'>";	
								}
							}
						}
					print "</a>";
				print "</td>";
			}
		
			if (isset($view['id'])){
				print "<td width=30 align=center>";
					//print "<a href='/{$baseUrl}/{$row['id']}'>";
					print "<a href='{$href}'>{$row['id']}</a>";
				print "</td>";
				unset($view['id']);
			}
			elseif (isset($view['uid'])){
				
				unset($view['uid']);
			}
			
			if (isset($view['name'])){
				print "<td>";
					
					//print "<a href='/{$baseUrl}/{$id}'>";
					print "<a href='{$href}'>";
						$name = $row['name']??"";
						if ($name==""){$name = $id;}
						print $name;
					print "</a>";
				print "</td>";
				
			}
			
			
			unset($view['name']);
			
			foreach($view as $col=>$name)
			{
				print "<td>";
				
				$colPrts = explode(".",$col);
				if (count($colPrts)==1)
				{
					$colName = $colPrts[0];
					$jsonField = "";
					
				}
				else
				{
					$colName = $colPrts[0].".";
					$jsonField = $colPrts[1];
				}
				
				$val = $row[$colName]??"";
				
				if(is_array($val))
				{
					if ($jsonField=="")
					{
						$t = print_r($val,true);
						$t = str_replace("stdClass Object","",$t);
						

						
						$t = substr($t,8,-3);
						$t = str_replace("[","<B>",$t);
						$t = str_replace("]","</B>",$t);

						print "<table class='microtable'>";
							$j=0;
							foreach(explode("\n",$t) as $line)
							{
								$cols = explode(" => ",$line);
								$ckey = trim($cols[0]??"");
								$cval = trim($cols[1]??"");
								if ($ckey!="" && $ckey!="(" && $ckey!=")" && $cval!="")
								{
									print "<tr>";
										print "<td>{$ckey} = </td>";
										print "<td>{$cval}</td>";
									print "</tr>";
									
									$j++;
									if ($j>10){break;}
								}
							}
						print "</table>";
					}
					else
					{
						print $val[$jsonField]??"_";
					}	
				}
				else
				{
					if (!isset($meta[$col])){
						print "unknown field `{$col}`";
					}else{
						printSpecialValue($val,$meta[$col]);	
					}
					
				}
					
				print "</td>";
			}
			
			/*if (isset($meta['order']))
			{
				print "<td>";
					print "<i class='orderMoverUp fa fa-arrow-up' index={$id}></i>"; 
					print "<i class='orderMoverDown fa fa-arrow-down' index={$id}></i>";
				print "</td>";
			}*/
			
			if ($rowId!=""){
				print "<td>";
					//print "<i class='orderMoverUp fa fa-arrow-up' index={$id}></i>"; 					
					print "<div class='material-icons' style='color:gray;cursor:move;text-align:center;'>open_with</div>";
				print "</td>";
			}
			
			
		print "</tr>";
	}
	
	
	
	
	
	
	
	
	
	
	
	function printSpecialValue($val,$meta)
	{
		$type = $meta['_type']??"";
		
		if (substr($type,0,1)=="$")
		{
			$schemeName = trim(substr($type,1));
			$dict = new Snowflake\SchemeModel($app,$schemeName);
			$dictObj = $dict->get($val);
			
			print "<a href='/scheme/{$schemeName}/{$val}'>";
				print $dictObj['name']??"";
			print "</a>";
		}
		elseif (substr($type,0,1)=="*")
		{
			$items = explode(",",$val);
			$cm="";
			foreach($items as $item)
			{
				//print $item;
				$kv = explode("#",$item);
				
				$id = $kv[1];
				$model = $kv[0];
			
				$dict = new Snowflake\SchemeModel($app,$model);
				$dictObj = $dict->get($id);
				
				print $cm;
				print "<a href='/scheme/{$model}/{$id}'>";
					print $dictObj['name']??"";
				print "</a>";
				$cm=", ";
			}
		}
		elseif ($type=="timestamp" || substr($type,0,4)=="date") // date,datetime,timestamp
		{
			if ($val!=""){
				if ($type=="timestamp"){
					$val = date('Y-m-d H:i:s', $val);
				}
				
				$dt = explode(" ",$val);
			
				if (count($dt)==1)
				{
					$date = $dt[0];
					$time = "";
				}
				else
				{
					$date = $dt[0];
					$time = $dt[1];
				}
				
				print "<div class='datetime' title='{$val}'>";
					if ($date == date("Y-m-d"))
					{
						print "today";
					}
					else
					{
						$months = ['','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
						$d = explode("-",$date);
						if (count($d)>1)
						{
							$m = (int)$d[1];
							print $d[2]." ".$months[$m]." ".$d[0];
						}
					}
					
					if ($time!="")
					{
						$d = explode(":",$time);
						print "<span>";
							print " ".$d[0].":".$d[1];
						print "</span>";
					}
				
				
				
				print "</div>";
			
			}
			
			
		}
		elseif ($type=="file")
		{
			$path = "/media/".$val;
			print "<a href='{$path}'>";
			$ext = strtolower(pathinfo($val, PATHINFO_EXTENSION));
			if (in_array($ext,[ 'jpg','jpeg','gif','png','webp' ]))
			{
				print "<img class='tableImage' src='{$path}' title='{$val}'>";
			}
			else
			{
				print $val;
			}
			print "</a>";
		}
		elseif ($type=="coordinates")
		{
			if (is_object($val))
			{
				print $val->x." , ".$val->y;
			}
			elseif (is_array($val))
			{
				print $val['x']." , ".$val['y'];
			}
			else
			{
				print_r($val);
			}
		}
		else
		{
			if (is_object($val)){
				print_r($val);
			}else{
				if (function_exists('mb_substr'))
				{
					print mb_substr(strip_tags(str_replace("<"," <",$val)),0,100);
				}
				else
				{
					print substr(strip_tags(str_replace("<"," <",$val)),0,100);
				}
			}
			
			
			
		}
	}
	
	
	
	
	
	function printTabCol($tabRow,$scheme,$obj,$key,$model,$db,$formType){
		
		
		if (!isset($scheme['fields'][$key]) && isset($scheme->external[$key])){
			print "==";
			if (((int)$obj['id'])==0){
				print "Редактирование данных станет доступным после сохранения документа";
			}else{
				print "<iframe style='border:0px solid red; width:100%; min-height:600px; height:100%;' src='/ischeme/{$key}/{$obj['id']}/{$scheme->external[$key]['relation']}'></iframe>";
			}
		}else{
			$colTitle = $tabRow['title']??$key;
			$skv = explode(".",$key);
			if (count($skv)==1){
				$scKey = $key;
			}else{
				$scKey = $skv[0].".";
			}
			
			$val = $obj[$scKey]??"";
			if (substr($key,0,1)=="@")
			{
				$docid = $obj['id']??0;
				//print Snowflake\Widget::component(substr($key,1), $colTitle , $docid, $model,$tabRow);
			}
			else
			{
				printFormField($key,$val,$model,$scheme,[
					'title'=>$colTitle,
					'fullname'=>$key,
				],$formType,$db,$obj);
			}
		}
		
	}
	
	function printRightBar($rightBar,$rightBarWidth,$scheme,$obj,$model,$db,$formType){
		print "<td width='{$rightBarWidth}%'>";
			$ii=0;
			foreach($rightBar as $key=>$tabRow)
			{
				$ii++;
				if (!isset($scheme['fields'][$key]) && isset($scheme->external[$key])){
					if (((int)$obj['id'])==0){
						print "Редактирование данных станет доступным после сохранения документа";
					}else{
						print "<iframe style='border:0px solid red; width:100%; min-height:600px; height:100%;' src='/ischeme/{$key}/{$obj['id']}/{$scheme->external[$key]['relation']}'></iframe>";
					}
				}else{
					$colTitle = $tabRow['title']??$key;
					$skv = explode(".",$key);
					if (count($skv)==1){
						$scKey = $key;
					}else{
						$scKey = $skv[0].".";
					}
					
					$val = $obj[$scKey]??"";
					if (substr($key,0,1)=="@")
					{
						$docid = $obj['id']??0;
						//print Snowflake\Widget::component(substr($key,1), $colTitle , $docid, $model,$tabRow);
					}
					else
					{
						printFormField($key,$val,$model,$scheme,[
							'title'=>$colTitle,
							'fullname'=>$key,
						],$formType,$db,$obj);
					}
				}
			}
		print "</td>";
	}
	
	
	
	function printTab($tabName,$tabCols,$scheme,$obj,$model,$db,$formType){
		$rightBar=[];
		$rightBarWidth=0;
		
		$colview = [];
		foreach($tabCols as $f=>$mt){
			if (isset($mt['col'])){
				if ($mt['col']=="rightBar"){
					$rightBar[$f] = $mt;	
					$rightBarWidth=20;
				}else{
					$colview[$mt['col']][$f] = $mt;	
				}
			}
		}
		
		if (count($colview)>0){
			$cview = $colview;
		}else{
			$cview = [];
			$cview['...'] = $tabCols;
		}
		
		print "<div class='tabPage' name='{$tabName}'>";
		
			print "<table class='pageTable'><tr>";
				$tdWidth = "width='".((100-$rightBarWidth)/count($cview))."%'";
				foreach($cview as $colKey=>$tabCol)
				{
					print "<td {$tdWidth}>";
						foreach($tabCol as $key=>$tabRow){
							printTabCol($tabRow,$scheme,$obj,$key,$model,$db,$formType);
						}
					print "</td>";
				}
				
				if (count($rightBar)>0){
					printRightBar($rightBar,$rightBarWidth,$scheme,$obj,$model,$db,$formType);
				}
				
			print "</tr></table>";
		print "</div>";
	
	
	
	}
	
	
	function printForm($formType,$obj,$schemeName,$model,$baseUrl,$parentId,$scheme,$provider,$db)
	{
		//print $formType;
		//		Sf\Widget::setDbProvider($db);
		//$view = Snowflake\SfLib::readFormView($model->path.$schemeName.".{$formType}.view");
		
		if (strpos($formType,"folder")>0){
			$view = $scheme['view'][$formType]??$scheme['view']["form_folder"]??$scheme['view']['form']??false;
		}else{
			$view = $scheme['view'][$formType]??$scheme['view']['form']??false;
		}
		
		$tabviews = [];
		if ($view){
			foreach($view as $f=>$mt){
				$tabName = $mt['tab']??"default";
				if ($tabName==""){$tabName="default";}
				$tabviews[$tabName][$f] = $mt;
			}
		}
		
		print "<form class='sfForm' method='POST' enctype='multipart/form-data'>";
			print "<input type='hidden' name='sf-token' value='123'>";
			//if (isset($obj['id']))
			//{		
				$sfid = $obj['id']??$obj['uid']??"";
				print "<input type='hidden' name='sf-id' value='{$sfid}'>";
			//}
			
			
			if ($formType=='form_addfolder' || $formType=='form_editfolder')
			{
				$folderview = $scheme->view[$formType]??$scheme->view['form_folder']??false;
				if (!$folderview){
					$folderview = [];
					$folderview['name'] = ['title'=>'Название'];
				}
			
				foreach($folderview as $kk=>$vv){
					//$meta = $scheme->fields[$kk];
					//print_r($meta+$vv);
					$val= $obj[$kk]??"";
					
					printFormField($kk,$val,$model,$scheme,[
						'title'=>$vv['title']??$kk,
						'fullname'=>$kk,
					],$formType,$db,$obj);
				}
			}
			else
			{
				if ($view==false){
					foreach($scheme['fields'] as $key=>$meta){
						$default = $meta['default']??"";					
						if (!in_array($key,['id'])){
							$val= $obj[$key]??$default;
							printFormField($key,$val,$model,$scheme,['addclass' => 'simpleFormField'],$formType,$db,$obj);
						}
					}
				}else{		
					
					
					if (count($tabviews)>0){
						$tabs = $tabviews;
					}else{
						$tabs = [];
						$tabs['default'] = $view;
					}
					
					//print "<pre>";	print_r($tabs);	print "</pre>";
					
					if (isset($tabs['default'])){
						
						printTab('default',$tabs['default'],$scheme,$obj,$model,$db,$formType);
						
						unset($tabs['default']);
					}
					
					print "<div class='tabset' style='margin-top:20px;'>";
					
					foreach($tabs as $tabName=>$tabCols)
					{
						printTab($tabName,$tabCols,$scheme,$obj,$model,$db,$formType);
					}
					
					print "</div>";
						
				}
				
				
			}

	
			
			print "<div class='sfFormControls'>";
				print "<table width=100%><tr><td>";
					//print "<input type='submit' class='btn btn-success' value='Сохранить и выйти'> ";
					print "<input type='button' class='btn btn-success' onclick='AjaxFormSubmit(this)' value='Сохранить'>";
					$ppid = $obj['pid']??$obj['puid']??"";
					if ($ppid==0)
					{
						//print "<a href='/{$baseUrl}/' class='btn btn-light'>Отмена</a>";
						print "<a href='#' class='btn btn-light'>Отмена</a>";
					}
					else
					{
						//print "<a href='/{$baseUrl}/{$ppid}' class='btn btn-light'>Отмена</a>";
						print "<a href='#' class='btn btn-light'>Отмена</a>";
					}
					
				print "</td><td align=right>";	
					$id = $obj['id']??"";
					if ($id!=""){
						print "<input type='button' class='btn btn-danger' value='Удалить' onclick='javascript:if (confirm(`Уверены?`)){location.href=`#action/{$schemeName}/remove?id={$id}`;}'>";
					}
				print "</td></tr></table>";
			print "</div>";
		print "</form>";
		
		
		
	}	
	
	
	function FormItemType($formType,$obj,$schemeName,$model,$baseUrl,$parentId,$scheme,$provider,$db)
	{	//Widgets\Webbuilder::blocks([]);
	
		$formTypeIndex = (int)str_replace("form_","",$formType);
	
		$view = $scheme['view'][$formType]??false;
		if ($view===false){
			print "NO SUCH FORM IN SCHEME: ".$formType;
		}else{
			
			$usedTabs = [];
			foreach($view as $field=>$meta){	// пробегаемся по всем полям, чтобы узнать какие табы используются
				$tabName = trim($meta['tab']??"");
				if ($tabName==""){$tabName="default_tab";}
				$usedTabs[$tabName][$field] = $meta;
			}
			
			if (count($usedTabs)>0)	{	$tabs = $usedTabs;					}
			else					{	$tabs = [ 'default_tab'=>$view	];	}

			print "<form class='sfForm' method='POST' enctype='multipart/form-data'>";
				print "<input type='hidden' name='sf-token' value='123'>";
				print "<input type='hidden' name='_type' value='{$formTypeIndex}'>";
				$sfid = $obj['id']??$obj['uid']??"";
				print "<input type='hidden' name='sf-id' value='{$sfid}'>";
				
				
				if (isset($tabs['default_tab'])){
					print "<div class='default_tab'>";
					$usedColumns = [];
					foreach($tabs['default_tab'] as $field=>$meta){ // пробегаемся по всем полям, чтобы узнать какие колонки используются
						if (isset($meta['col'])){
							if ($meta['col']=="rightBar"){
								$rightBar[$field] = $meta;	
								$rightBarWidth=20;
							}else{
								$usedColumns[$meta['col']][$field] = $meta;	
							}
						}
					}
					
					if (count($usedColumns)>0)	{	$columns = $usedColumns;			}
					else						{	$columns = ['...'=>$tabColumns];	}
					
					$rightBar=[];
					$rightBarWidth=0;
					
					print "<table class='pageTable'><tr>";
							$tdWidth = "width='".((100-$rightBarWidth)/count($columns))."%'";
							foreach($columns as $colKey=>$tabCol)
							{
								print "<td {$tdWidth}>";
									foreach($tabCol as $key=>$tabRow){
										if (!isset($scheme['fields'][$key]) && isset($scheme['external'][$key])){
											if (((int)$obj['id'])==0){
												print "Редактирование данных станет доступным после сохранения документа";
											}else{
												print "<iframe style='border:0px solid red; width:100%; min-height:600px; height:100%;' src='/ischeme/{$key}/{$obj['id']}/{$scheme['external'][$key]['relation']}'></iframe>";
											}
										}else{
											if (strpos($key,".")===false){ 	// если поле не составное
												$realKey = $key;
											}else{							// если поле составное
												$realKey = explode(".",$key)[0].".";
											}
											
											
											$val = $obj[$realKey]??"";
											printFormField($key,$val,$model,$scheme,[
												'title'=>$tabRow['title']??$key,
												'fullname'=>$key,
											],$formType,$db,$obj);
											
										}
									}
								print "</td>";
							}
							

							if (count($rightBar)>0){
								print "<td width='{$rightBarWidth}%'>";
									foreach($rightBar as $key=>$tabRow)
									{
										if (!isset($scheme['fields'][$key]) && isset($scheme['external'][$key])){
											if (((int)$obj['id'])==0){
												print "Редактирование данных станет доступным после сохранения документа";
											}else{
												print "<iframe style='border:0px solid red; width:100%; min-height:600px; height:100%;' src='/ischeme/{$key}/{$obj['id']}/{$scheme['external'][$key]['relation']}'></iframe>";
											}
										}else{
											if (strpos($key,".")===false){ 	// если поле не составное
												$realKey = $key;
											}else{							// если поле составное
												$realKey = explode(".",$key)[0].".";
											}
											
											$val = $obj[$realKey]??"";
											printFormField($key,$val,$model,$scheme,[
												'title'=>$tabRow['title']??$key,
												'fullname'=>$key,
											],$formType,$db,$obj);
										}
									}
								print "</td>";
							}
							
						print "</tr></table>";
						print "</div>";
					
					unset($tabs['default_tab']);
				}
				
				
				
				print "<div class='tabset'>";
				
				foreach($tabs as $tabName=>$tabColumns){
					$rightBar=[];
					$rightBarWidth=0;

					$usedColumns = [];
					foreach($tabColumns as $field=>$meta){ // пробегаемся по всем полям, чтобы узнать какие колонки используются
						if (isset($meta['col'])){
							if ($meta['col']=="rightBar"){
								$rightBar[$field] = $meta;	
								$rightBarWidth=20;
							}else{
								$usedColumns[$meta['col']][$field] = $meta;	
							}
						}
					}
					
					if (count($usedColumns)>0)	{	$columns = $usedColumns;			}
					else						{	$columns = ['...'=>$tabColumns];	}
					
					
					
					print "<div class='tabPage' name='{$tabName}'>";
					
						print "<table class='pageTable'><tr>";
						
							$tdWidth = "width='".((100-$rightBarWidth)/count($columns))."%'";
							foreach($columns as $colKey=>$tabCol)
							{
								print "<td {$tdWidth}>";
									foreach($tabCol as $key=>$tabRow){
										if (!isset($scheme['fields'][$key]) && isset($scheme['external'][$key])){
											if (((int)$obj['id'])==0){
												print "Редактирование данных станет доступным после сохранения документа";
											}else{
												print "<iframe style='border:0px solid red; width:100%; min-height:600px; height:100%;' src='/ischeme/{$key}/{$obj['id']}/{$scheme['external'][$key]['relation']}'></iframe>";
											}
										}else{
											if (strpos($key,".")===false){ 	// если поле не составное
												$realKey = $key;
											}else{							// если поле составное
												$realKey = explode(".",$key)[0].".";
											}
											
											
											$val = $obj[$realKey]??"";
											
											printFormField($key,$val,$model,$scheme,[
												'title'=>$tabRow['title']??$key,
												'fullname'=>$key,
											],$formType,$db,$obj);
											
										}
									}
								print "</td>";
							}
							

							if (count($rightBar)>0){
								print "<td width='{$rightBarWidth}%'>";
									foreach($rightBar as $key=>$tabRow)
									{
										if (!isset($scheme['fields'][$key]) && isset($scheme['external'][$key])){
											if (((int)$obj['id'])==0){
												print "Редактирование данных станет доступным после сохранения документа";
											}else{
												print "<iframe style='border:0px solid red; width:100%; min-height:600px; height:100%;' src='/ischeme/{$key}/{$obj['id']}/{$scheme['external'][$key]['relation']}'></iframe>";
											}
										}else{
											if (strpos($key,".")===false){ 	// если поле не составное
												$realKey = $key;
											}else{							// если поле составное
												$realKey = explode(".",$key)[0].".";
											}
											
											$val = $obj[$realKey]??"";
											printFormField($key,$val,$model,$scheme,[
												'title'=>$tabRow['title']??$key,
												'fullname'=>$key,
											],$formType,$db,$obj);
										}
									}
								print "</td>";
							}
							
						print "</tr></table>";
					print "</div>";
				}
				
				
				
				//subtables
			
			
				/*if (isset($scheme['subtables']) && is_array($scheme['subtables']) && count($scheme['subtables'])>0){
					$hiddenFields = ['id','doc_id','author'];
					$readOnlyFields = ['updated'];
					
					foreach($scheme['subtables'] as $sTable=>$sMeta){
						$subtblname = $schemeName."_".$sTable;
						
						print "<div class='tabPage' name='{$sMeta['title']}'>";
						
						if (isset($obj['id']))
						{
							$subview = $sMeta['view']??"table";
							
							
							if ($subview=="documents"){
								
								$doc_id = $obj['id']??$obj['uid']??"";
								
								print "<div id='sub_doc_list'>";
								
									$gen = $db->$subtblname->find([
										'_id'=>$obj['id']
									],[
										'orderby'=>"_order"
									]);
									print "<span id='subdocument_list' class='sortable' _table='{$subtblname}'>";
									foreach($gen as $row){
										print "<div class='sub_document' id='{$subtblname}{$row['id']}' _id='{$row['id']}'>";
											$btitle = $row['name']??$row['_id']??$row['id'];
											
											print "<div style='background:#eee;padding:3px;text-overflow:ellipsis;'>";
												print "<table cellspacing=0 cellpadding=0 width=100%><tr>";
													print "<td><div class=''>{$btitle}</div></td>";
													print "<td width=1 align=right><i id='{$subtblname}{$row['id']}_applyBtn' class='fa fa-floppy-o' style='cursor:pointer;display:none;color:blue;' onclick='applySubdocument(\"{$subtblname}\",{$row['id']},{$obj['id']})'></i></td>";
													print "<td width=1 align=right><i id='{$subtblname}{$row['id']}_editBtn' class='fa fa-edit' style='cursor:pointer;' onclick='editSubdocument(\"{$subtblname}\",{$row['id']},{$obj['id']})'></i></td>";
													print "<td width=1 align=right><i class='fa fa-times' style='cursor:pointer;' onclick='killSubdocument(\"{$subtblname}\",{$row['id']},{$obj['id']})'></i></td>";
												print "</tr></table>";
											print "</div>";
											
											//print "<div>";
											foreach($row as $rkey=>$rdata){
												if ($rkey!="name" && isset($sMeta['fields'][$rkey]['title'])){
													$title = $sMeta['fields'][$rkey]['title'];
													$type = $sMeta['fields'][$rkey]['_type'];
													
													//$fMeta['class'] = "field__{$subtblname}";
													//$fMeta['name'] = "";
													print Mainway\Widget::autoView($subtblname.".".$rkey,$rdata,$sMeta['fields'][$rkey],$db);
													
													
													//if ($type==)
													//print $title."=".$rdata."<BR>";
												}
											}
											//print "</div>";
										
										print "</div>";
									}
									print "</span>";
																	
								
									print "<div class='sub_document' id='sub_doc_editor' style='padding:5px;'>";
										
										foreach($sMeta['fields'] as $fld=>$fMeta){
											if (!in_array($fld,$hiddenFields)){
												if ($fld=="_order"){$fld="";}
												
												if (isset($fMeta['title'])){
													$fMeta['class'] = "field__{$subtblname}";
													$fMeta['name'] = "";
													print Mainway\Widget::auto($subtblname.".".$fld,"",$fMeta,$db);
												}
												
											}
										}
										print "<input id='btnSaveSubdocument' type='button' class='btn btn-primary' value='Добавить' onclick='saveSubdocument(\"{$subtblname}\",\"{$doc_id}\")'>";
									print "</div>";
								
								print "</div>";
								
								
								
								
							}else{
								print "<table class='sTable'>";
									print "<thead><tr>";
										foreach($sMeta['fields'] as $fld=>$fMeta){
											if (!in_array($fld,$hiddenFields)){
												if ($fld=="_order"){$fld="#";}
												
												$th = $fMeta['title']??$fld;
												
												print "<th>{$th}</th>";
											}
										}
										
										print "<th width=1></th>";
									print "</tr></thead><tbody class='sortable' _table='{$subtblname}'>";
									
									
									$gen = $db->$subtblname->find([
										'_id'=>$obj['id']
									]);
									foreach($gen as $row){
										print "<tr id='{$subtblname}{$row['id']}' _id='{$row['id']}'>";
											foreach($row as $rkey=>$rdata){
												if (!in_array($rkey,$hiddenFields)){
													print "<td>";
													if (is_array($rdata)){
														$ddata = refArrayDecode($rdata);
														print "<a href='/scheme/{$ddata['$']}/{$ddata['id']}'>{$ddata['name']}</a>";										
													}else{
														print $rdata;
													}
													print "</td>";
												}
											}
											print "<td>";
												print "<input type='button' class='btn btn-danger btn-sm'  style='padding:0 10px 2px 10px;' value='Удалить' onclick='killSubdocument(\"{$subtblname}\",{$row['id']},{$obj['id']})'>";
											print "</td>";
										print "</tr>";
									}
									
									print "</tbody><tfoot>";
									
								
									print "<tr id='{$subtblname}_addform'>";
										
										print "<form method='POST' action='?addSubtableRow'>";
										
										$_id = $obj['id']??$obj['uid']??"";
										
										print "<input class='field__{$subtblname}' type='hidden' name='__sf-token' value='sf-token'>";
										print "<input class='field__{$subtblname}' type='hidden' name='__action' value='addSubtableRow'>";
										print "<input class='field__{$subtblname}' type='hidden' name='__table' value='{$subtblname}'>";
										print "<input class='field__{$subtblname}' type='hidden' name='__doc_id' value='{$_id}'>";
										
									
										foreach($sMeta['fields'] as $fld=>$fMeta){
											if (!in_array($fld,$hiddenFields)){
												if ($fld=="doc_order"){$fld="";}
												print "<td>";
												
												if (!in_array($fld,$readOnlyFields) && $fld!=""){
													$fMeta['class'] = "field__{$subtblname}";
													
													print Mainway\Widget::auto("__".$fld,"",$fMeta,$db);
												}
												
												print "</td>";
											}
										}
										
										//print "<td><input type='submit' class='btn btn-success btn-sm' value='Добавить'></td>";
										print "<td><input type='button' class='btn btn-success btn-sm' style='padding:0 10px 2px 10px;' value='Добавить' onclick='saveSubtableLine(\"{$subtblname}\")'></td>";
										
										
										print "</form>";
										
									print "</tr>";
									
									
									
									
								
								
								print "</tfoot></table>";
							}
						
						}else{
							print "<div style='text-align:center; color:gray'>Редактирование этих данных станет доступным только после сохранения текущего документа...</div>";
						}
						
						print "</div>";
					}
				}*/
				
				
				//subtables END
				
				
				
				
				
				print "</div>";
			
				
				

		
				
				print "<div class='sfFormControls'>";
					print "<table width=100%><tr><td>";
						//print "<input type='submit' class='btn btn-success' value='Сохранить и выйти'> ";
						print "<input type='button' class='btn btn-success' onclick='AjaxFormSubmit(this)' value='Сохранить'>";
						$ppid = $obj['pid']??$obj['puid']??"";
						if ($ppid==0)
						{
							//print "<a href='/{$baseUrl}/' class='btn btn-light'>Отмена</a>";
							print "<a href='#' class='btn btn-light'>Отмена</a>";
						}
						else
						{
							//print "<a href='/{$baseUrl}/{$ppid}' class='btn btn-light'>Отмена</a>";
							print "<a href='#' class='btn btn-light'>Отмена</a>";
						}
						
					print "</td><td align=right>";	
						$id = $obj['id']??"";
						if ($id!=""){
							print "<input type='button' class='btn btn-danger' value='Удалить' onclick='javascript:if (confirm(`Уверены?`)){location.href=`#action/{$schemeName}/remove?id={$id}`;}'>";
						}
					print "</td></tr></table>";
				print "</div>";
			print "</form>";
			
		}
	}
	
	
	function decodeReference(array $obj){
		$arr = [];
		$ref = "";
		
		foreach($obj as $k=>$v){
			$pair = explode(".",$k,2);
			if ($ref==""){
				$ref = $pair[0];
			}
			
			$arr[$pair[1]] = $v;
		}
		
		return (object)[
			'ref'=>$ref,
			'data'=>$arr
		];
	}
	
	function printFormField($key,$val,$model,$scheme,$data=[],$formtype="",$db=null,$context=[])
	{
		
		//print "<pre>";
		//print_r($scheme);
		
		$data['title'] = $data['title']??$key;
		$addClass = $data['addclass']??"";
		
		
		
		if (is_array($val)){
			//var_dump($val);
			/*if (strpos($key,".")>0){		
				$kkv = explode(".",$key);
				$value = $val[$kkv[1]]??"";
			}else{
				$value = print_r($val,true);
			}*/
			$refObj = decodeReference($val);
			$name = $refObj->data['name']??$refObj->data['id'];
			$id = $refObj->data['id']??"";
			
			$value = $id;
		}else{
			$value = $val;
		}
		
		//print_r($value);
			
		$scKey = $key;
		$skv = explode(".",$key);
		if (count($skv)==1){
			$scKey = $key;
			$schemeKey = $key;
		}else{
			$scKey = $skv[0].".";
			$schemeKey = $skv[0];
		}
			
		$type = $scheme['fields'][$schemeKey]['type']??"";
		
		if ($type==""){
			
			$widget = $scheme['view'][$formtype][$schemeKey]['widget']??"";
			if ($widget=="" && substr($formtype,0,5)=="form_"){
				$widget = $scheme['view']['form'][$schemeKey]['widget']??"";
			}
			
			if ($widget!=""){
				
				print "\n<div class='tabCol col {$addClass}'>";
					$widgetClass = "";
					$widgetName = "";
					if (strpos($widget,".")===false){
						$widgetClass = "Common";
						$widgetName = $widget;
					}else{
						list($widgetClass,$widgetName) = explode(".",$widget);
					}
					$cls = "Widgets\\".ucfirst($widgetClass);
					if (method_exists($cls,$widgetName)){
						//print $cls."::".$widgetName;
						print $cls::$widgetName([
							'db'=>$db,
							'key'=>$key,
							'value'=>$value,
							'scheme'=>$scheme,
							'object'=>$context,
						]);
						
					}else{
						print "Widget not found: {$cls}::{$widgetName}()";
					}
				print "</div>\n";
			}
			
		}else{
			
			$fMeta = $scheme['fields'][$schemeKey];
			$view = $scheme['view'][$formtype]??false;
			if ($view===false && substr($formtype,0,5)=="form_"){
				$view = $scheme['view']['form']??false;
				if ($view!==false){
					$fMeta += $view[$schemeKey];
				}
			}
			
			
			
			print "\n<div class='tabCol col {$addClass}'>";
				if ($fMeta['type']!="bool"){
					print "<div class='formFieldLabel'>{$data['title']}</div>";
				}
				//print_r($fMeta);
				
				//if (!isset($fMeta['key'])){
				//	$fMeta['key'] = $schemeKey;
				//}
				
				//print "<div class='formField'>".SF\Widget::byType($key,$fMeta,$value,$data)."</div>";
				
				
				if (isset($fMeta['set']) && is_array($fMeta['set'])){
					$type = "set";
				}else{
					$type = $fMeta['widget']??$fMeta['type']??"string";
				}
				
				//print_r($type);
				$fMeta['field'] = $schemeKey;
				
				print "<div class='formField'>".Widgets\Widget::byType($type,$fMeta,[
					'value'=>$value
				],[
					'db'=>$db
				])."</div>";
				
				
			print "</div>\n";			
		}
	}


?>