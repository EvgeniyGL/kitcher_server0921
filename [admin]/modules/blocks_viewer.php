<?php
	$path = Path\Path::from(__dir__,"../views/blocks");
	$files = scandir($path);
	foreach($files as $file){
		if (substr($file,-10,10)==".blade.php"){
			$block_name = substr($file,0,-10);
			print "<div class='iframe-wrap blockItem' block='{$block_name}'>";
				print "<h5>{$block_name}</h5>";
				print "<iframe class='lbe_block_frame' src='rpc/blocks_basepage?plain=1&block={$block_name}'></iframe>";
			print "</div>";
		}
	}
?>