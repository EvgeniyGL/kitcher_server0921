<div style='min-height:82px;'></div>
<header class='moblock' style='border-bottom:1px solid #dee2e6;padding-bottom:10px; position:fixed;top:0;z-index:10000;width:100%;background:rgba(255,255,255,0.95);'>
	<div class="container" style='margin-top:15px;'>
		<div class="row">
			<div class="col-sm-12 col-md-6 col-lg-6 col-xl-2 order-sm-1 order-md-1 order-lg-1 order-xl-1" style='display:table;'>
				<div style='display:table-cell;vertical-align:middle;'>
					<img src='/[website]/logo.png' style='max-width:100%;'>
				</div>
				<div style='display:table-cell;width:50px;' class="d-md-none">
					<img src='/assets/icons/menu.png' style='width:100%;cursor:pointer;' onclick='document.getElementById("jstMenu").classList.add("fullScreenMenu");'>
				</div>
			</div>
		   
			<div id='jstMenu' class="col-sm-12 col-md-12 col-lg-12 col-xl-7 order-sm-3  order-md-3 order-lg-3 order-xl-2 d-none d-md-block">
				<div id='jstMenuCloseBtn'>
					<i class="fa fa-times fa-5x" onclick='document.getElementById("jstMenu").classList.remove("fullScreenMenu");'></i>
				</div>
				<nav class='navbar navbar-expand navbar-light'>
					<ul class='navbar-nav' style='width:100%; display:flex; justify-content:space-around;'>
						<li><a class='nav-link' href='/'>Главная</a></li>
						<li><a class='nav-link' href='/objects'>Объекты</a></li>
						<li><a class='nav-link' href='/commerce'>Коммерция</a></li>
						<li><a class='nav-link' href='/docs'>Документация</a></li>
						<li><a class='nav-link' href='/forbuyers'>Покупателям</a></li>
						<li><a class='nav-link' href='/rent'>Аренда</a></li>
						<li><a class='nav-link' href='/contacts'>Контакты</a></li>
					</ul>
				</nav>
			</div>
		   
			<div class="col-sm-5  col-md-6 col-lg-6 col-xl-3 order-sm-2 order-md-2 order-lg-2 order-xl-3 d-none d-md-block" style='text-align:right;'>
					
				<a href='tel:+79991234567' class="pulse-button">
					+7(999) 123-45-67				
					<span></span>
				</a>
									
			</div>
		</div>
	</div>
</header>