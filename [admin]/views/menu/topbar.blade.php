@push('style')

.menu-topbar{
	background:white; 
	position:fixed; 
	left:0;
	top:0;
	right:0;
	height:45px;
	z-index:10000;
	box-shadow: 0 0 4px #9ac;
	padding-left: 50px;
}

.menu-topbar .tools{
	padding-top:2px;
	padding-left:30px;
	user-select:none;
}

.menu-topbar .toolBtn{
	cursor:pointer;
	display:inline-block;
	color:#ccc;
	margin:1px;
	width:39px;
	height:39px;
	text-align:center;
	line-height:39px;
	user-select:none;
	vertical-align:top;
}

.menu-topbar .toolBtn:hover{
	background:rgba(0,0,0,0.05);
	color:#333;
}



@endpush
<div class='menu-topbar'>
	
	<div id='tools' class='tools'>
		<div class="material-icons toolBtn">create_new_folder</div>
		<div class="material-icons toolBtn">note_add</div>
		<div class="material-icons toolBtn">star</div>
		<div class="material-icons toolBtn">edit</div>
		<div class="material-icons toolBtn">tune</div>
		<div class="material-icons toolBtn">insert_comment</div>
		
	</div>
	

	<div id='loader' class="loader" style='height:3px;display:none;position:fixed; top:0;left:0;width:100%;z-index:10001;'>
	  <div class="loader__element"></div>
	</div>
</div>


@push('onReady')

	

@endpush
