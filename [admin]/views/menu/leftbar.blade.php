@push('style')

.menu-leftbar{
	font-family: 'Raleway', sans-serif;
	background:#2b2f3a; 
	position:fixed; 
	left:0;
	top:0;
	bottom:0;
	width:50px;
	z-index:10001;
	box-shadow: 0 0 3px #333;
	background-image:url(/[admin]/assets/menubg.jpg);
	background-color:rgba(0,0,0,0.5);
}

.menu-leftbar a{
	text-decoration:none;
	color:white;
}
.menu-leftbar a:hover{
	text-decoration:none;
	color:white;
}


.menu-item{
	color:white;
	cursor:pointer;
	height:45px;
	margin-bottom:1px;
	background-color:rgba(255,255,255,0.05);
	width:100%;
}

.menu-leftbar .icon{
	text-align:center;
	padding-top:10px;
	border-left:2px solid rgba(255,255,255,0.0);
	padding-right:2px;
	user-select:none;
	//font-size:20px;
}

.menu-leftbar .menu-link{
	text-align:left;
	line-height:45px;
	font-size:14px;
	user-select:none;
}

.menu-leftbar>.hover,.menu-leftbar>.menu-item:hover{
	border-left:2px solid CornflowerBlue;
	background-color:rgba(0,0,0,0.3);
}

.menu-leftbar .menu-item:hover,.menu-leftbar .hover{
	background-color:rgba(0,0,0,0.3);
}



.menu-leftbar-full{
	background:#2b2f3a; 
	display:none;
	position:fixed; 
	left:50px;
	top:0;
	bottom:0;
	width:200px;
	z-index:10001;
	background-image:url(/[admin]/assets/menubg.jpg);
	background-position: -50px 0px;
}

.menu-leftbar>a:first-child>.menu-item,.menu-leftbar-full>a:first-child>.menu-item{
	background-color:rgba(0,0,0,0.1);
}


@endpush
<div class='menu-leftbar'>
	@foreach ($menu_items as $item)
	  <a href='{{$item["link"]}}'><div class="material-icons icon menu-item">{{$item['icon']}}</div></a>
	@endforeach

	<div class='menu-leftbar-full'>
		@foreach ($menu_items as $item)
		  <a href='{{$item["link"]}}'><div class="menu-item menu-link">{{$item['name']}}</div></a>
		@endforeach
	</div>
</div>


@push('onReady')

	$('.menu-leftbar').mouseenter(()=>{
		$('.menu-leftbar-full').fadeIn('fast');
	}).mouseleave( ()=>{
		$('.menu-leftbar-full').hide();
	});
	
	$('.menu-leftbar-full .menu-item').mouseenter(function(){
		let index = $('.menu-leftbar-full .menu-item').index(this);
		$('.menu-leftbar .menu-item').eq(index).addClass('hover');
	}).mouseleave(function(){
		let index = $('.menu-leftbar-full .menu-item').index(this);
		$('.menu-leftbar .menu-item').eq(index).removeClass('hover');
	});
	
	$('.menu-leftbar .menu-item').mouseenter(function(){
		let index = $('.menu-leftbar .menu-item').index(this);
		$('.menu-leftbar-full .menu-item').eq(index).addClass('hover');
	}).mouseleave(function(){
		let index = $('.menu-leftbar .menu-item').index(this);
		$('.menu-leftbar-full .menu-item').eq(index).removeClass('hover');
	});

@endpush
