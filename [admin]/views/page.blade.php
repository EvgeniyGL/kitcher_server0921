@extends('layouts.html+bs5+jq')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>Это дополнение к основной боковой панели.</p>
@endsection

@section('body')
	@include('blocks.justmenu')
	<main>
		<div class="container py-4">
			{!! $content !!}
		</div>
	</main>
	@include('blocks.footer')   
@endsection