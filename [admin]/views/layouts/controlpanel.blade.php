<!doctype html>
<html lang="ru" class="h-100">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="/assets/bootstrap5/css/bootstrap.min.css" rel="stylesheet">
		<title>{{$title}}</title>
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Raleway&display=swap" rel="stylesheet"> 
		<link href="https://fonts.googleapis.com/css2?family=Material+Icons" rel="stylesheet">
		
		<link rel="stylesheet" type="text/css" href="/assets/slick-slider/slick.css"/>
		<link rel="stylesheet" type="text/css" href="/assets/slick-slider/slick-theme.css"/>
		
		<link rel="stylesheet" type="text/css" href="/[admin]/assets/widgets/table.css"/>
		<link rel="stylesheet" type="text/css" href="/assets/admin_scheme.css"/>
		<link href="/assets/summernote_0.8.12/summernote-lite.css" rel="stylesheet">
		<link href="/assets/select2/css/select2.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/assets/leaflet07/leaflet.css" />
		<link rel="stylesheet" href="/assets/daterangepicker/daterangepicker.css" />

		@stack('stylesheets')
		<style>
			*{
				
			}
			
			body{
				padding-left:50px;
				padding-top:45px;
				background-color:#edf1f5;
			}
			
			:root {
			  --main-color: #111;
			  --loader-color: red;
			  --back-color: #A5D6A7;
			  --time: 3s;
			  --size: 3px;
			}

			.loader {
				background-color: var(--main-color);
				overflow: hidden;
			  width: 100%;
			  height: 100%;
			  position: fixed;
			  top: 0; left: 0;
			  display: flex;
			  align-items: center;
			  align-content: center; 
			  justify-content: flex-start;  
				z-index: 100000;
			}

			.loader__element {
				height: var(--size);
				width: 100%;
				background: var(--back-color);

			}

			.loader__element:before {
				content: '';
				display: block;
				background-color: var(--loader-color);
				height: var(--size);
				width: 0;
			  animation: getWidth var(--time) ease-in infinite;
			}

			@keyframes getWidth {
				100% { width: 100%; }
			}
			
			
			
			
			.note-editor.note-frame{
				border:1px solid #eeeeee;
			}
			
			
			
			
			.select2-container{
				display:block;
				font-size:13px;
			} 
			
			.select2-selection{
				border:1px solid #eeeeee !important;
				border-radius:0 !important;
			}
			
			.select2-dropdown{
				border:1px solid #eeeeee !important;
				border-radius:0 !important;
				box-shadow: 0 2px 3px #666;
			}
			
			.select2-search__field{
				border:1px solid #eeeeee !important;
			}
			
			
			.component_files{
				margin-top:20px;
			}
			
			
			.sticky-toolbar{
				position:fixed;
				top:50px;
				z-index:99;
				width:100%;
				box-shadow:0 2px 3px rgba(0,0,0,0.3);
			}
			
			a {text-decoration:none;}
			
			
			.blockeditor{
				min-height:200px;
			}
			
			.lbe_block_devider{
				background:rgba(0,255,0,0.2);
				text-align:center;
				color:rgba(0,155,0,1);
				font-size:16px;
				font-weight:bold;
				user-select: none;
			}
			
			.lbe_block_devider:hover{
				background:rgba(0,255,0,0.4);
				cursor:help;
			}
			
			#lbe_block_selector{
				padding:20px;
				display:none;
				position:fixed;
				top:100px;
				left:50%;
				width:90vw;
				height:80vh;
				margin-left:-45vw;
				background:url(/assets/patterns/bgpattern.webp);
				outline:1000px solid #666;
				overflow-y:scroll;
				background:white;
			}
			
			.lbe_block_frame{
				border:none;
				width:90%;
				min-height:200px;
				box-shadow:1px 1px 4px gray;
				margin-bottom:30px;
				overflow:hidden;
				
				zoom: 0.15;
				-moz-transform:scale(0.75);
				-moz-transform-origin: 0 0;
				-o-transform: scale(0.75);
				-o-transform-origin: 0 0;
				-webkit-transform: scale(0.75);
				-webkit-transform-origin: 0 0;
			}
			
			.iframe-wrap {
			  position: relative;
			}
			
			.iframe-wrap::after {
			  content: "";
			  display:block;
			  height: 100%;
			  width: 100%;
			  position: absolute;
			  top: 0;
			  left: 0;
			}
			
			
			#lbe_document_frame{
				border:3px dotted gray;
				width:100%;
				height:70vh;
				
			}
			
			
			.draggable{
				background-color:yellow;
			}
			
			
			.autosizedframe{
				width:100%;
			}
			
			.webbuilder_block_container iframe{
				border:1px dotted gray;
				
			}
			
			.webbuilder_block_menu{
				
				width:30px;
				
			}
			
			
						
			@stack('style')
		</style>
	</head>
	<body class="d-flex flex-column h-100">
		@yield('leftbar')
		@yield('topbar')
		
		@yield('main')
		
		<div id='lbe_block_selector'>lbe_block_selector</div>
	
		<div id='errorWindow' style='display:none;position:fixed;background:white;top:100px;left:50%;margin-left:-500px;width:1000px;height:700px;border:2px solid red;overflow:scroll;'></div>
		
		<script src="/assets/jquery.min.js"></script>
		<script src="/assets/bootstrap5/js/popper.min.js"></script>
		<script src="/assets/bootstrap5/js/bootstrap.min.js"></script>
		<script src="/assets/sortable.js"></script>
		<script src="/assets/select2/js/select2.min.js"></script>
		<script src="/assets/summernote_0.8.12/summernote-lite.js"></script>
		<script src="/assets/moment-with-locales.min.js"></script>
		<script src="/assets/daterangepicker/daterangepicker.js"></script>
		
		<script>
			
		
		
			var currentDevider;
			var currentBlock;
			var constructorMode=false;
			
			function enableConstructor(mode){
				let moid = $("#lbe_document_frame").contents().find("[moblock]").find("[moid]");
				if (mode){
					$("#lbe_document_frame").contents().find("body").attr('contenteditable','true');
					$("#lbe_document_frame").contents().find("[moblock]").css("border","1px dotted red"); //.css("outline","2px dotted MediumVioletRed");
					moid.css("border","1px solid red");
					moid.css("background-color","rgba(255,255,0,0.5)");
				}else{
					$("#lbe_document_frame").contents().find("body").attr('contenteditable','false');
					$("#lbe_document_frame").contents().find("[moblock]").css("border","none");
					moid.css("border","none");
					moid.css("background-color","");
					let json = makeover("#lbe_document_frame");
					var ed = $("[name='content']");
					console.log(ed);
					ed.val('');
					//ed.summernote('editor.insertText', json);
					ed.summernote('code', json);
				}
			}
			
			function makeover(iframeSelector){
				var data=[];
				let body = $(iframeSelector).contents().find("body");
				body.find("[moblock]").each(function(){
					var moblock = $(this).attr('moblock');
					var obj = {
						'block':moblock,
						'data':{}
					};
					$(this).find("[moid]").each(function(){
						var moid = $(this).attr('moid');
						obj['data'][moid] = $(this).html();
					});
					
					data.push(obj);
				});

				return JSON.stringify(data);
			}
		
		
			$(document).ready(function(){
				@stack('onReady')
				
				let module = document.location.pathname.substr(1);
				if (module==""){module="main";}
				//rpc(module+":table",(data)=>{
				//  $('main').html(data);
				//});
				
				xlink(location.hash.substr(1));
				window.onhashchange = function(){
					xlink(location.hash.substr(1));
				}
				
				
				$(document).on("click",'.blockItem',function(){
					var block = $(this).attr('block');
					rpc("blocks_get_data?block="+block,function(data){
						let html = "<div class='block_container'>"+data.main+"<div>";
						//currentDevider.before(html);
						//$("#lbe_document_frame").contents().find("body").append(html);
						
						//$srlData = serialize($block);
						//let fullhtml = "<table class='webbuilder_block_container'><tr>";
						//fullhtml+= "<td><iframe class='autosizedframe' src='/rpc/blocks_host?plain=1&srldata='></iframe></td>";
						//fullhtml+= "<td class='webbuilder_block_menu'>";
						//self::blocks_menu(1);
						//fullhtml+= html;
						//fullhtml+= "</td>";
						//fullhtml+= "</tr></table>";
						
						
						$("#webbuilder_block_add").before("=======");
						$('#lbe_block_selector').hide();
					});
				});
				
				
				$(window).keydown(function (e) {
					if (e.which==112) {         
						constructorMode=!constructorMode;
						enableConstructor(constructorMode);
					}
				});
				
				
			});
			
			
			
			function init_sortable(){
				$(".sortable_table").each(function(){				
					new Sortable(this, {
						animation: 350,
						ghostClass: 'draggable',
						onEnd: function (evt) {		
						
						  if (evt.oldIndex!=evt.newIndex){
							  let pr = $(evt.item).parent();
							  console.log(pr);
							  let table = $(evt.item).parent().attr('table');
							  let parent_object_id = $(evt.item).parent().attr('parent_object_id');
							  console.log(table);
							  console.log(parent_object_id);
							  if (table!==undefined && parent_object_id!==undefined){
								  console.log("sortable_table");
								let orderNew = $(evt.item).attr("_id");
								if (typeof orderNew !== 'undefined' && orderNew !== false){
									let ids=parent_object_id+"@";
									$(evt.from).children().each(function(){
									  ids+=($(this).attr("_id")+" ");
									});

									console.log(ids);
									rpc("action/"+table+"/setroworder/",(data)=>{
										console.log(data);
									},{
										'roworder':ids,
										'parent_object_id':parent_object_id
									});
								}
								
							  }
							  
							  
						  }
						}
					});
				});
			}
			
			
			function init(){
				
				init_sortable();
				
				
				
				
				$(".tabset").each(function(){
					var tabid=0;
					var header = "<div class='tabsetHeader'>";
					$(this).find('.tabPage').each(function(){
						title = $(this).attr('name');
						$(this).attr('tabindex',tabid);
						if (tabid==0)
						{
							header+="<div class='tabsetButton' index="+tabid+" active='active'>"+title+"</div>";
							$(this).attr('active','active');
						}
						else
						{
							header+="<div class='tabsetButton' index="+tabid+">"+title+"</div>";
						}
						tabid++;
					});

					header+="</div>";
					if (tabid>1)
					{
						$(this).prepend(header);
					}
					else
					{
						$(this).prepend("<div class='tabsetHeader' style='height:5px;'></div>");
					}
				});
				
				
				$(".tabsetButton").on('click',function(){
					var index = $(this).attr('index');
					var tabsetHeader = $(this).parent();
					$(tabsetHeader).find('.tabsetButton').attr('active','');
					$(this).attr('active','active');
					
					var tabset = $(tabsetHeader).parent();
					$(tabset).find('.tabPage').attr('active','');
					
					var page = $(tabset).find('.tabPage')[index];
					$(page).attr('active','active');
					
					if (typeof autosizedframe === "function") { 
						setTimeout("autosizedframe()",300);
					}
					
				});
				
				
				
				
				$('textarea.htmleditor').summernote({
					airMode: false,
					minHeight: 300,
					maxHeight: null,
					codemirror: {
						lineNumbers: true,
						mode:'htmlmixed',
						theme:'eclipse',
						indentWithTabs:true,
						smartIndent:false,
						styleActiveLine: true,
						matchBrackets: true,
						extraKeys: {"Alt-F": "findPersistent"}
					},
					toolbar: [
					  ['edit', ['undo','redo']],
					  ['style', ['style']],
					  ['font', ['bold', 'italic','underline', 'clear']],
					  ['fontname', ['fontname']],
					  ['color', ['color']],
					  ['para', ['ul', 'ol', 'paragraph']],
					  ['table', ['table']],
					  ['insert', ['hr','link', 'picture', 'video']],
					  ['view', ['fullscreen', 'codeview', 'help']],
					],
					callbacks: {
						onImageUpload: function(files) {
							console.log('onUpload: muzhit_admin/upload.php');
							for(let i=0; i < files.length; i++) {
								console.log(files[i]);
								let out = new FormData();
								out.append('file', files[i], files[i].name);
								out.append('path', window.location.pathname);
								
								$.ajax({
									method: 'POST',
									url: '/muzhit_admin/upload.php',
									contentType: false,
									cache: false,
									processData: false,
									data: out,
									success: function (img) {
										console.log('success:'+img);
										$('textarea.htmleditor').summernote('insertImage', img);
									},
									error: function (jqXHR, textStatus, errorThrown) {
										console.error(textStatus + " " + errorThrown);
									}
								});
								
								
							}
						}
					}
				});

					
				$('#summernote').summernote({
					callbacks: {
						onImageUpload: function(files) {
							for(let i=0; i < files.length; i++) {
								$.upload(files[i]);
							}
						}
					},
					height: 500,
				});

				$.upload = function (file) {
					let out = new FormData();
					out.append('file', file, file.name);

					$.ajax({
						method: 'POST',
						url: 'upload.php',
						contentType: false,
						cache: false,
						processData: false,
						data: out,
						success: function (img) {
							$('#summernote').summernote('insertImage', img);
						},
						error: function (jqXHR, textStatus, errorThrown) {
							console.error(textStatus + " " + errorThrown);
						}
					});
				};
				
				
				
				
				/*$(".orderMoverUp").on("click",function(){
					var id = $(this).attr('index');
					orderMove(id,'up');
				});
				
				$(".orderMoverDown").on("click",function(){
					var id = $(this).attr('index');
					orderMove(id,'down');
				});*/
				

				$("select.select2").select2();

			}
			
			function xlink(path){
				if (path.substr(0,7)=='action/'){
					rpc(path,(data)=>{
						console.log(data);
						location.href=location.pathname;
					});
				}else{
					let module = document.location.pathname.substr(1);
					if (module!="")	{
						let proc = "table@"+module+"/"+path;
						//console.log(proc);
						rpc(proc,(data)=>{
							for (var key in data){
								//console.log(key+"~~");
								//console.log(data);
								$(key).html(data[key])
							}
							init();
						});
					}
				}
			}
			
			
			
			function rpc(module,callback,data){
				if (data===undefined){
					rpcGET(module,callback);
				}else{
					rpcPOST(module,data,callback);
				}
			}
			
			function rpcGET(module,callback){
				console.log("rpcGET:"+module);
				$("#loader").hide();
				$("#loader").fadeIn('slow');
				/*$.get("/rpc/"+module,function(resp){
					if (resp.substr(0,1)=="{"){
						callback(JSON.parse(resp));
					}else{
						$('#errorWindow').html(resp).show();
					}
					setTimeout("$('#loader').hide();",1000);
				});*/
				
				$.ajax({
					url : "/rpc/"+module,
					type: "GET",
					processData: false,
					contentType: false,
					success:function(resp, textStatus, jqXHR){
						
						if (resp.substr(0,1)=="{"){
							callback(JSON.parse(resp));
						}else{
							$('#errorWindow').html(resp).show();
						}
					},
					error: function(jqXHR, textStatus, errorThrown){
						alert("rpcPOST FAIL");
					}
				});
				
			}
			
			function rpcPOST(module,data,callback){
				var senddata;
				if (data instanceof FormData){
					senddata = data;
				}else{
					senddata = new FormData();
					for(var key in data) { 
						senddata.append(key,data[key]);
					}
				}
				
				
				console.log("rpcPOST:"+module);
				$("#loader").hide();
				$("#loader").fadeIn('slow');
				
				console.log("rpcPOST DATA:");
				console.log(data);

				$.ajax({
					url : "/rpc/"+module,
					type: "POST",
					data : senddata,
					processData: false,
					contentType: false,
					success:function(resp, textStatus, jqXHR){
						if (resp.substr(0,1)=="{"){
							callback(JSON.parse(resp));
						}else{
							$('#errorWindow').html(resp).show();
						}
					},
					error: function(jqXHR, textStatus, errorThrown){
						alert("rpcPOST FAIL");
					}
				});
			}
			
			
			
			function toggle(index)
			{
				var sel = "[toggle="+index+"]"
				$(sel).toggle();
			}
			
			
			
			function addFiles(obj,path,idcontainer)
			{
				//var url = document.location.href+"?addfile";
				var url = "?addfile";
				console.log(url);
				console.log(path);
				if (obj.files.length>0) 
				{
					for(var i=0; i<obj.files.length; i++)
					{
						var reader = new FileReader();
						reader.onload = function (e) {
						   //$('#img').attr('src', e.target.result);
							//console.log(e);
							$.post(url,{'sf-token':'123', path:path, file:e.target.result} ,function(data){
								console.log(data);
								if (data!="")
								{
									$(idcontainer).prepend("<div class='image'> <span></span> <img src='/"+data+"'> <span></span> </div>");
								}
							});
						}
						reader.readAsDataURL(obj.files[i]);
					}
				}
				
			}
			
			
			function lbe_add(trg){
				currentDevider = $(trg);
				$('#lbe_block_selector').html("Loading...");
				$('#lbe_block_selector').fadeIn('fast');
				rpc("blocks_viewer",function(data){
					console.log(data);
					$('#lbe_block_selector').html(data.main);
				});
			}
			
			
			function xlocation(n){
				let lo = (location.pathname+location.hash).replace("#","/");
				if (lo.substr(0,1)=="/"){lo=lo.substr(1);}
				
				let parts = lo.split("/");
				
				return parts[n];
			}
			
			
		
			
			
			function AjaxFormSubmit(ajaxSubmitButton){
				let form = $(ajaxSubmitButton).closest('form');
				var formdata = new FormData();
				
				let hash = location.hash.substr(1)
				let parentIndex=0;
				let action="";
				if(hash.indexOf("/")>=0){
					if (hash.substr(0,1)=="/"){
						action = hash.substr(1);
					}else{
						let pair = hash.split('/');
						parentIndex = pair[0];
						action = pair[1];
					}
				}
				
				if (action===""){
					action = "edit";
					formdata.append('id', hash);
				}else{
					formdata.append('pid', xlocation(1));
				}
				
				form.find("input,textarea,select").each(function(){
					let name = $(this).attr('name');
					let subFrm = $(this).closest(".subtableForm");
					let type = $(this).attr('type');
					if (typeof name !== 'undefined' && subFrm.length==0 && type!='button' && type!='submit'){
						if ($(this).attr("type")=='file'){
							console.log($(this));
							let file = $(this)[0].files[0];
							if (typeof file !== 'undefined' && file !== false) {
								formdata.append(name,file , file.name);
							}
						}else{
							let vl = $(this).val();
							if (name.endsWith('[]')){
								name = name.substr(0,name.length-2);
								formdata.append(name, '['+vl+']' );	
							}else{
								formdata.append(name, vl );	
							}
						
							
						}
					}
				});
				
				console.log(formdata);	
					
								
				rpc("action/"+xlocation(0)+"/"+action,(data)=>{
					history.back();
				},formdata);
			}
			
			
			function autosizedframe(){
				$(".autosizedframe").each(function(){
					let height = $(this).contents().find("body").height();
					console.log(height);
					$(this).css("height",(height+3)+"px");
					
				});
			}
			
			
			
			function subtableForm(table,parentId,id=0){
				let subfrm = $(`.subtableForm[table='${table}']`);
				
				if (parentId=='' && id==''){
					subfrm.hide();
				}else{
					if (id==0){//add
						$(subfrm).find(".subtableForm_title").html(table+" - Новая запись");
						$(subfrm).find(`[name='id']`).val('');
						$(subfrm).find(`.control`).val('').change();
						
						$(subfrm).find(`img[toggle]`).remove();
						
						$(subfrm).find(`input[toggle]`).show();
					}else{// edit
						$(subfrm).find(`input[toggle]`).hide();
						$(subfrm).find(".subtableForm_title").html(table+" #"+id);
						
						$(subfrm).find(`input[name='id']`).val(id);
						
						let subtbl = $(`.subtable[table='${table}']`);
						let subrow = $(subtbl).find(`tr[_id=${id}] td`).each(function(){
							let key = $(this).attr('key');
							
							
							let val = $(this).text();
							let attrVal = $(this).attr('value');
							if (typeof attrVal !== 'undefined' && attrVal !== false) {
								val = attrVal;
							}

							console.log(key);
							console.log(val);
							let frmCtrl = $(subfrm).find(`.control[name='${key}']`);
							
							//if (key=='photo' || key=="image"){
								
							if (frmCtrl.attr('type')=='file'){
								
								let toggleIndex = frmCtrl.attr('toggle');
								let img = "<img src='/media/"+val+"' style='height:200px; cursor:pointer; border:1px solid #eeeeee;' toggle='"+toggleIndex+"' onclick='toggle("+toggleIndex+");'>";
								frmCtrl.closest("div").find(".control[toggle]").html(img);
								
							}else if (frmCtrl.prop("tagName")=='SELECT'){
								frmCtrl.val(val).change();	
							}else{
								frmCtrl.val(val);	
							}
						});
						
					}
					
					subfrm.fadeIn();
				}
			}
			
			function subtableFormSave(table,parentId){
				
				var formdata = new FormData();
				
				formdata.append('_table', table);
				formdata.append('_id', parentId);

				let subfrm = $(`.subtableForm[table='${table}']`);
				
				$(subfrm).find("input,textarea,select").each(function(){
					let name = $(this).attr('name');
					//formdata[name] = $(this).val();
					
					if ($(this).attr("type")=='file'){
						console.log($(this));
						let file = $(this)[0].files[0];
						if (typeof file !== 'undefined' && file !== false) {
							formdata.append(name,file , file.name);
						}
						
					}else{
						formdata.append(name, $(this).val());	
					}
					
				});

				console.log(formdata);
				
				
				rpc("action/"+table+"/save",(data)=>{
					console.log(data);
					
					var formdata = new FormData();
					
					rpc("action/"+table+"/subtable",(data)=>{
						console.log(data);
						
						subfrm.parent().html(data.html);
						init_sortable();
						
					},{
						'_table':table,
						'_id':parentId,
					});
					
				},formdata);
				
				subtableForm(table,'','');
			}
			
			function subtableRemoveItem(table,parentId,id){
				if (confirm("Уверены, что хотите удалить этот элемент?")){
					rpc("action/"+table+"/kill",(data)=>{
						//console.log(data);
						//alert(data);
						rpc("action/"+table+"/subtable",(data)=>{
							console.log(data);
							let subfrm = $(`.subtableForm[table='${table}']`);
							subfrm.parent().html(data.html);
							init_sortable();
							
						},{
							'_table':table,
							'_id':parentId,
						});
						
					},{
						'_table':table,
						'_id':parentId,
						'id':id,
					});
				}
			}
			
			
			$(document).ready(function(){
				
				$(document).ajaxStop(function() {
					$('#loader').hide();
				  
				});
				
				//setTimeout("autosizedframe()",1000);
			});
			
			
			@stack('script')	

			{!! $schemeScript !!}
		</script>
	</body>
</html>
