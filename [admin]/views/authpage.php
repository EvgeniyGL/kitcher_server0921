<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>
	<link href="/assets/bootstrap4/css/bootstrap.min.css" rel="stylesheet">

    <style>
		body {
			background:#edf1f5;
		}


		.form-signin {
		  width: 100%;
		  max-width: 330px;
		  padding-top: 150px;
		  margin: auto;
		}
    </style>
    
  </head>
	<body class="text-center">
		<form class="form-signin" action='/auth' method='post'>
		  <h1 class="h3 mb-3 font-weight-normal">Login</h1>
		  
		  <input type="text" name="login" class="form-control" placeholder="username" required autofocus>
		  <BR>
		  <input type="password" name="password" class="form-control" placeholder="password" required>
		  <BR>
		  <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
		</form>
	</body>
</html>
