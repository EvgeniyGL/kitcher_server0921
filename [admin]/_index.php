<?php
	use eftec\bladeone\BladeOne;
	use jsonx\JsonX;
	
	require "_auth.php";
	
	if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip'))
	   ob_start('ob_gzhandler');
	else ob_start();
	
	define("BLADEONE_MODE",1); // (optional) 1=forced (test),2=run fast (production), 0=automatic, default value.
	
	$blade = new BladeOne(__DIR__ .'/views',__DIR__ .'/views/.view_cache',BladeOne::MODE_DEBUG);
	
	require "main.php";
	
	$schemeScript="";
	
	$uri = $_SERVER['REQUEST_URI'];
	if (substr($uri,0,1)=="/"){
		$uri = substr($uri,1);
	}
	
	$uri = explode("/",$uri);
	$uri1 = $uri[0]??"...";
	
	$schemeScriptFile = "schemes/{$uri1}.js";
	if (file_exists($schemeScriptFile)){
		$schemeScript = html_entity_decode(file_get_contents($schemeScriptFile),ENT_HTML5);
	}
	
	$menuItems = [];
	if (file_exists(__dir__ ."/menu.json")){
		$menuItems = JsonX::fromFile(__dir__ ."/menu.json");
	}
	
	print $blade->run("home",[
		"menu_items"=>$menuItems,
		"title"=>$pageTitle,
		"schemeScript"=>$schemeScript
	]); 
?>