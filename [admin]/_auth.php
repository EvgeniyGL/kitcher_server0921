<?php
	// InitialPassword

	require_once "config.php";
	$db = Norm\Norm::init($config);
	
	if (session_status() == PHP_SESSION_NONE){
		session_start();
	}

	$table = "sysusers";
	$scheme = new Mech\Mech($table);
	//$db->_install($scheme);
	$usersModel = $db->$table;
	
	$sessionid = session_id();
	
	
	if (isset($_POST['login']) && isset($_POST['password']))
	{
		$login 		= $_POST['login'];
		$password 	= $_POST['password']??"";
		
		
		
		$user = $usersModel->get([
			'name' => $login,
		]);
		
		if ($user!=false && password_verify($password, $user['password']))
		{
			$userId = $user['id'];
			print $sessionid;
			$usersModel->edit($userId,[
				'sessionid' => $sessionid,
			]);
			
			
			print "OK";
			header("Location: /");
			die();
		}
		else
		{
			print "!";
			require("views/authpage.php");
			die();
		}
		
	}
	else
	{
		$user = $usersModel->get(['sessionid' => $sessionid]);	
		if ($user==false)
		{
			if ($route->arg0=="auth")
			{
				require("views/authpage.php");
				die();
				
			}
			else
			{
				header("Location: /auth");
				die();
			}
		}
	}	
?>