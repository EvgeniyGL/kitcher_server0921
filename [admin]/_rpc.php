<?php
	require "_auth.php";

	$reqtype = strtoupper($_SERVER['REQUEST_METHOD']);
	
	//if ($reqtype=="GET"){

		
		
	//}elseif ($reqtype=="POST"){

		if ($route->arg1=="action" && isset($route->arg2)&& isset($route->arg3)){
			require(__dir__ ."/actions/schemeactions.php");
			$schemeName = $route->arg2;
			$actionMethod = $route->arg3;
			
			if (substr($actionMethod,0,8)=="addtype_"){
				$actionMethod = "add";
			}
			
			if (method_exists("SchemeActions",$actionMethod)){
				require "config.php";
				$db = Norm\Norm::init($config);
				//$mech = new Mech\Mech($schemeName);
				//print $actionMethod;
				SchemeActions::$actionMethod(
					db		:	$db,
					//model	:	$db->$schemeName,
					//scheme	:	$mech->mech,
					schemeName:	$schemeName
				);
			}else{
				print "Scheme action method not found: ".$actionMethod;
			}			
		}else{
			
			if (strpos($route->arg1,"@")===false){
				$schemeName = "";
				$module = $route->arg1;
			}else{
				list($module,$schemeName)= explode("@",$route->arg1);
			}

			$modPath = __dir__ ."/modules/{$module}.php";
			if (file_exists($modPath)){
				if ($schemeName==""){
					run($modPath,$route);
				}else{
					runByScheme($modPath,$schemeName,$route);
				}
			}else{
				print "ERROR:module not found! ({$module})"."<BR>";
				print_r($route);
			}
			
		}
	//}
	

		
	
	
	
	
	function run($modPath,$route){
		require_once "config.php";
		$db = Norm\Norm::init($config);
		//Sf\Widget::setDbProvider($db);
		
		if (isset($_GET['plain'])){
			require $modPath;
		}else{
			$response=[];
			ob_start();
				require $modPath;
				$response['main'] = ob_get_clean();
			print json_encode($response);
		}
		
	}
	
	
	function runByScheme($modPath,$schemeName,$route){
		require "config.php";
		$db = Norm\Norm::init($config);
		Sf\Widget::setDbProvider($db);
		
		$mech = new Mech\Mech($schemeName);
		$scheme = $mech->mech;
		$model = $db->$schemeName;
		
		
		/*$hSchemeName = "";
		$action = "";
		if (strpos($route->arg2,":")!==false){
			list($hSchemeName,$action) = explode(":",$route->arg2,2);
		}*/
		
		
		//$search = $_GET['search']??"";
		//$currentObject = $model->get($id);
		//$parentId = $currentObject['id']??$currentObject['uid']??"0";
		$response=[];
		
		ob_start();
			//print $modPath;
			
			require($modPath);
			
		$response['main'] = ob_get_clean();
		//print $modPath."[{$schemeName}/{$id}/{$method} ]";
		/*if (class_exists($cls)){
			if (method_exists($cls,$method)){
				$cls::$method();
			}else{
				print "ERROR:method not found in module! ({$module}/{$method})";
			}
		}*/			
		print json_encode($response);
		
	}

?>