<?php
class Dishes3{
	
	private $db;
	
	function __construct(){
		require "config.php";
		$this->db = Norm\Norm::init($config);		
	}
	
	
	function actionDefault(){
		
		$cacheFile = "[api]/.cache/dishes2.dat";
		if (file_exists($cacheFile) && (time()-filemtime($cacheFile))<60 ){
			
			header('Content-Type: application/json');
			header('API: Cache');
			
			if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip'))
			   ob_start('ob_gzhandler');
			else ob_start();
			
			//readfile($cacheFile);
			include($cacheFile);
			die();
			
		}else{	
		
			$units = [];
			$uts = $this->db->units->find();
			foreach($uts as $unt){
				$units[$unt['id']] = $unt['code'];
			}
			
			
			$items = $this->db->dishes->find([],['limit'=>1000,'dereferencing'=>0]);
			$r=[];
			foreach($items as $item){
				$components = $this->db->dishes_components->find(['_id'=>$item['id']]);
				$dishParts=[];
				foreach($components as $cmp){
					$unitid = $cmp['item']['ingredients.unit']??"";
					
					/*$dishPart = [
						'ingredientId'			=>$cmp['item']['ingredients.id'],
						'ingredientName'		=>$cmp['item']['ingredients.name'],
						'ingredientPackaging'	=>$cmp['item']['ingredients.packaging'],
						'ingredientUnit'		=>$units[$unitid]??"",
						'ingredientCount'		=>$cmp['count']
					];*/
					
					$dishPart = [
						'id'			=>$cmp['item']['ingredients.id'],
						'name'		=>$cmp['item']['ingredients.name'],
						'pack'	=>$cmp['item']['ingredients.packaging'],
						'unit'		=>$units[$unitid]??"",
						'count'		=>$cmp['count']
					];
					
					/*$offers = $this->db->ingredients_goods->find(['_id'=>$cmp['item']['ingredients.id']],['dereferencing'=>0]);
					foreach($offers as $ofr){
						$city = (int)$ofr['city']??0;
						$dishPart['ingredientOffers']["c".$city][$ofr['storeid']][] = $ofr;
					}*/
					$dishParts[] = $dishPart;
					//$dishParts[] = $cmp['item']['ingredients.id'];
					
				}	
				//$item['ingredients'] = "[".implode(",",$dishParts)."]";
				$item['ingredients'] = $dishParts;

				
				
				
				$steps = $this->db->dishes_steps->find(['_id'=>$item['id']],[
					'orderby'=>'_order'
				]);
				$dishSteps=[];
				foreach($steps as $stp){
					$dishSteps[] = $stp;
				}	
				$item['steps'] = $dishSteps;
				
				
				
				$r[] = $item;
			}
			
			$this->print2Json($r);
		}
	}
	
	function actionKeywords(){
		$keywords = trim($_POST['keywords']??"");
		if ($keywords!=""){
			$items = $this->db->$action->find([
				'keywords %LIKE%'=> $keywords
			],['limit'=>200]);
			
			$this->print2Json($items);
		}
	}
	
	
	function actionGet(){
		$id = $_POST['id'];
		$item = $this->db->$action->get($id);
		$this->print2Json($item,true);
	}
	
	
	function print2Json($arrOrGen,$single=false){
		header('Content-Type: application/json');
		header('API: NO_Cache');
		
		if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip'))
		   ob_start('ob_gzhandler');
		else ob_start();
		
		if ( is_array($arrOrGen) ){
			$cm="";
			print "{\"error\":\"0\",\n\"result\":[\n";
			foreach($arrOrGen as $item){
				print $cm."\t".json_encode($item,JSON_UNESCAPED_UNICODE);
				$cm=",\n";
			}
			print "\n]}";
		}elseif(is_object($arrOrGen) && is_a($arrOrGen,"Generator")){
			if ($single){
				print json_encode($arrOrGen->current(),JSON_UNESCAPED_UNICODE);
			}else{
				$cm="";
				print "{\"error\":\"0\",\n\"result\":[\n";
				foreach($arrOrGen as $item){
					print $cm."\t".json_encode($item);
					$cm=",\n";
				}
				print "\n]}";
			}
		}else{
			print_r($arrOrGen);
		}
		
		
		
		$buffer = ob_get_contents();
		file_put_contents("[api]/.cache/dishes2.dat",$buffer);
		
		die();
	}
	
}

?>