<?php
class Dishes{
	
	private $db;
	
	function __construct(){
		require "config.php";
		$this->db = Norm\Norm::init($config);		
	}
	
	
	function actionDefault(){
		$units = [];
		$uts = $this->db->units->find();
		foreach($uts as $unt){
			$units[$unt['id']] = $unt['code'];
		}
		
		
		$items = $this->db->dishes->find([],['limit'=>1000,'dereferencing'=>0]);
		$r=[];
		foreach($items as $item){
			$components = $this->db->dishes_components->find(['_id'=>$item['id']]);
			$dishParts=[];
			foreach($components as $cmp){
				$unitid = $cmp['item']['ingredients.unit']??"";
				
				$dishPart = [
					'ingredientId'			=>$cmp['item']['ingredients.id'],
					'ingredientName'		=>$cmp['item']['ingredients.name'],
					'ingredientPackaging'	=>$cmp['item']['ingredients.packaging'],
					'ingredientUnit'		=>$units[$unitid]??"",
					'ingredientCount'		=>$cmp['count']
				];
				
				$offers = $this->db->ingredients_goods->find([
					'_id'=>$cmp['item']['ingredients.id'],
				],['dereferencing'=>0]);
				foreach($offers as $ofr){
					$dishPart['ingredientOffers'][$ofr['storeid']] = $ofr; // предполагается, что не может альтернатив в рамках одного магазина!
				}
				$dishParts[] = $dishPart;
			}	
			$item['ingredients'] = $dishParts;
			
			
			/*$steps = $this->db->dishes_steps->find(['_id'=>$item['id']],[
				'orderby'=>'_order'
			]);
			$dishSteps=[];
			foreach($steps as $stp){
				$dishSteps[] = $stp;
			}	
			$item['steps'] = $dishSteps;*/
			
			
			
			$r[] = $item;
		}
		
		$this->print2Json($r);
	}
	
	function actionKeywords(){
		$keywords = trim($_POST['keywords']??"");
		if ($keywords!=""){
			$items = $this->db->$action->find([
				'keywords %LIKE%'=> $keywords
			],['limit'=>200]);
			
			$this->print2Json($items);
		}
	}
	
	
	function actionGet(){
		$id = $_POST['id'];
		$item = $this->db->$action->get($id);
		$this->print2Json($item,true);
	}
	
	
	
	function print2Json($arrOrGen,$single=false){
		header('Content-Type: application/json');
		if ( is_array($arrOrGen) ){
			$cm="";
				print "{\"error\":\"0\",\n\"result\":[\n";
				foreach($arrOrGen as $item){
					print $cm."\t".json_encode($item);
					$cm=",\n";
				}
				print "\n]}";
		}elseif(is_object($arrOrGen) && is_a($arrOrGen,"Generator")){
			if ($single){
				print json_encode($arrOrGen->current(),JSON_UNESCAPED_UNICODE);
			}else{
				$cm="";
				print "{\"error\":\"0\",\n\"result\":[\n";
				foreach($arrOrGen as $item){
					print $cm."\t".json_encode($item);
					$cm=",\n";
				}
				print "\n]}";
			}
		}else{
			print_r($arrOrGen);
		}
	}
	
}

?>