<?php
require_once("smsc_api.php");

class Auth{
	
	private $db;
	
	function __construct(){
		require "config.php";
		$this->db = Norm\Norm::init($config);		
	}
	
	
	function actionDefault(){
		//$items = $this->db->videos->find([],['limit'=>500]);
		//$this->print2Json($items);
	}
	
	function actionPhone(){
		
		$phone = $_POST['phone']??"";
		
		if (substr($phone,0,2)=="+7"){
			$phone = str_replace("+7","8",$phone);
		}
		
		if (strlen($phone)==11){
			$r = send_sms($phone,"test");
			print "<pre>";
			print_r($r);
		}
		
		
		/*$keywords = trim($_POST['keywords']??"");
		if ($keywords!=""){
			$items = $this->db->$action->find([
				'keywords %LIKE%'=> $keywords
			],['limit'=>200]);
			
			$this->print2Json($items);
		}*/
	}
	
	
	function actionGet(){
		//$id = $_POST['id'];
		//$item = $this->db->$action->get($id);
		//$this->print2Json($item,true);
	}
	
	
	
	function print2Json($arrOrGen,$single=false){
		header('Content-Type: application/json');
		if ( is_array($arrOrGen) ){
			print json_encode($arrOrGen,JSON_UNESCAPED_UNICODE);
		}elseif(is_object($arrOrGen) && is_a($arrOrGen,"Generator")){
			if ($single){
				print json_encode($arrOrGen->current(),JSON_UNESCAPED_UNICODE);
			}else{
				$cm="";
				print "{\"error\":\"0\",\n\"result\":[\n";
				foreach($arrOrGen as $item){
					print $cm."\t".json_encode($item,JSON_UNESCAPED_UNICODE);
					$cm=",\n";
				}
				print "\n]}";
			}
		}else{
			print_r($arrOrGen);
		}
	}
	
}

?>