<?php
require_once("smsc_api.php");

class App{
	
	private $db;
	
	function __construct(){
		require "config.php";
		$this->db = Norm\Norm::init($config);		
	}
	
	
	function actionDefault($r){

	}
	
	
	function actionDevice2($r){
		$appid 		= $_POST['appid']??"";
		$fcm_token 	= trim($_POST['fcm_token']??"");
		$appver 	= $_POST['appver']??"0";
		
		if ($appid!=""){
			$id = (int)$this->db->app->get(["appid"=>$appid]);

			if ($id==0){
				$appkey = rand(111,999999);
				$id = (int)$this->db->app->add([
					"appid"=>$appid,
					"fcm_token"=>$fcm_token,
					"appver"=>$appver,
					"appkey"=>$appkey,
					"created~"=>"now()",
					"lastaccess~"=>"now()",
					"accesscount"=>"1",
				]);
				
				if ($id>0){
					print "OK";
				}else{
					print "ERR";
				}
			}else{
				$data = [
					"appid"=>$appid,
					"appver"=>$appver,
					"lastaccess~"=>"now()",
					"accesscount~"=>"accesscount+1",
				];
				
				if ($fcm_token!=""){
					$data["fcm_token"]=$fcm_token;
				}
				
				
				$aff = $this->db->app->edit([
					"appid"=>$appid,
				],$data);
				
				if ($aff==1){
					print "OK";
				}else{
					print "ERR";
				}
			}
		}
	}
	
	
	function actionDevice($r){
		//$appid = (int)($r->arg2??"0");
		$appid = $_POST['appid']??"0";
		//$fcm_token = $_POST['fcm_token']??"";
		$appver = $_POST['appver']??"0";
		
		$id = (int)$this->db->app->add([
			"appid"=>$appid,
			//"fcm_token"=>$fcm_token,
			"appver"=>$appver,
		]);
		
		if ($id==0){
			$id = $this->db->app->edit([
				"appid"=>$appid
			],[
				"lastaccess~"=>"now()",
				"accesscount~"=>"accesscount+1",
				//"fcm_token"=>$fcm_token,
				"appver"=>$appver,
			]);
		}else{
			$appkey = rand(111,999999);
			
			$id = $this->db->app->edit([
				"appid"=>$appid,
			],[
				"appkey"=>$appkey,
				//"fcm_token"=>$fcm_token,
				"appver"=>$appver,
				"lastaccess~"=>"now()",
			]);
			
			print "APPKEY:".$appkey;
		}
	}
	
	
	function actionPhone(){
		
		$appid = $_POST['appid']??"";
		
		if ($appid!=""){
			$phone = $_POST['phone']??"";
			$code = rand(11111,99999);
			
			$data = [
				"phone"=>$phone,
				"sms_code"=>$code,
				"phone_state"=>1,
			];
			
			//print_r($data);
			
			$this->db->app->edit([
				"appid"=>$appid
			],$data);
			
			
			if (substr($phone,0,2)=="+7"){
				$phone = str_replace("+7","8",$phone);
			}
			
			if (strlen($phone)==11){
				$r = send_sms($phone,$code);
				//print_r($r);
				//print "SMS:$code";
			}
			
		}else{
			print "ERR1";
		}
		
	}
	
	
	function actionSms(){
		
		$appid = $_POST['appid']??"";
		
		if ($appid!=""){
			$phone = $_POST['phone']??"";
			$code = $_POST['code']??"";
			
		
			$row = $this->db->app->get([
				"appid"=>$appid,
				"phone"=>$phone,
				"sms_code"=>$code,
				"phone_state"=>1,
			]);
			
			if ($row!=false){
				
				$this->db->app->edit([
					"appid"=>$appid,
					"phone"=>$phone,
					"sms_code"=>$code,
				],[				
					"phone_state"=>2,
					"sms_code"=>0,
				]);
				
				print "OK";
				
			}else{
				print "ERR2";
			}
		}else{
			print "ERR1";
		}
		
	}
	
	
	function actionCity(){
		
		$appid = $_POST['appid']??"";
		$cityname = $_POST['cityname']??"";
		
		$cityname = trim($cityname);
		$cityname = strtolower($cityname);

		if ($appid!="" && $cityname!=""){
			$this->db->app->edit([
				"appid"=>$appid
			],[
				"cityname"=>$cityname
			]);
			
			print "OK";			
		}
	}
	
	function actionName(){
		
		$appid = $_POST['appid']??"";
		$name = $_POST['name']??"";

		if ($appid!="" && $name!=""){
			$this->db->app->edit([
				"appid"=>$appid
			],[
				"name"=>$name
			]);
			
			print "OK";			
		}
	}
	
}

?>