<?php
class Device{
	
	private $db;
	
	function __construct(){
		require "config.php";
		$this->db = Norm\Norm::init($config);		
	}
	
	/* function beforeAction($action,$r){
		$scheme = new Mech\Mech($action);
		
		if ($scheme->api){

			if ($r->arg1=="get"){
				$item = $this->db->$action->get($r->arg2);
				$this->print2Json($item,true);
			}elseif ($r->arg1=="keywords"){
				$keywords = trim(urldecode($r->arg2??""));
				if ($keywords!=""){
					$items = $this->db->$action->find([
						'keywords %LIKE%'=> $keywords
					],['limit'=>200]);
					
					$this->print2Json($items);
				}
			}else{
				$items = $this->db->$action->find([],['limit'=>500]);
				$this->print2Json($items);
			}
			
		}
		
	} */
	
	/* function afterAction($action){
		print "afterAction $exist<BR>";
	}  */
	
	function actionDefault(){
		print "actionDefault<BR>";
	}
	
	function actionSync(){
		$softid = $_POST['softid']; //a375a52fb726c80fc_175953fe2bb_z6lhjoe1yhkae2hx-0ad83a19de2b7918dfda6a7895c98cca
		$this->db->apps->add([
			'id'=>$softid
		]);
		
		$this->db->devices->add([
			'id'=>$softid,
			'created'=>time()
		]);
		
		$appsstr = trim($_POST['apps']);
		$pairs = explode("\n",$appsstr);
		foreach($pairs as $pair){
			if (trim($pair)!=""){
				$ab = explode("=",$pair,2);
				$appName = trim($ab[0]);
				$appPath = trim($ab[1]??"");
				
				$this->db->apps->edit($softid,[
					'shortcuts+name'=>$appName,
					'shortcuts+path'=>$appPath,
				]);
				
				
			}
		}
		
		
		$desktop = $this->db->desktop->get($softid);
		
		
		$this->print2Json($desktop['shortcuts']);
		
		
		die();
	}
	
	
	function actionMoney(){
		$softid = $_POST['softid'];
		$device = $this->db->devices->get($softid);
		
		$money = $device['money']??0;
		
		$data=[
			'money'=>1234,
		];
		
		$this->print2Json($data);
	}
	
	
	function print2Json($arrOrGen,$single=false){
		header('Content-Type: application/json');
		
		print "{\"error\":\"0\",\n\"data\":";
		
		if ( is_array($arrOrGen) ){
			print json_encode($arrOrGen,JSON_UNESCAPED_UNICODE);
		}elseif(is_object($arrOrGen) && is_a($arrOrGen,"Generator")){
			if ($single){
				print json_encode($arrOrGen->current(),JSON_UNESCAPED_UNICODE);
			}else{
				$cm="";
				print "[\n";
				foreach($arrOrGen as $item){
					print $cm."\t".json_encode($item,JSON_UNESCAPED_UNICODE);
					$cm=",\n";
				}
				print "\n]}";
			}
		}else{
			print_r($arrOrGen);
		}
		
		print "}";
	}
	
}

?>