<?php
class Index{
	
	private $db;
	
	function __construct(){
		require "config.php";
		$this->db = Norm\Norm::init($config);		
	}
	
	function beforeAction($action,$r){
		$scheme = new Mech\Mech($action);
		
		if ($scheme->api){

			if ($r->arg1=="get"){
				$item = $this->db->$action->get($r->arg2);
				$this->print2Json($item,true);
			}elseif ($r->arg1=="keywords"){
				$keywords = trim(urldecode($r->arg2??""));
				if ($keywords!=""){
					$items = $this->db->$action->find([
						'keywords %LIKE%'=> $keywords
					],['limit'=>200]);
					
					$this->print2Json($items);
				}
			}else{
				$items = $this->db->$action->find([],['limit'=>500]);
				$this->print2Json($items);
			}
			
		}
		
	}
	
	/* function afterAction($action){
		print "afterAction $exist<BR>";
	}  */
	
	function actionDefault(){
		print "actionDefault<BR>";
	}
	
	/* function actionVideos(){
		
	} */
	
	
	function print2Json($arrOrGen,$single=false){
		header('Content-Type: application/json');
		if ( is_array($arrOrGen) ){
			print json_encode($arrOrGen,JSON_UNESCAPED_UNICODE);
		}elseif(is_object($arrOrGen) && is_a($arrOrGen,"Generator")){
			if ($single){
				print json_encode($arrOrGen->current(),JSON_UNESCAPED_UNICODE);
			}else{
				$cm="";
				print "{\"error\":\"0\",\n\"result\":[\n";
				foreach($arrOrGen as $item){
					print $cm."\t".json_encode($item,JSON_UNESCAPED_UNICODE);
					$cm=",\n";
				}
				print "\n]}";
			}
		}else{
			print_r($arrOrGen);
		}
	}
	
}

?>