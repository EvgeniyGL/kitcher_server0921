<?php

	function getProductByUrl($url){
		$shopids=[
			"lenta"=>132,
			"metro"=>103,
			"auchan"=>270,
		];
		
		$relativePath = str_replace("https://sbermarket.ru/","",$url);
		
		$pair = explode("/",$relativePath,2);
		$shopName = $pair[0];
		$itemName = $pair[1];
		$shopid = $shopids[$shopName];

		$apiUrl = "https://sbermarket.ru/api/stores/{$shopid}/products/{$itemName}";
		
		$cfile = str_replace("https://","",$apiUrl);
		$cfile = str_replace("/","~",$cfile);
		$cfile = "goods_cache/".$cfile."__".date("Y-m-d_H-i").".json";
		
		if (!file_exists($cfile)){
			$page = file_get_contents($apiUrl);
			file_put_contents($cfile,$page);
		}
		
		
		$json = file_get_contents($cfile);
		
		$data = json_decode($json,true);
		
		$name = $data['product']['name'];
		$price = $data['product']['offer']['price'];
		$stock_rate = $data['product']['offer']['stock_rate'];
		$max_stock_rate = $data['product']['offer']['max_stock_rate'];
		$image_url = $data['product']['images']['0']['product_url'];
		
		$stock_state = floor($stock_rate/($max_stock_rate/100));
		
		$r=[
			'name'=>$name,
			'price'=>$price,
			'stock_state'=>$stock_state,
			'photo_url'=>$image_url,
			'shop'=>$shopName,
			'shopid'=>$shopid,
			'shopid'=>$shopid,
			'url'=>$url,
			'json_url'=>$apiUrl,
		];
		
		return $r;
	}
	
	
	$url = $_GET['url'];
	$product = getProductByUrl($url);
	
	header("Content-type: application/json; charset=utf-8");
	print json_encode($product,JSON_UNESCAPED_UNICODE);

?>