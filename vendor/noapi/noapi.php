<?php
namespace Noapi;

class Noapi{
	
	private $vars=[];
	
	public final function __construct($route="/api/:table/:method/:query"){
		
		
		
		
		if (substr($route,-1,1)=="/"){
			$route=substr($route,0,-1);
		}
		
		$rts = explode("/",$route);
		
		$uri = $_SERVER["REQUEST_URI"];
		if (substr($uri,-1,1)=="/"){
			$uri=substr($uri,0,-1);
		}
		
		$urs = explode("/",$uri);
		
		$rstart=0;
		if (substr($route,0,1)=="/"){
			$rstart=1;
		}else{
			$urs[0] = $_SERVER['HTTP_HOST'];
		}
		
		
		$rcnt = count($rts);
		
		for($i=$rstart; $i<$rcnt; $i++){
			if (substr($rts[$i],0,1)==":"){
				$this->vars[substr($rts[$i],1)] = $urs[$i]??"";
			}else{
				if ($rts[$i]!=$urs[$i]){
					die("BAD ROUTE!");
				}
			}
		}
		
		if ($this->auth((object)$this->vars)){
			$method = $this->vars['method']??"";
			$methodFunc = "method".ucfirst($method);
			
			if (method_exists($this,$methodFunc)){
				$this->$methodFunc((object)$this->vars);
			}else{
				print "NO SUCH METHOD: $methodFunc";
			}
		}else{
			print "NO AUTH!";
		}
	}
	
	
	function auth($vars){
		return true;
	}
	
	function print2Json($arrOrGen,$single=false){
		header('Content-Type: application/json');
		if ( is_array($arrOrGen) ){
			print json_encode($arrOrGen,JSON_UNESCAPED_UNICODE);
		}elseif(is_object($arrOrGen) && is_a($arrOrGen,"Generator")){
			if ($single){
				print json_encode($arrOrGen->current(),JSON_UNESCAPED_UNICODE);
			}else{
				$cm="";
				print "[\n";
				foreach($arrOrGen as $item){
					print $cm.json_encode($item,JSON_UNESCAPED_UNICODE);
					$cm=",\n";
				}
				print "\n]";
			}
		}else{
			print_r($arrOrGen);
		}
	}
	
}

?>