<?php
	spl_autoload_register(function ($cls) {
		if (!class_exists($cls))
		{
			$file = strtolower(str_replace("\\","/",__DIR__."/".$cls)).'.php';
			if (file_exists($file)){	
				require($file);
			}
		}
	});
?>