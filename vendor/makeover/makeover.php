<?php
namespace makeover;

class Makeover{
	
	public static function html($html,$data){ //<p class="lead" moid='text'>Quickly
		$out = "";
		$prev=0;
		$moidPos = strpos($html," moid=");	
		while($moidPos!==false){
			$spaceAfter = self::strpos_nearest($html,[" ",">"],$moidPos+1);			
			$moid = substr($html,$moidPos+6,$spaceAfter-($moidPos+6));
			
			$tagBrkt = strrpos(substr($html,0,$moidPos),"<");
			$space = strpos($html," ",$tagBrkt+1);
			$tagName = strtolower(substr($html,$tagBrkt+1,$space-($tagBrkt+1)));
			
			
			
			
			$beginContent = strpos($html,">",$moidPos+6);
			$endContent = stripos($html,"</{$tagName}>",$beginContent+1);
			
			//print "[{$tagName}:".$moid." $endContent]";
			
			$out.= substr($html,$prev,$beginContent-$prev+1);
			$prev = $endContent;
			
			//$content = substr($html,$beginContent+1,$endContent-($beginContent+1));
			
			//$out.= "|".substr($html,$prev,$beginContent+1);
			
			$moidName = str_replace("'","",$moid);
			$moidName = str_replace('"',"",$moidName);
			$val = $data[$moidName]??"...";
			$out.= $val;
			//$moidName="";
			
			$moidPos = strpos($html," moid=",$moidPos+6);
			//$prev = $endContent;
		}
		
		$out.= substr($html,$prev);
		
		
		//$out.=$html;
		
		//return htmlentities($out);
		return $out;
	}
	
	
	static function getMeta($html){
		$meta = [];
		
		$blockPos = strpos($html," moblock=");
		if ($blockPos!==false){
			$openQuotePos = $blockPos+9;
			$quote = substr($html,$openQuotePos,1);
			$closeQuotePos = strpos($html,$quote,$blockPos+10);
			$blockName = substr($html,$blockPos+10,$closeQuotePos-$openQuotePos);
			$meta['block'] = $blockName;
		}
		
		
		
		
		return $meta;
	}
	
	
	
	static function strpos_nearest($haystack,array $needles,$offset=0){
		$min=strlen($haystack);
		$found=false;
		foreach($needles as $needle){
			$pos = strpos($haystack,$needle,$offset);
			//print "(".$pos.")";
			if ($pos!==false && $pos<$min){
				$min = $pos;
				$found=true;
			}
		}
		
		if ($found){
			return $min;
		}else{
			return false;
		}
	}
	
}

?>