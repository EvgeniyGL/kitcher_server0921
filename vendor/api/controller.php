<?php
namespace Api;

class Controller{
	
	protected $db;
	
	function __construct(){
		require "config.php";
		$this->db = \Norm\Norm::init($config);		
	}
	
	function print2Json($arrOrGen,$single=false){
		header('Content-Type: application/json');
		
		function isAssoc(array $arr)
		{
			if (array() === $arr) return false;
			return array_keys($arr) !== range(0, count($arr) - 1);
		}
		
		if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip'))
		   ob_start('ob_gzhandler');
		else ob_start();
		
		
		print "{\"error\":\"0\",\n\"result\":";
		
		if(is_object($arrOrGen)){
			print json_encode($item,JSON_UNESCAPED_UNICODE);				
		}elseif(is_a($arrOrGen,"Generator")){
			if ($single){
				print json_encode($arrOrGen->current(),JSON_UNESCAPED_UNICODE);
			}else{
				$cm="";
				print "[";
				foreach($arrOrGen as $item){
					print $cm.json_encode($item,JSON_UNESCAPED_UNICODE);
					$cm=",";
				}
				print "]";
			}
		}elseif (is_array($arrOrGen)){
			
			//if (!is_numeric(array_key_first($arrOrGen)) && isAssoc($arrOrGen)){ //assoc
				
			//}else{ //sequential
				print json_encode($arrOrGen);//,JSON_UNESCAPED_UNICODE
			//}
			
		}else{
			print json_encode($arrOrGen,JSON_UNESCAPED_UNICODE);
		}
		
		print "}";

		die();
	}
	
}

?>