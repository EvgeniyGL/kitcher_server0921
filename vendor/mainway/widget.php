<?php
namespace Mainway;

class Widget{
	
	static function auto($idname, $value="", $meta=[], $db=null){
		
		//if (is_array($meta)){
			//print_r($meta);
			//return self::list($idname,$value,$meta);
		//}else{
			
			if (isset($meta['ref'])){
				return self::refCombo($idname,$value,$meta,$db);
			}else{
				if (isset($meta['expression'])){
					return "";
				}else{
					$type = $meta['type']??"";
					if (in_array($type,['int'])){
						return self::number($idname,$value,$meta);
					}else{
						
						if (method_exists(__CLASS__,$type)){
							return self::$type($idname,$value,$meta);
						}else{
							return self::string($idname,$value,$meta);
							//return "NO WIDGET FOR TYPE:{$type}";
						}
						
					}
				}
			}
		//}
	}
	
	
	
	static function autoView($idname, $value="", $meta=[], $db=null){
		if (isset($meta['ref'])){
			//return self::refCombo($idname,$value,$meta,$db);
		}else{
			$type = $meta['type']??"";
			if ($type!="")
			{
				$type = $type."View";
				if (method_exists(__CLASS__,$type)){
					return self::$type($idname,$value,$meta);
				}else{
					return self::stringView($idname,$value,$meta);
					//return "NO WIDGET FOR TYPE:{$type}";
				}
			}
			
			
			
		}
	}
	
	
	static function refCombo($idname,$value,$meta,$db){
		$ref = $meta['ref'];
		$scheme = new \Mech\Mech($ref);
		
		$cls="";
		if (isset($meta['class'])){
			$cls = "class='{$meta['class']}'";
		}
		
		$name = $meta['name']??$idname;
		
		$t = "<select {$cls} id='{$idname}' name='{$name}'>";
		$t.= "<option value='0'>Не выбрано...</option>";
		
		if (isset($scheme->fields['pid'])){
			$t.= self::refCombo_getTreeChilds($db,$ref,0);
		}else{
			$t.= self::refCombo_getFlatChilds($db,$ref);
		}
		
		$t.= "</select>";
		return $t;
	}
	
	static function refCombo_getTreeChilds($db,$table,$parent=0,$depth=0){
		$t = "";
		$gen = $db->$table->find([
			'pid' => $parent
		]);
		foreach($gen as $row){
			$disabled="";
			$txt = $row['name'];
			if (isset($row['type']) && $row['type']==0){
				$txt = strtoupper($txt);
				$disabled = "disabled='disabled'";
			}
			
			$t.= "<option title='{$table}#{$row['id']}' value='{$row['id']}' {$disabled}>".self::strMultiply(" &nbsp; &nbsp;",$depth)."{$txt}</option>";
			$t.= self::refCombo_getTreeChilds($db,$table,$row['id'],$depth+1);
		}
		
		return $t;
	}
	
	static function refCombo_getFlatChilds($db,$table){
		$t = "";
		$gen = $db->$table->find();
		foreach($gen as $row){
			$t.= "<option value='{$row['id']}'>{$row['name']}</option>";
		}
		
		return $t;
	}
	
	
	static function strMultiply($str,$count=0){
		$t="";
		
		if ($count>0){
			for($i=0;$i<$count; $i++){
				$t.=$str;
			}
		}
		
		return $t;
	}
	
	
	
	static function string($idname,$value,$meta){
		$placeholder = $meta['title']??$idname;
		
		$cls="";
		if (isset($meta['class'])){
			$cls = "class='{$meta['class']}'";
		}
		
		$name = $meta['name']??$idname;
		
		return "<input {$cls} id='{$idname}' name='{$name}' type='text' placeholder='{$placeholder}' value='{$value}'>";
	}
	
	static function text($idname,$value,$meta){
		$placeholder = $meta['title']??$idname;
		
		$cls="";
		if (isset($meta['class'])){
			$cls = "class='{$meta['class']}'";
		}
		
		$name = $meta['name']??$idname;
		
		return "<textarea {$cls} id='{$idname}' name='{$name}' type='text' placeholder='{$placeholder}'>{$value}</textarea>";
	}
	
	static function number($idname,$value,$meta){
		if ($value=="" && isset($meta['default'])){
			$value = $meta['default'];
		}
		
		$cls="";
		if (isset($meta['class'])){
			$cls = "class='{$meta['class']}'";
		}
		
		$name = $meta['name']??$idname;
		
		$val = (int)$value;
		return "<input {$cls} id='{$idname}' name='{$name}' type='number' placeholder='{$idname}' value='{$val}'>";
	}
	
	static function float($idname,$value,$meta){
		if ($value=="" && isset($meta['default'])){
			$value = $meta['default'];
		}
		
		$cls="";
		if (isset($meta['class'])){
			$cls = "class='{$meta['class']}'";
		}
		
		$name = $meta['name']??$idname;
		
		$val = (float)$value;
		return "<input {$cls} id='{$idname}' name='{$name}' type='number' placeholder='{$idname}' value='{$val}'>";
	}
	
	
	static function file($idname,$value,$meta){
		$placeholder = $meta['title']??$idname;
		
		$cls="";
		if (isset($meta['class'])){
			$cls = "class='{$meta['class']}'";
		}
		
		$name = $meta['name']??$idname;
		
		return "<input {$cls} id='{$idname}' name='{$name}' type='file' placeholder='{$placeholder}'>";
	}
	
	
	
	static function stringView($idname,$value,$meta){	
		return "<div>{$value}</div>";
	}
	
	
	static function fileView($idname,$value,$meta){
		return "<div style='background:url(/media/{$value});background-size:cover;height:40%;'></div>";
	}
	
	static function list($idname,$value,$meta){
		$cls="";
		if (isset($meta['class'])){
			$cls = "class='{$meta['class']}'";
		}
		
		$name = $meta['name']??$idname;
		
		$t = "<select {$cls} id='{$idname}' name='{$name}'>";
		foreach($meta as $item){
			$t.= "<option>{$item}</option>";
		}
		
		$t.= "</select>";
		return $t;
	}
	
	
	
}

?>