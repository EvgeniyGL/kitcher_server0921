<?php
namespace Mainway;

class Mainway
{
	static $instance;
	static $route;
	static $appcfg;
	
	
	static function run(){
		$r = self::route();
		self::$route = $r;
		
		//print "<pre>";
		//print_r(self::$route);
		//print "</pre>";
		
		$app=[];
		
		
		$app['site']=$r->site;
		$app['tpl']="";
		
		
		if ($r->arg0==""){
			$script = "[".$r->site."]/_index.php";
		}else{
			$script = "[{$r->site}]/{$r->arg0}/_{$r->arg0}.php"; //folder module
			if (file_exists($script)){
				$app['module_folder'] = "[{$r->site}]/{$r->arg0}";
			}else{
				$script = "[{$r->site}]/_{$r->arg0}.php";
			}
		}
		
		if (file_exists($script)){ //script-based
			$app['script'] = $script;
			$app['tpl'] = $r->arg0;
		}else{
			if ($r->arg0==""){ 
				$controller = "[".$r->site."]/index.controller.php";
				$app['class'] = "index";
				$app['action'] = "default";
				$app['script'] = $controller;
			}else{ 
				$app['tpl'] = $r->arg0;
				$controller = "[".$r->site."]/".$r->arg0.".controller.php";
				if (file_exists($controller)){
					$app['script'] = $controller;
					$app['class'] = $r->arg0;
					if ($r->arg1==""){
						$app['action'] = "default";
					}else{
						$app['action'] = $r->arg1;
					}
				}else{
					$controller = "[".$r->site."]/index.controller.php";
					if (file_exists($controller)){
						$app['class'] = "index";
						$app['action'] = $r->arg0;
						$app['script'] = $controller;
					}else{
						$script = "[".$r->site."]/_router.php";
						if (file_exists($script)){
							$app['script'] = $script;
						}else{
							$app['script'] = "[".$r->site."]/_404.php";
						}
					}
				}
			}
		}
		
		
		self::$appcfg = $app;
		self::render($r);
	}
	
	
	static function route(){
		$r=[];
		
		$r['subdomain'] = "";
		
		$hosts = explode(".",$_SERVER['HTTP_HOST']);
		if (count($hosts)<=2){
			$r['site'] = "website";
		}else{
			$r['site'] = "";
			$cm="";
			for($i=0; $i<count($hosts)-2; $i++){
				$r['site'].= $cm.$hosts[$i];
				$cm=".";
			}
		}
		
		if(!file_exists("[".$r['site']."]")){
			$r['subdomain'] = $r['site'];
			$r['site'] = "website";
		}
		
		$uri = explode("?",$_SERVER['REQUEST_URI'],2)[0];
		
		if (substr($uri,0,1)=="/")	{ $uri = substr($uri,1);	}
		if (substr($uri,-1,1)=="/"){$uri = substr($uri,0,-1);	}
		
		$uri = explode("/",$uri,20);
		
		$r['args'] = count($uri);
		for($i=0; $i<$r['args']; $i++){
			$r['arg'.$i] = $uri[$i];
		}
		
		if (!isset($r['arg0'])) {$r['arg0']="";}
		if (!isset($r['arg1'])) {$r['arg1']="";}
		if (!isset($r['arg2'])) {$r['arg2']="";}
		
		return (object)$r;
	}
	
	
	static function render($route){
		ob_start();
			require self::$appcfg['script'];

			if (isset(self::$appcfg['class'])){
				$ctrl = new self::$appcfg['class']();
				$func = "action".ucfirst(self::$appcfg['action']);
				
				if (method_exists($ctrl,'beforeAction')){
					$ctrl->beforeAction(self::$appcfg['action'],$route);
				}
				
				if (method_exists($ctrl,$func)){
					
					$ctrl->$func($route);
					
					if (method_exists($ctrl,'afterAction')){
						$ctrl->afterAction(self::$appcfg['action']);
					}
				}
				
				
				
			}
		$page_content = ob_get_clean();
		
		$tpl = $page_template??"template";
		
		$tplPath = "[".$route->site."]/".$tpl.".tpl.php";
		if (file_exists($tplPath)){
			require $tplPath;
		}else{
			print $page_content;
		}
	}
	
	
	
	
}

?>