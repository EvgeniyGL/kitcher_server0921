<?php
namespace http;

class Response{
	
	static function JsonOK(array $data=[]){
		$data['result'] = "OK";
		$data['datatype'] = "json";
		return json_encode($data);
	}
	
	static function JsonERR(array $data=[]){
		$data['result'] = "ERR";
		$data['datatype'] = "json";
		return json_encode($data);
	}
	
}

?>