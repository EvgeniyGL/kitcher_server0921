<?php
namespace path;

class Path{

	static function root($path=""){
		return $_SERVER['DOCUMENT_ROOT'].$path;
	}
	
	static function from($basePath,$relativePath){
		$basePath = str_replace("\\","/",$basePath);
		
		if (substr($basePath,-1,1)=="/"){
			$basePath = substr($basePath,0,-1);
		}
		
		if (substr($relativePath,-1,1)=="/"){
			$relativePath = substr($relativePath,0,-1);
		}
		
		if (substr($relativePath,0,1)=="/"){
			$ret = $basePath.$relativePath;
		}else if (substr($relativePath,0,3)=="../"){
			if (substr($basePath,0,1)=="/"){
				$basePath = substr($basePath,1);
			}
			$baseParts = explode("/",$basePath);
			$baseCnt = count($baseParts);
			$subCnt = substr_count($relativePath,"../");
			
			$path = "";
			for($i=0; $i<$baseCnt-$subCnt; $i++){
				$path.=$baseParts[$i]."/";
			}
			$ret =  $path.str_replace("../","",$relativePath);
		}else{
			$ret =  $basePath."/".$relativePath;
		}
		
		if (!substr($ret,1,2)==":/"){
			$ret="/".$ret;
		}
		
		return $ret;
	}
	
	static function make($relativePath){
		
		if (substr($relativePath,0,1)=="/"){ 
			return self::root($relativePath);
		}elseif (substr($relativePath,0,3)=="../"){ 
			$relativePath = substr($relativePath,3);
			return self::script($relativePath);
		}else{
			return __dir__."/".$relativePath;
		}		
		
	}
	
	
	
}

?>