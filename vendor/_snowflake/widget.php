<?php
namespace Snowflake;

class Widget
{
	private static $db;
	
	static function setDbProvider($db)
	{
		self::$db = $db;
	}
	
	
	static function component($cmp,$title,$id,$model)
	{
		$componentid = "component".rand(111111,999999);
		
		if (substr($cmp,0,3)=="DIR" || substr($cmp,0,3)=="dir")	//      @DIR /media/^^/*.jpg-
		{
			$html="";
			if ($title!="")
			{
				//$html.="<div class='formFieldLabel'>{$title}</div><HR>";
				//$html.="<h5>{$title}</div><HR>";
			}
			
			
				
				
			$reverse=false;
			$path = trim(substr($cmp,3));
			$mask="";
			
			
			if (substr($path,-1,1)=="+")
			{
				$path = trim(substr($path,0,-1));
			}
			elseif (substr($path,-1,1)=="-")
			{
				$path = trim(substr($path,0,-1));
				$reverse=true;
			}
			
			
			
			if (strpos($path,"*")!=false)
			{
				$prt = explode("*",$path);
				$path = trim($prt[0]);
				$mask = strtolower(trim($prt[1]));
				
				if (substr($mask,0,1)=="."){$mask = substr($mask,1);}
				
			}
			
			
			if (substr($path,0,1)=="/"){$path = substr($path,1);}
			if (substr($path,-1,1)=="/"){$path = substr($path,0,-1);}
			
			$modeldir = strtolower($model->name);
			$docdir = $modeldir."/".$id;
			$path = str_replace("^^",$docdir,$path);
			$path = str_replace("^",$modeldir,$path);
			
			
			if (!is_dir($path))
			{
				if (strpos($path,"/")==false)
				{
					@mkdir($path);
				}
				else
				{
					$prts = explode("/",$path);
					$p="";
					foreach($prts as $part)
					{
						if (!is_dir($p.$part))
						{
							
							@mkdir($p.$part);
						}
						$p.=$part."/";						
					}
				}
			}
			
			
			if (substr($cmp,0,3)=="DIR")
			{
				$html.="<div>";
					$html.="<input type='file' name='addfiles[]' multiple='multiple' onchange='addFiles(this,\"{$path}\",\"#{$componentid}\")'>";
				$html.="</div>";
			}
			
			$html.="<div id='{$componentid}' class='widget_component component_dir' style='width:100%;'>";
			
			if ($reverse)
			{
				$files = scandir($path,1);
			}
			else
			{
				$files = scandir($path);
			}
			
			//print "<div >";
			
			foreach($files as $file)
			{
				if (substr($file,0,1)!="." && !is_dir($path."/".$file))
				{
					$ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
					if ($mask=="" || $mask==$ext)
					{
						if (in_array($ext,[ 'jpg','jpeg','gif','png','webp' ]))
						{
							$html.="<div class='image'> <span></span> <img src='/{$path}/{$file}' title='{$file}'> <span></span> </div>";
						}
						else
						{
							$html.="<div class='file' src='/{$path}/{$file}'>{$file}</div>";
						}
					}
				}
			}
			
			//print "</div>";
			
			
			
			
			$html.="</div>";
			
			return "<div class='component_files'><div class='formFieldLabel'>Files:</div>".$html."</div>";
			
		}
		else
		{
			return "NO_COMPONENT: ".$cmp;
		}
			
		
		
		
	}
	
	static function byType($key,$type,$val,$data=null)
	{
		
		$componentid = "component".rand(111111,999999);
		
		$idname = $data['fullname']??$key;
		
		$ikv = explode(".",$idname);
		if (count($ikv)>1)
		{
			$idname = $ikv[0]."[".$ikv[1]."]";
		}
		
		
		if ($type=="ai")
		{
			return "<input name='{$idname}' class='formControl' type='text' disabled='disabled' value='{$val}'>";
		}
		elseif (substr($type,0,4)=="guid" || substr($type,0,3)=="uid" || substr($type,0,4)=="uuid")
		{
			return "<input name='{$idname}' class='formControl' type='text' disabled='disabled' value='{$val}'>";
		}
		elseif (substr($type,0,3)=="int" || substr($type,0,6)=="bigint"   || substr($type,0,8)=="tinnyint" || substr($type,0,8)=="smallint")
		{
			return "<input name='{$idname}' class='formControl' type='number' value='{$val}'>";
		}
		elseif (substr($type,0,9)=="datetimex")
		{
			$vl = date("c", strtotime($val));
			
			return "<input name='{$idname}' class='formControl' type='text' value='{$val}'>";
		}
		elseif (substr($type,0,8)=="datetime")
		{
			if ($val=="" || $val=="0000-00-00 00:00:00"){$val="2000-01-01 00:00:00";}
			
			$dt = explode(" ",$val);
			$d = explode("-",$dt[0]);
			
			$vl = $d[2].".".$d[1].".".$d[0];
			
			if (count($dt)==2)
			{
				$t = explode(":",$dt[1]);
				$vl.= " ".$t[0].":".$t[1].":".$t[2];
			}

			return "<input name='{$idname}' class='formControl datetime' value='{$vl}' title='{$vl}'>";
		}
		elseif (substr($type,0,4)=="date")
		{
			return "<input name='{$idname}' class='formControl' type='date' value='".substr($val,0,10)."'>";
		}
		
		elseif (substr($type,0,4)=="time")
		{
			return "<input name='{$idname}' class='formControl' type='time' value='{$val}'>";
		}
		elseif (substr($type,0,4)=="file" && substr($type,0,5)!="files")
		{
			if ($val=="")
			{
				return "<input name='{$idname}' class='formControl' type='file'>";
			}
			else
			{
				$randindex=rand(111111,999999);
				$html = "<div class='input_file_wrapper'>";
					$ext = strtolower(pathinfo($val, PATHINFO_EXTENSION));
					if (in_array($ext,[ 'jpg','jpeg','gif','png','webp', ]))
					{
						$html.= "<img src='/media/{$val}' style='max-width:100%; cursor:pointer; border:1px solid #eeeeee;' toggle={$randindex} title='{$val}' onclick='toggle({$randindex});'>";
					}
					else
					{
						$html.= "<div class='formControl' style='background:white;' toggle={$randindex} onclick='toggle({$randindex});'>{$val}</div>";
					}
				
					$html.= "<input name='{$idname}' class='formControl' type='file' value='{$val}' toggle={$randindex} style='display:none;' oncontextmenu=\"javascript:toggle({$randindex});return false;\">";
				$html.= "</div>";
				return $html;
			}
		}
		elseif (substr($type,0,5)=="files")
		{
			$html="";
			if ($val!="")
			{
				$items = explode(";",$val);
				$html.="<div>";
					foreach($items as $item)
					{
						$ext = strtolower(pathinfo($item, PATHINFO_EXTENSION));
						$html.="<div class='filePreview' type='{$ext}' file='{$item}'> <span></span>";
						
							if (in_array($ext,[ 'jpg','jpeg','gif','png','webp', ]))
							{
								$html.= "<img src='/media/{$item}' title='{$item}'>";
							}
							else
							{
								$html.= " <div>{$item}</div>";
							}
						$html.="<span></span ></div>";
					}
				$html.="</div>";
			}
			
			return "<input name='{$idname}[]' class='formControl' type='file' multiple='multiple'>".$html;
		}
		elseif (substr($type,0,6)=="string")
		{
			return "<input id='{$idname}' name='{$idname}' class='formControl' type='text' value='{$val}'>";
		}
		elseif ($type=="html")
		{
			return "<textarea name='{$idname}' class='formControl htmleditor'>{$val}</textarea>";
		}
		elseif ($type=="text")
		{
			return "<textarea name='{$idname}' class='formControl'>{$val}</textarea>";
		}
		elseif (substr($type,0,8)=="password")
		{
			return "<input name='{$idname}' class='formControl' type='password' value='' placeholder='********'>";
		}
		elseif (substr($type,0,5)=="phone")
		{
			return "<input name='{$idname}' class='formControl' type='phone' value='{$val}' placeholder=''>";
		}
		elseif (substr($type,0,5)=="email")
		{
			return "<input name='{$idname}' class='formControl' type='email' value='{$val}' placeholder='user@google.com'>";
		}
		elseif (substr($type,0,11)=="coordinates")
		{
	
			if (is_object($val))
			{
				$val = $val->x." ; ".$val->y;
			}
			elseif (is_array($val))
			{
				$val = $val['x']." ; ".$val['y'];
			}
			
			$html = "<input name='{$idname}' class='formControl' value='{$val}'>";
			
			if ($val=="")
			{
				$x = "0";
				$y = "0";
			}
			else
			{
				$pts = explode(";",$val);
				$x = trim($pts[0]);
				$y = trim($pts[1]);
			}
			
			$html.= "<div class='coordinates_choser' style='height:400px;'>";
				$html.= "<div id='locationPicker'  style='width: auto; height: 400px;' x='{$x}' y='{$y}'></div>";
			$html.= "</div>";

			return $html;
		}
		elseif (substr($type,0,1)=="$")
		{
			
			$modelName = trim(substr($type,1));
			
			$html = "<select name='{$idname}' class='formControl select2'>";
			
			$id = (int)$val;
			
			if (substr($type,0,2)=="$[")
			{
				$html.= self::getOptionsForList($type,$id);
			}
			else
			{
				$html.= self::getOptionsForDictionary($modelName,$id,$key);
			}
			
			
			
			$html.= "</select>";
			
			return $html;
		}
		elseif (substr($type,0,1)=="*")
		{
			$models = explode(",",trim(substr($type,1)));
			$vals = explode(",", $val);
			
			$html = "<select name='{$idname}' class='formControl select2' multiple='multiple'>";
				foreach($vals as $link)
				{
					if ($link!="attach") // костыль!!!
					{
						$lkv = explode("#",$link);
						$modelName = $lkv[0];
						$modelId = $lkv[1]??0;
						
						if ($modelId>0)
						{
							$obj = self::$db->get($modelName,$modelId);
						}
						
						if (!isset($obj) || $obj==false)
						{
							$obj=[];
							$obj['id'] = $modelId;
							$obj['name'] = "!{$modelName}#{$modelId}";
						}
					
				
						$html.= "<option value='{$obj['id']}' selected='selected' title='{$modelName}#{$modelId}'>{$obj['name']}</option>";
					}
				}
				
			$html.= "</select>";
			
			return $html;
		}
		elseif (substr($type,0,1)=="{")
		{
			if (is_array($val))
			{
				$value = implode(",",$val);
			}
			else
			{
				$value = $val;
			}
			
			
			return "<input name='{$idname}' class='formControl' type='text' value='{$value}'>";
		}
		elseif (substr($type,0,1)=="[")
		{
			$value = (int)$val;
			$typestr = substr($type,1);
			if (substr($typestr,-1,1)=="]") {$typestr = substr($typestr,0,-1);}
			
			$html = "<select name='{$idname}' class='formControl'>";
			$i=0;
			$items = explode(",",$typestr);
			foreach($items as $item)
			{
				$kv = explode("=",$item,2);
				
				$sel = "";
				if (count($kv)==1)
				{
					if ($value==$i) {$sel = "selected='selected'";}	
					$html.= "<option value='{$i}' {$sel}>{$kv[0]}</option>";
				}
				else
				{
					if ($value==$kv[0]) {$sel = "selected='selected'";}	
					$html.= "<option value='{$kv[0]}' {$sel}>{$kv[1]}</option>";
				}
				
				$i++;
			}
			$html.= "</select>";			
			return $html;
		}
		else
		{
			return "<input name='{$idname}' class='formControl' type='text' value='{$val}'> {$type}";
		}
	}
	
	
	static function getOptionsForDictionary($modelName,$id=0,$key="")
	{
		$html="";
		if ($id==0)
		{
			$obj = false;
			if ($key=='pid')
			{
				$html.= "<option value='0' selected='selected'>[...]</option>";
			}
			else
			{
				$html.= "<option value='0' selected='selected'>none</option>";
			}
		}
		else
		{
			if ($key=='pid')
			{
				$html.= "<option value='0'>[...]</option>";
			}
			else
			{
				$html.= "<option value='0' >none</option>";
			}
			
			$obj = self::$db->get($modelName,$id);
			if ($obj==false)
			{
				$html.= "<option value='{$id}' selected='selected'><span color=red>Error: Item №{$id} doesn't exists or removed</span></option>";
			}
			else
			{
				$html.= "<option value='{$obj['id']}' selected='selected';>{$obj['name']}</option>";
			}
		}
		
		if ($key=='pid')
		{
			$res = self::$db->find($modelName,"id<>{$id} AND type=0 ORDER BY id ASC LIMIT 100");
		}
		else
		{
			$res = self::$db->find($modelName,"id<>{$id} ORDER BY id DESC LIMIT 100");
		}
		
		foreach($res as $row){
			if (isset($row['type']) && $row['type']==0)
			{
				$html.= "<option value='{$row['id']}'>[{$row['name']}]</option>";
			}
			else
			{
				$html.= "<option value='{$row['id']}'>{$row['name']}</option>";
			}
		}
		
		return $html;
	}
	
	
	
	static function getOptionsForList($type,$id=0)
	{
		$html="";
		
		$listStr = trim(substr($type,2));
		if (substr($listStr,-1,1)=="]")
		{
			$listStr = substr($listStr,0,-1);
		}
		
		$arr=[];
		$list = explode(",",$listStr);
		$i=0;
		foreach($list as $item)
		{
			$kv = explode(":",$item);
			if (count($kv)==1)
			{
				$arr[$i] = $kv[0];
			}
			else
			{
				$arr[$kv[0]] = $kv[1];
			}
			
			$i++;
		}
		
		
		foreach($arr as $key=>$val)
		{
			if ($key==$id)
			{
				$html.= "<option value='{$key}' selected='selected'>{$val}</option>";
			}
			else
			{
				$html.= "<option value='{$key}'>{$val}</option>";
			}
		}
		
		
		return $html;
	}
	
	
	
	
	static function pagination($rowsCnt,$currentPage=1,$itemPerPage=10) //page numeration begins from 1 (not from 0)
	{
		$pagesCnt = (int)ceil($rowsCnt/$itemPerPage);
		
		if ($currentPage<1) {$currentPage=1;}
		
		$prevPage=$currentPage-1;
		if ($prevPage>1){$prevHref="href='?page={$prevPage}'";} else {$prevHref="";}
		
		$nextPage=$currentPage+1;
		if ($nextPage>$pagesCnt) {$nextPage=$pagesCnt;}
		
		
		$prevDisabled = "";
		$nextDisabled = "";
		
		
		print "<div class='sfwidget_pagination'>";
			if ($prevPage==$currentPage){$prevDisabled = "disabled";}
			print "<a {$prevHref} {$prevDisabled}>❮</a>";
			for($i=1; $i<=$pagesCnt; $i++)
			{
				$num=$i;
				//if ($num<=1){$num="";}
				
				if ($currentPage==$i)
				{
					$current="current";
					$href= "";
				} 
				else
				{
					$current = "";
					$href= "href='?page={$num}'";
				}			
				
				print "<a {$href} {$current}>{$i}</a>";
			}
			if ($nextPage==$currentPage)
			{
				$prevDisabled = "disabled";
				$nextHref="";
			}
			else
			{
				$nextHref="href='?page={$nextPage}'";
			}
			print "<a {$nextHref} {$nextDisabled}>❯</a>";
		print "</div>";
	}
	
}


?>