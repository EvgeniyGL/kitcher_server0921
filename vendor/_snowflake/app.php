<?php
namespace Snowflake;

class App
{
	private static $instance;
	
	private $rootPath;
	private $appRouter;
	private $renderer;
	
	private $app;
	
	private $startTime;
	
	private $subdomainController=false;
	
	private $sandboxScript;
	
	private $dbconnections=[];
	public  $dataproviders=[];
	
	
	
	//==========
	private $zone;
	
	private $models=[];
	private $schemes=[];
	
	
	function __construct($opts=[])
	{
		$this->startTime = microtime(true);

		$this->rootPath = dirname($_SERVER['SCRIPT_FILENAME'])."/";
		$opts['rootPath'] = $this->rootPath;
		
		$this->start($opts);
	}
	
	
	function model($modelName)
	{
		if (!isset($this->models[$modelName]))
		{
			$this->models[$modelName] = new SchemeModel2($this,$modelName);
		}		
		
		return $this->models[$modelName];
	}
	
	
	function scheme($schemeName)
	{
		return \Snowflake\SchemeManager::get($schemeName);
	}
	
	/* function dataProvider($dbType)
	{
		$dbType = trim(strtolower($dbType));
		
		if (!isset($this->dataproviders[$dbType]))
		{
			require_once($this->rootPath."db.config.php");
			
			if ($dbType=="mongodb")
			{
				$this->dataproviders[$dbType] = new \Snowflake\Data\MongodbProvider(
					$db[$dbType]['host'],
					$db[$dbType]['base'],
					$db[$dbType]['user'],
					$db[$dbType]['pass'],
				);
			}
			elseif ($dbType=="mysql")
			{
				$this->dataproviders[$dbType] = new \Snowflake\Data\MysqlProvider(
					$db[$dbType]['host'],
					$db[$dbType]['base'],
					$db[$dbType]['user'],
					$db[$dbType]['pass'],
				);
			}
			else
			{
				$this->fatal("app","unknown data provider:{$dbType}");
			}
		}
		
		return $this->dataproviders[$dbType];
	} */
	
	
	
	public function __invoke($p)
	{
		if (substr($p,0,1)=="$")
		{
			$str = $this->model("strings")->findOne([
				'name' => substr($p,1)
			]);
			
			return $str['content']??false;
		}
		elseif (substr($p,0,1)=="#") //schemeObject
		{
			return $this->scheme(substr($p,1));
		}
		else //model
		{
			return $this->model($p);
		}
	}
	
	
	static function run($opts=[])
	{	
		self::$instance = new self($opts=[]);
		return self::$instance;
	}
	
	function sandboxRender($app,$tmpl="")
	{
		ob_start();
		require($this->sandboxScript);
		$pageContent = ob_get_clean();		

		if ($tmpl=="")
		{
			return $pageContent;
		}
		else
		{
			$tmplPath = $this->zone->path.$tmpl;

			if (file_exists($tmplPath))
			{
				ob_start();
				require($tmplPath);
				return ob_get_clean();
			}
			else
			{
				//die("template not found!");
				return $pageContent;
			}
		}
	}
	
	
	function url($item=-1)
	{
		$url = explode("?",$_SERVER['REQUEST_URI'])[0];			//only path
		if (substr($url,0,1)=="/"){$url = substr($url,1);}	//no lead slash
		if (substr($url,-1,1)=="/"){$url = substr($url,0,-1);}	//no end slash
		
		if ($item==-1)
		{
			return $url;
		}
		else
		{
			$urls = explode("/",$url);
			return $urls[$item]??"";
		}
	}
	
	function getZone()
	{
		$zoneId = "website";
		$bySubdomain=false;
		
		$domain = explode(".",$_SERVER['HTTP_HOST']);
		if (count($domain)>=3)	// if subdomain then subdomain is a zoneID
		{
			if ($domain[0]=="www")
			{
				$zoneId = "website";
			}
			else
			{
				$sub = array_reverse($domain)[2]; // SUB.site.com or *.SUB.site.com
				if (file_exists("[{$sub}]"))	// [sub] zone
				{
					$zoneId = $sub;
					$bySubdomain=true;
				}
				else	// [] zone
				{
					$zoneId = "";
				}
			}
		}
		else
		{
			//$url = explode("?",$_SERVER['REQUEST_URI'])[0];			//only path
			//if (substr($url,0,1)=="/"){$url = substr($url,1);}	//no lead slash
			//if (substr($url,-1,1)=="/"){$url = substr($url,0,-1);}	//no end slash
			$url = explode("/",$this->url());			
			$firstUrl = $url[0];
			
			if ($firstUrl!="" && file_exists("[{$firstUrl}]"))
			{
				$zoneId = $firstUrl;
			}
		}
			
		return (object)[
			'id' =>$zoneId,
			'dir'=>"[{$zoneId}]",
			'path'=>$_SERVER['DOCUMENT_ROOT']."/[{$zoneId}]/",
		];
	}
	
	
	function initDB1()
	{
		if (file_exists($this->rootPath."db.config.php"))
		{
			require_once($this->rootPath."db.config.php");
			if (is_array($db))
			{
				if (isset($db['mysql']))
				{
					//$this->dataproviders['mysql'] 	= new \Snowflake\Data\MysqlProvider($db['mysql']['host'],$db['mysql']['user'],$db['mysql']['pass'],$db['mysql']['base']);
				}
				
				if (isset($db['mongodb']))
				{
					/* $this->dataproviders['mongodb'] = new \Snowflake\Data\MongodbProvider(
						$db['mongodb']['host'],
						$db['mongodb']['base'],
						$db['mongodb']['user'],
						$db['mongodb']['pass'],
					); */
					
					/* $id = $this->dataproviders['mongodb']->add("test",[
						['zzz'=>1,'bb'=>222],
						['zzz'=>2,'bb'=>222],
						['zzz'=>3,'bb'=>222],
					],true); */ 
					
					//$id = $this->dataproviders['mongodb']->add("test",['zzz'=>1,'bb'=>222]);
					
					//print $id;
					
					
					/* $res = $this->dataproviders['mongodb']->edit("test","5f21a4f87e7b00000b0061be",[
						'aa'=>'aaaaaaa1111',
						'bb'=>333,
					]); */
					
					
					
					//$this->dataproviders['mongodb']->kill("5f209a557e7b00000b0061b4");
					
					//$res = $this->dataproviders['mongodb']->get("test","5f22a065be6a00004b004e26");
					//print_r($res);
					
					/* $items = $this->dataproviders['mongodb']->selectGenerator("test");
					foreach($items as $item)
					{
						print_r($item);
						print "<HR>";
					} */
					
					
					//print $this->dataproviders['mongodb']->count("test",['bb'=>333]);
					//print $this->dataproviders['mongodb']->meta("test","count",['bb'=>['$eq'=>777]]);
					
				}
				
				
				
			}
			else
			{
				die("db.config has no `db` array");
			}
		}
	}
	
	
	function tag($tag,$print=true) //deprecated
	{
		static $stringModel=null;
		if ($stringModel==null)
		{
			//$stringModel = new \Snowflake\SchemeModel($this,"strings");
			
		}
		
		//$str = $stringModel->findOne([
		//	'name' => $tag,
		//]);
		
		$str = $this->model("strings")->findOne([
			'name' => $tag,
		]);
		
		
		if ($str!=false)
		{
			if ($print==true)
			{
				print $str['content'];
			}
		}

		return $str['content']??"";
	}
	
	function val($key) //deprecated
	{
		return $this->tag($key,false);
	}
		
	function start(array $opts)
	{
		$this->zone = $this->getZone();
		
		//print_r($this->zone);
		
		//$this->initDB();
		
		if (!file_exists($this->zone->path."app.controller.php"))// classic php-site (/pages/123 => _pages.php)
		{
			
			$url = $this->url(0);
			if ($url==""){$url = "index";}
			$this->sandboxScript = $this->zone->path."_".$url.".php";
			//print $this->sandboxScript;
			if (!file_exists($this->sandboxScript))
			{
				$this->sandboxScript = $this->zone->path."_router.php";
			}
			
			
			print $this->sandboxRender($this,"template.php");
			
		}
		else	// snowflake app
		{
			require($this->zone->path."app.controller.php");
			$appController = new \AppController();
		}
		
		if (DEBUG)
		{
			print "\n\n<!--".round((microtime(true)-$this->startTime),5)."-->";
		}
	}
	
	
	function fatal($module,$errstr="",$params="")
	{
		print "APPFATAL:<BR>";
		print $module."<BR>";
		print $errstr."<BR>";
		print $params."<BR>";
		die(); 
	}
	
	
}

?>