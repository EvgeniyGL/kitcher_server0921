<?php
namespace Snowflake;

class Schemeutils
{
	static function decodeOpts($opts)
	{
		$opts = trim($opts);
		
		$mysqltype="";
		
		if (substr($opts,0,1)=="$")	// one2many reference to another table (foreign key by id)
		{
			$refTable = trim(substr($opts,1));
			return [
				'type'		=> "bigint DEFAULT '0' COMMENT '".'$'.$refTable."'",
				'reftable'  => $refTable,
			];
		}
		elseif (substr($opts,0,1)=="*")	// reference list to another tables (users#1,nomenclature#127,sales?date=01.01.2020)
		{
			$refObjects = trim(substr($opts,1));
			if ($refObjects==""){$refObjects="";}
			return [
				'type'		=> "text COMMENT '".'*'.$refObjects."'",
				'refobjects'  => $refObjects,
			];
		}
		elseif (substr($opts,0,1)=="{")	// json object
		{
			$opts = substr($opts,1);
			if (substr($opts,-1,1)=="}")
			{
				$opts = substr($opts,0,-1);
			}
			
			return [
				'type'		=> "JSON COMMENT '{{$opts}}'",
				'jsonfields'  => $opts,
			];
		}
		else
		{
			$opts = explode(" ",$opts);
			
			$res=[];
			$add_ai="";
			
			if (in_array("ai",$opts)){$res['auto_increment']=true; $res['primary_key']=true; $add_ai = " AUTO_INCREMENT"; }
			if (in_array("pk",$opts)){$res['primary_key']=true;}
			
			
			$msql = self::mysqlType($opts[0]);
			
			$res['type'] = $msql['mysqltype'].$add_ai;
			
			return $res;
		}
		
		
	}
	
	
	static function mysqlType($type)
	{
		$type = strtolower(trim($type));
		
		$pts = explode("=",$type);
		
		$type = $pts[0];
		$default = trim($pts[1]??"");
		
		$resType="";
		$resDefault="";
		$resTrigger="";
		
		if (substr($type,0,6)=="string")
		{
			$size = (int)substr($type,6);
			if ($size==0){$size=100;}
			
			$mtype = "varchar({$size})";
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			
			$resType = $mtype;
		}
		elseif (in_array($type,['ai','int','bigint','smallint']))
		{
			if ($type=='ai')
			{
				$mtype = "bigint";
			}
			else
			{
				$mtype = $type;
			}
			
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			$resType = $mtype;
		}
		elseif (in_array($type,['html']))
		{
			$mtype = "text";
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			$resType = $mtype;
		}
		elseif ($type=='phone')
		{
			$mtype = "varchar(50)";
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			$resType = $mtype;
		}
		elseif ($type=='email')
		{
			$mtype = "varchar(100)";
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			$resType = $mtype;
		}
		elseif (in_array($type,['coordinates','point']))
		{
			$mtype = "POINT";
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			$resType = $mtype;
		}
		elseif ($type=='file')
		{
			$mtype = "varchar(200)";
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			$resType = $mtype;
		}
		elseif ($type=='files')
		{
			$mtype = "text";
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			$resType = $mtype;
		}
		elseif ($type=='password')
		{
			$mtype = "varchar(100)";
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			$resType = $mtype;
		}
		elseif (in_array($type,['guid','uuid','uid']))
		{
			$mtype = "varchar(50)";
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			$resType = $mtype;
		}
		elseif ($type=='orderposition')
		{
			$mtype = 'int';
			$resType = 'int';
		}
		elseif (substr($type,0,1)=='[')
		{
			$mtype = 'tinyint';
			$resType = $mtype;
			$default = 0;
		}
		elseif ($type=='data')
		{
			$mtype = 'text';
			$resType = $mtype;
		}
		else
		{
			$resType = $type;
		}
		
		
		if ($default!="")
		{
			//print "( $default )";
			
			
			if (substr($default,0,1)=="(")
			{
				//todo procs and triggers!
				$resDefault = "";
				
				$dtrig = substr($default,1);
				if (substr($dtrig,-1,1)==")") {$dtrig = substr($dtrig,0,-1);}
				
				$tps = explode(">",$dtrig);
				
				if (count($tps)==1)
				{
					$tFunc = strtolower(trim($tps[0]));
					$tCond = "";
				}
				else
				{
					$tCond = strtolower(trim($tps[0]));
					$tFunc = strtolower(trim($tps[1]));
				}
				
				if ($tFunc=="now")
				{
					if (self::isMariaDB()==true)
					{
						if ($tCond=="")
						{
							$resDefault = " NOT NULL DEFAULT CURRENT_TIMESTAMP";
						}
						elseif ($tCond=="onedit")
						{
							$resDefault = " NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP";
						}
					}
				}
				elseif ($tFunc=="uuid" || $tFunc=="guid" )
				{
					if (self::isMariaDB()==true)
					{
						$resDefault = " NOT NULL DEFAULT UUID()";
					}
				}
				
			}
			elseif (substr($default,0,1)=="[")
			{
				$resDefault = " NOT NULL DEFAULT 0";
			}
			else
			{
				$resDefault = " NOT NULL DEFAULT '{$default}'";
			}
		}
		
		return [
			'mysqltype' => $resType.$resDefault,
			'trigger'	=> $resTrigger,
		];
		
		
	}
	
	static function isMariaDB()
	{
		$db = \Snowflake\Data\Manager::get("mysql");
		//print_r($db);
		
		$sql = "SELECT COUNT(*) as `cnt` FROM information_schema.ALL_PLUGINS WHERE PLUGIN_NAME='Aria'";
		$res = $db->sql($sql);
		$row = $res->fetch();
		$cnt = $row['cnt']??0;
		return (bool)$cnt;
	}
}

?>