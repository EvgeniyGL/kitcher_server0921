<?php
namespace Snowflake;

class Value
{
	private $vals=[];
	
	function __construct($value,$data=[])
	{
		$this->vals['main_value'] = $value;
		
		foreach($data as $key=>$val)
		{
			$this->vals[$key] = $val;
		}
	}
	
	public function __toString()
    {
        return $this->vals['main_value'];
    }
	
	public function __get($name) 
    {
		return $this->vals[$name];
	}
	
	
	
}

?>