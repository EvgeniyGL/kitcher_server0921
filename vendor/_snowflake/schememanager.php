<?php
namespace Snowflake;

class SchemeManager
{
	private static $location="";
	private static $schemes=[];
	
	
	public static function location($path)
	{
		self::$location = $path;
	}
	
	public static function get($schemeName)
	{
		if (!isset(self::$schemes[$schemeName]))
		{
			//print "[-{$schemeName}-]";
			self::$schemes[$schemeName] = new SchemeObject($schemeName);
		}		
		
		return self::$schemes[$schemeName];
	}
	
	
}




?>