<?php
namespace Snowflake;

class Request
{
	static function url($item=-1)
	{
		$url = explode("?",$_SERVER['REQUEST_URI'])[0];			//only path
		if (substr($url,0,1)=="/"){$url = substr($url,1);}	//no lead slash
		if (substr($url,-1,1)=="/"){$url = substr($url,0,-1);}	//no end slash
		
		if ($item==-1)
		{
			return $url;
		}
		else
		{
			$urls = explode("/",$url);
			return $urls[$item]??"";
		}
	}
}

?>