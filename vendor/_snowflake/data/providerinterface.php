<?php
namespace Snowflake\Data;

interface ProviderInterface
{
	function __construct($host,$database,$user,$pass,$opts);
	function  add($table,$data);		// insert
	function edit($table,$query,$data);	// update
	function kill($table,$query);		// delete
	function find($table,$query,$opts);	// select by filter/query
	function  get($table,$query);		// select by id/uid/oid
	function command($command,$opts);	
}

?>