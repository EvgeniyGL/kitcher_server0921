<?php
namespace Snowflake\Data;

class MysqlProvider
{
	private $pdo;
	private $currentTable="";
	private $currentTableScheme=[];
	
	function __construct($host,$user,$pass,$base,$dataobj=null)
	{
		if ($dataobj!=null)
		{
			$this->with($dataobj);
		}
		
		
		try 
		{
			$this->pdo = new \PDO("mysql:host={$host};dbname={$base};charset=UTF8", $user, $pass);
			$this->pdo->exec("set names utf8");
		} 
		catch (PDOException $e) 
		{
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
	}
	
	
	function sql($sql,array $params=[])
	{	
		/*if (is_array($params))
		{
			
		}
		else
		{
			return $this->pdo->exec($sql);
		}*/
		
		$sth = $this->pdo->prepare($sql);
		$sth->execute($params);
		//$sth->setAttribute(\PDO::FETCH_ASSOC);
		return $sth;
	}
	
	
	function with($dataobj)
	{
		if (is_array($dataobj))
		{
			$this->currentTable = $dataobj['dbtable'];
			$this->currentTableScheme = $dataobj;
			
		}
		else
		{
			$this->currentTable = $dataobj;
			$this->currentTableScheme = [];
		}
		
		return $this;
	}
	
	
	function setJsonFields($table,$id,array $jsonVals,$overwrite=false)
	{
		$sql = "SELECT * FROM `{$table}` WHERE id=".$id;
		$res = $this->pdo->query($sql, \PDO::FETCH_ASSOC);
		$row = $res->fetch();
		
		$data=[];
		foreach($jsonVals as $field=>$newVals)
		{	
			if (isset($row[$field]))
			{
				$old = json_decode($row[$field],true);
				
				if (!is_array($old)){$old=[];}
				
				$data[$field] = array_merge($old, $newVals);
			}
		}
		
		
		$sql = "UPDATE `{$table}` SET ";
		$cm="";
		foreach($data as $col=>$obj)
		{
			$sql.=$cm."`{$col}`='".json_encode($obj,JSON_UNESCAPED_UNICODE)."'";
			$cm=",";
		}
		$sql.= " WHERE id=".$id;
		
		//print $sql;
		$res = $this->sql($sql);
		
		return $res->rowCount();
	}
	
	
	function add($a,$b=null)
	{
		if (is_array($b)){	
			if (is_array($a)){$table = $a['dbtable'];	$scheme = $a;	}
			else			 {$table = $a;			$scheme = [];		}
			$data = $b;
		}else{
			$table = $this->currentTable;	$data = $a;		$scheme = [];
		}
		
		$fields=[];
		$values=[];	
		$jsonVals=[];
		
		

		foreach($data as $field=>$val)
		{
			if (strpos($field,".")!=false)
			{
				$jsonVals[$field] = $val;
			}
			else
			{
				$fields[] = $field;
				
				
				if (substr($val,0,1)=="^" || substr($val,0,1)=="~")
				{
					$val = substr($val,1);
					$values[] = $val;
				}
				else
				{
					$values[] = "'".$val."'";
				}
				
			}
		}
		
		$sql = "INSERT INTO `{$table}` (".implode(",",$fields).") VALUES(".implode(",",$values).")";
		print $sql;
		$this->sql($sql);
		
		$iid = (int)$this->pdo->lastInsertId();
		
		if ($iid!=0 && count($jsonVals)>0)
		{
			$cnt = $this->setJsonFields($table,$iid,$jsonVals);
		}
		
		return $iid; 
	}
	
	
	function edit($a,$b,$c=null)	// id,[data] or table,id,[data]
	{
		if (is_array($c)){	
			if (is_array($a)){$table = $a['dbtable'];	$scheme = $a;	}
			else			 {$table = $a;			$scheme = [];		}
			$data = $c;
			$id   = $b;
		}else{
			$table = $this->currentTable;	$id = $a;	$data = $b; 		$scheme = [];
		}
		
		$fields=[];
		$values=[];	
		$jsonVals=[];
		
		
		
		//print "<HR>";
		//print "==";
		//print_r($data);

		foreach($data as $field=>$val)
		{
			if (strpos($field,".")!=false)
			{
				$jsonVals[$field] = $val;
			}
			else
			{
				$fields[$field] = $val;
			}
		}
		
		$sql = "UPDATE `{$table}` SET ";
		$cm="";
		foreach($fields as $key=>$val)
		{
			if (substr($val,0,1)=="^" || substr($val,0,1)=="~")
			{
				$val = substr($val,1);
				$sql.=$cm."`{$key}`=".$val."";
			}
			else
			{
				$sql.=$cm."`{$key}`='".$val."'";
			}
			
			
			$cm=",";
		}
		$sql.= " WHERE id=".$id;
				
		//print "<pre style='border:1px solid green;'>";			
		//print $sql;
		//print "</pre>";
				
		$res = $this->sql($sql);
		
		
		if (count($jsonVals)>0)
		{
			$cnt = $this->setJsonFields($table,$id,$jsonVals);
		}
		
		//return $res->rowCount(); 
		return new \Snowflake\Value($res->rowCount(),[
			'from'=>'mysqlprovider/edit',
			'sql' => $sql,
		]);
	}
	
	
	function kill($a,$b=null)
	{
		if ($b!=null){	
			if (is_array($a)){$table = $a['dbtable'];	$scheme = $a;	}
			else			 {$table = $a;			$scheme = [];		}
			$id = $b;
		}else{
			$table = $this->currentTable;	$id = $a;		$scheme = [];
		}
		
		$sql = "DELETE FROM {$table} WHERE id=".$id;
		$res = $this->sql($sql);
		return $res->rowCount(); 
	}
	
	
	function get($a,$b=null)
	{
		if ($b!=null){	
			if (is_array($a)){$table = $a['dbtable'];	$scheme = $a;	}
			else			 {$table = $a;			$scheme = [];		}
			$id = $b;
		}else{
			$table = $this->currentTable;	$id = $a;		$scheme = [];
		}
		
		$sql = "SELECT * FROM {$table} WHERE id=".$id;
		$res = $this->pdo->query($sql, \PDO::FETCH_ASSOC);
		
		if ($res!=false && $res->rowCount()>0)
		{
			$row = $res->fetch();
			foreach($row as $key=>$val)
			{
				if (substr($key,-1,1)==".")
				{
					$row[$key] = (array)json_decode($val);
				}
				elseif ($key=="coordinates")
				{
					$csql = "SELECT X(`coordinates`) as `x`,Y(`coordinates`) as `y` FROM {$table} WHERE id=".$id;
					$cres = $this->pdo->query($csql, \PDO::FETCH_ASSOC);
					$crow = $cres->fetch();
					
					$row[$key] = $crow['x']." ; ".$crow['y'];
				}
			}
			return $row; 
		}
		else
		{
			return false;
		}
		
		
	}
	
	
	function selectGenerator($sql)
	{
		$res = $this->pdo->query($sql, \PDO::FETCH_ASSOC);
		if ($res!=false)
		{
			while ($row = $res->fetch())
			{
				foreach($row as $key=>$val)
				{
					if (substr($key,-1,1)==".")
					{
						$row[$key] = (array)json_decode($val);
					}
				}
				
				yield $row;
			}
		}
	}
	
	
	function where($a="",$b=null)
	{
		if (is_null($b))
		{
			$table = $this->currentTable;
			$where = trim($a);
		}
		else
		{
			$table = $a;
			$where = trim($b);
		}
			 
		$sql = "SELECT * FROM {$table} ";	
		
		if ($where!="")
		{
			$f5 = strtoupper(substr($where,0,5));
			if ($f5=='ORDER' || $f5=="LIMIT" || $f5=="GROUP" || $f5=="WHERE")
			{
				$sql.= $where;
			}
			else
			{
				$sql.= "WHERE ".$where;
			}
		}
		
		return $this->selectGenerator($sql);
	}
	
	
	function countWhere($a="",$b=null)
	{
		if (is_null($b))
		{
			$table = $this->currentTable;
			$where = trim($a);
		}
		else
		{
			$table = $a;
			$where = trim($b);
		}
		
		$sql = "SELECT COUNT(*) AS `cnt` FROM {$table} ";	
		
		if ($where!="")
		{
			$f5 = strtoupper(substr($where,0,5));
			if ($f5=='ORDER' || $f5=="LIMIT" || $f5=="GROUP" || $f5=="WHERE")
			{
				$sql.= $where;
			}
			else
			{
				$sql.= "WHERE ".$where;
			}
		}
		
		$res = $this->sql($sql);
		$row = $res->fetch();
		return (int)$row['cnt'];
	}
	
	
	
	
	function find($a="",$b=null)
	{
		if ($b!=null){	
			if (is_array($a)){$table = $a['dbtable'];	$scheme = $a;	}
			else			 {$table = $a;			$scheme = [];		}
			$filter = $b;
		}else{
			$table = $this->currentTable;	$filter = $a;		$scheme = [];
		}
		
		
		
		$sql = "SELECT * FROM {$table}";
		
		if (is_array($filter)) // todo: PDO placeholders!!!
		{
			if (count($filter)>0)
			{
				$sql.= " WHERE ";
				
				$cm="";
				foreach($filter as $key=>$val)
				{
					$sql.=$cm."`{$key}`='{$val}'";
					$cm=" AND ";
				}
			}
		}
		else
		{
			$filter = trim($filter);
			if ($filter!="")
			{
				$f5 = strtoupper(substr($filter,0,5));
				if ($f5=='ORDER' || $f5=="LIMIT" || $f5=="GROUP" || $f5=="WHERE")
				{
					$sql.= " ".$filter;
				}
				else
				{
					$sql.= " WHERE ".$filter;
				}
			}
		}
		
		//print $sql."<BR>";
		
		$res = $this->pdo->query($sql, \PDO::FETCH_ASSOC);
		if ($res!=false)
		{
			while ($row = $res->fetch())
			{
				foreach($row as $key=>$val)
				{
					if (substr($key,-1,1)==".")
					{
						$row[$key] = (array)json_decode($val);
					}
				}
				
				yield $row;
			}
		}
	}
	
	
	function findOne($a="",$b=null)
	{
		$res = $this->find($a,$b);
		return $res->current();
	}
	
	
	function meta()
	{
		$res = $this->pdo->query("SELECT * FROM `{$this->currentTable}` LIMIT 0");
		if ($res==false)
		{
			return false;
		}
		else
		{
			$data=[];
			for ($i = 0; $i < $res->columnCount(); $i++) 
			{
				$col = $res->getColumnMeta($i);
				$data[$col['name']] = "";
			}
			return $data;
		}
	}
	
	
	
	
}

?>