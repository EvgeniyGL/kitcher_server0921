<?php
namespace Snowflake\Data;


class MysqlProvider implements \Snowflake\Data\ProviderInterface
{
	public $relative=true;
	
	private $pdo;
	private $database;	
	private $checks=[];
	
	private $opts = [];
	private $mariaDB=null;
	
	
	function __construct($host,$database,$user,$pass,$opts=[]){	
		if ($database==""){die("MysqlProvider:No database specified!");}
		$this->database = $database;
		
		$this->opts['autoinstall']  = true;
	
		try {
			$this->pdo = new \PDO("mysql:host={$host};dbname={$database};charset=UTF8", $user, $pass);
			$this->pdo->exec("set names utf8");
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
	}
	
	
	function queryBuilder($query,$opts=[])
	{
		$sql="";
		$vals=[];
		
		//var_dump($query);
		
		if ($query=="")
		{
			
		}
		elseif(is_numeric($query))
		{
			print "!!!";
			$sql = " WHERE `id`=:id";
			$vals['id'] = $query;
		}
		elseif (is_array($query))
		{
			if (count($query)>0)
			{
				$sql.= " WHERE ";
				
				$cm="";
				foreach($query as $key=>$val)
				{
					$logic = "=";
					$lk = substr($key,-1,1);
					if ($lk=="%")
					{
						$logic = " LIKE ";
						$val = "%{$val}%";
						$key = substr($key,0,-1);
					}

					
					$sql.=$cm."`{$key}`{$logic}:{$key}";
					$cm=" AND ";
					
					$vals[$key] = $val;
				}
			}
		}
		elseif ( preg_match('/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i', $query) )
		{	//UID
			$sql = " WHERE `uid`=:uid";
			$vals['uid'] = $query;
		}
		else
		{
			//ERROR ?
			die("ERROR: MysqlProvider::queryBuilder!");
		}
		
		
		/* $query = trim($query);
		if ($query!="")
		{
			$f5 = strtoupper(substr($query,0,5));
			if ($f5=='ORDER' || $f5=="LIMIT" || $f5=="GROUP" || $f5=="WHERE")
			{
				$sql.= " ".$query;
			}
			else
			{
				$sql.= " WHERE ".$query;
			}
		} */
		
		return (object)[
			'sql' =>$sql,
			'vals'=>$vals,
		];
	}
	
	
	function add($table,$data){	
	
		//print_r($data);
		$this->check($table);

	
		$fields=[];
		$values=[];	
		$jsonVals=[];
		
		foreach($data as $field=>$val)
		{
			if (strpos($field,".")!=false){
				$jsonVals[$field] = $val;
			}else{
				$fields[] = $field;
				if (substr($val,0,1)=="^" || substr($val,0,1)=="~")
				{
					$val = substr($val,1);
					$values[] = $val;
				}
				else
				{
					$values[] = "'".$val."'";
				}
			}
		}
		
		$sql = "INSERT INTO `{$table}` (".implode(",",$fields).") VALUES(".implode(",",$values).")";
		//print $sql."<HR>";
		
		$this->sql($sql);
		$iid = (int)$this->pdo->lastInsertId();
		if ($iid!=0 && count($jsonVals)>0){
			$cnt = $this->setJsonFields($table,$iid,$jsonVals);
		}
		
		return $iid; 
	}
	
	
	function check($table)
	{
		if (!isset($checks[$table]))
		{
			try {
				$result = $this->pdo->query("SELECT 1 FROM {$table} LIMIT 1");
			} catch (Exception $e) {
				return false;
			}

			$checks[$table] = ($result!==false);

			if (!$checks[$table] && $this->opts['autoinstall']==true)
			{
				$this->install($table);
				
				try {
					$result = $this->pdo->query("SELECT 1 FROM {$table} LIMIT 1");
				} catch (Exception $e) {
					return false;
				}
				
				$checks[$table] = ($result!==false);
			}
			
		}
		
		return $checks[$table];
	}
	
	function command($command,$params=[])
	{
		
	}
	
	
	
	function decodeFields($opts)
	{
		$opts = trim($opts);
		
		$mysqltype="";
		
		if (substr($opts,0,1)=="$")	// one2many reference to another table (foreign key by id)
		{
			$refTable = trim(substr($opts,1));
			return [
				'type'		=> "bigint DEFAULT '0' COMMENT '".'$'.$refTable."'",
				'reftable'  => $refTable,
			];
		}
		elseif (substr($opts,0,1)=="*")	// reference list to another tables (users#1,nomenclature#127,sales?date=01.01.2020)
		{
			$refObjects = trim(substr($opts,1));
			if ($refObjects==""){$refObjects="";}
			return [
				'type'		=> "text COMMENT '".'*'.$refObjects."'",
				'refobjects'  => $refObjects,
			];
		}
		elseif (substr($opts,0,1)=="{")	// json object
		{
			$opts = substr($opts,1);
			if (substr($opts,-1,1)=="}")
			{
				$opts = substr($opts,0,-1);
			}
			
			return [
				'type'		=> "JSON COMMENT '{{$opts}}'",
				'jsonfields'  => $opts,
			];
		}
		else
		{
			$opts = explode(" ",$opts);
			
			$res=[];
			$add_ai="";
			
			if (in_array("ai",$opts)){$res['auto_increment']=true; $res['primary_key']=true; $add_ai = " AUTO_INCREMENT"; }
			if (in_array("pk",$opts)){$res['primary_key']=true;}
			
			
			$msql = $this->mysqlType($opts[0]);
			
			$res['type'] = $msql['mysqltype'].$add_ai;
			
			return $res;
		}
		
		
	}
	
	
	
	function mysqlType($type)
	{
		$type = strtolower(trim($type));
		
		$pts = explode("=",$type);
		
		$type = $pts[0];
		$default = trim($pts[1]??"");
		
		$resType="";
		$resDefault="";
		$resTrigger="";
		
		if (substr($type,0,6)=="string"){
			$size = (int)substr($type,6);
			if ($size==0){$size=100;}
			$resType = "varchar({$size})";
		}elseif (in_array($type,['ai','int','bigint','smallint'])){
			if ($type=='ai'){
				$resType = "bigint";
			}else{
				$resType = $type;
			}
		}elseif (in_array($type,['html'])){
			$resType = "text";
		}elseif ($type=='phone'){
			$resType = "varchar(50)";
		}elseif ($type=='email'){
			$resType = "varchar(100)";
		}elseif (in_array($type,['coordinates','point'])){
			$resType = "POINT";
		}elseif ($type=='file'){
			$resType = "varchar(200)";
		}elseif ($type=='files'){
			$resType = "text";
		}elseif ($type=='password'){
			$resType = "varchar(100)";
		}elseif (in_array($type,['guid','uuid','uid'])){
			$resType = "varchar(50)";
		}elseif ($type=='orderposition'){
			$resType = 'int';
		}elseif (substr($type,0,1)=='['){
			$resType = 'tinyint';
			$default = 0;
		}
		elseif ($type=='data')
		{
			//$mtype = 'text';
			$resType = 'text';
		}
		else
		{
			$resType = $type;
		}
		
		
		if ($default!=="")
		{		
			if (substr($default,0,1)=="(")
			{
				//todo procs and triggers!
				$resDefault = "";
				
				$dtrig = substr($default,1);
				if (substr($dtrig,-1,1)==")") {$dtrig = substr($dtrig,0,-1);}
				
				$tps = explode(">",$dtrig);
				
				if (count($tps)==1)
				{
					$tFunc = strtolower(trim($tps[0]));
					$tCond = "";
				}
				else
				{
					$tCond = strtolower(trim($tps[0]));
					$tFunc = strtolower(trim($tps[1]));
				}
				
				if ($tFunc=="now")
				{
					if ($this->mariaDB==true)
					{
						if ($tCond=="")
						{
							$resDefault = " NOT NULL DEFAULT CURRENT_TIMESTAMP";
						}
						elseif ($tCond=="onedit")
						{
							$resDefault = " NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP";
						}
					}
				}
				elseif ($tFunc=="uuid" || $tFunc=="guid" )
				{
					if ($this->mariaDB==true)
					{
						$resDefault = " NOT NULL DEFAULT UUID()";
					}
				}
				
			}
			elseif (substr($default,0,1)=="[")
			{
				$resDefault = " NOT NULL DEFAULT 0";
			}
			else
			{
				$resDefault = " NOT NULL DEFAULT '{$default}'";
			}
		}
		

		return [
			'mysqltype' => $resType.$resDefault,
			'trigger'	=> $resTrigger,
		];
		
		
		
		
	}
	
	
	
	function isMaria()
	{
		$res = $this->sql("SELECT COUNT(*) as `cnt` FROM information_schema.ALL_PLUGINS WHERE PLUGIN_NAME='Aria'");
		$row = $res->fetch();
		$cnt = $row['cnt']??0;
		return (bool)$cnt;
	}
	
	
	function install($table)
	{
		$scheme = \Snowflake\SchemeManager::get($table);
		//print "<pre>";
		//print_r($scheme);
		//print "</pre>";
		
		if (is_null($this->mariaDB))
		{
			$this->mariaDB = $this->isMaria();
		}
		
		
		$fields = $scheme->fields;
		
		if (isset($fields['uid']) && !isset($fields['id']))
		{
			$aid['id'] = 'ai';
			if (isset($fields['puid']) && !isset($fields['pid']))
			{
				$aid['pid'] = "bigint=0";
			}			
			
			$fields = $aid+$fields;
		}
		
		
		
		//print "<pre>";
		//print_r($fields);
		//print "</pre>";
		
		$sql = "CREATE TABLE IF NOT EXISTS `{$table}` (";
			$cm="";
			$PK = "";
			foreach($fields as $field=>$opts)
			{
				$opts = $this->decodeFields($opts);
				
				if (substr($opts['type'],0,4)=="JSON")
				{
					$field = $field.".";
				}
				
				$sql.= $cm."\n`{$field}` {$opts['type']} ";
				
				if (isset($opts['primary_key']) && $opts['primary_key']==true)
				{
					$PK = ",\n PRIMARY KEY (`{$field}`)";
				}
				
				
				$cm = ",";
			}
			
		$sql.=$PK;
		$sql.= ")";
		
		//print "<pre>";
		//print $sql;
		//print "</pre>";
		
		$this->sql($sql);
		
		return "<pre>{$sql}</pre>";
	}
	
	
	function sql($sql,array $params=[])
	{	
		print $sql."<HR>";
	
		$sth = $this->pdo->prepare($sql);
		$sth->execute($params);
		//$sth->setAttribute(\PDO::FETCH_ASSOC);
		
		//print "<pre>";		print_r( $this->pdo->errorInfo());
		
		return $sth;
	}
	
	

	
	function setJsonFields($table,$id,array $jsonVals,$overwrite=false)
	{
		$sql = "SELECT * FROM `{$table}` WHERE id=".$id;
		$res = $this->pdo->query($sql, \PDO::FETCH_ASSOC);
		$row = $res->fetch();
		
		$data=[];
		foreach($jsonVals as $field=>$newVals)
		{	
			if (isset($row[$field]))
			{
				$old = json_decode($row[$field],true);
				
				if (!is_array($old)){$old=[];}
				
				$data[$field] = array_merge($old, $newVals);
			}
		}
		
		
		$sql = "UPDATE `{$table}` SET ";
		$cm="";
		foreach($data as $col=>$obj)
		{
			$sql.=$cm."`{$col}`='".json_encode($obj,JSON_UNESCAPED_UNICODE)."'";
			$cm=",";
		}
		$sql.= " WHERE id=".$id;
		
		//print $sql;
		$res = $this->sql($sql);
		
		return $res->rowCount();
	}
	
	
	
	
	
	function edit($table,$query,$data)	// id,[data] or table,id,[data]
	{
		$fields=[];
		$values=[];	
		$jsonVals=[];

		foreach($data as $field=>$val)
		{
			if (strpos($field,".")!=false){
				$jsonVals[$field] = $val;
			}else{
				$fields[$field] = $val;
			}
		}
		
		$sql = "UPDATE `{$table}` SET ";
		$cm="";
		foreach($fields as $key=>$val){
			if (substr($val,0,1)=="^" || substr($val,0,1)=="~")	{
				$val = substr($val,1);
				$sql.=$cm."`{$key}`=".$val."";
			}else{
				$sql.=$cm."`{$key}`='".$val."'";
			}
			$cm=",";
		}
		
		if (is_array($query)){
			$sql.= ""; // TODO SqlBuilder !!!!!
		}else{
			if (is_numeric($query)){
				$sql.= " WHERE id=".$query;
			}else{
				$sql.= " WHERE uid='".$query."'";
			}
			
		}
		
		$res = $this->sql($sql);
		
		if (count($jsonVals)>0){
			$cnt = $this->setJsonFields($table,$id,$jsonVals);
		}
		
		return $res->rowCount();
	}
	
	
	function kill($table,$query)
	{
		if (is_numeric($query))	{
			$sql = "DELETE FROM {$table} WHERE id=".$query;
		}else{
			$sql = "DELETE FROM {$table} WHERE uid='".$query."'";
		}
		
		$res = $this->sql($sql);
		return $res->rowCount(); 
	}
	
	
	function find($table,$query=[],$opts=[])
	{	
		$this->check($table);
	
		//print "~FIND~";
		$query = $this->queryBuilder($query,$opts);
		$sql = "SELECT * FROM {$table} ".$query->sql;
		//print $sql."<BR>";
		//print_r($query->vals);
		$sth = $this->pdo->prepare($sql);
		$res = $sth->execute($query->vals);
		if ($res!=false){
			$sth->setFetchMode(\PDO::FETCH_ASSOC);
			while ( $row = $sth->fetch() ){
				foreach($row as $key=>$val)	{
					if (substr($key,-1,1)==".")	{
						$row[$key] = (array)json_decode($val);
					}
				}
				yield $row;
			}
		}
	}
	
	
	function get($table,$idx)
	{
		if (is_array($idx))
		{
			$query = $idx;
		}
		else
		{
			$idx = trim($idx);
			
			if ($idx=="" || $idx==0)
			{
				return [];
			}
			elseif (is_numeric($idx))
			{
				if ($idx<=0)
				{
					return [];
				}
				else
				{
					$query = ['id'=>$idx];
				}
			}
			else
			{
				$query = ['uid'=>$idx];
			}
		}
		
		
		$res = $this->find($table,$query,['LIMIT'=>1]);
		return $res->current();
	}
	
	
	
}

?>