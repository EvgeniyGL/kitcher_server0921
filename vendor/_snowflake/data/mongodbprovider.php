<?php
namespace Snowflake\Data;

class MongodbProvider implements \Snowflake\Data\ProviderInterface
{
	public $relative=false;
	
	private $mongoManager;
	private $database;
	
	
	
	function __construct($host,$database,$user="",$pass="",$opts=[]){
		if ($database==""){die("MongodbProvider:No database specified!");}
		$this->database = $database;
		if (strpos($host,':')===false)	{	$host.=":27017";}
				
		try	{
			$this->mongoManager = new \MongoDB\Driver\Manager("mongodb://{$host}");	//mongodb://username:password@localhost/test
		}catch (\MongoDB\Driver\Exception\Exception $e)	{
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
	}
	
	
	function add($table,$data){	
		
		$bulk = new \MongoDB\Driver\BulkWrite();
		$insertedId = $bulk->insert($this->dataPrepare($data));
		//$writeConcern = new \MongoDB\Driver\WriteConcern(\MongoDB\Driver\WriteConcern::MAJORITY, 100);
		$result = $this->mongoManager->executeBulkWrite($this->database.".".$table, $bulk); //, $writeConcern

		if ($result->getInsertedCount()>0){
			return (string)$insertedId;
		}else{
			return false;
		}
	}
	
	
	function edit($table,$query,$data){	
		$bulk = new \MongoDB\Driver\BulkWrite();	
		$bulk->update($this->queryPrepare($query),['$set' => $this->dataPrepare($data)],['upsert' => false]); //'multi' => false,
		//$writeConcern = new \MongoDB\Driver\WriteConcern(\MongoDB\Driver\WriteConcern::MAJORITY, 100);
		$result = $this->mongoManager->executeBulkWrite($this->database.".".$table, $bulk); //, $writeConcern
		return $result->getModifiedCount();
	}
	
	
	function kill($table,$query){
		$bulk = new \MongoDB\Driver\BulkWrite();
		$bulk->delete($this->queryPrepare($query));
		//$writeConcern = new \MongoDB\Driver\WriteConcern(\MongoDB\Driver\WriteConcern::MAJORITY, 100);
		$result = $this->mongoManager->executeBulkWrite($this->database.".".$table, $bulk); //, $writeConcern
		return $result->getDeletedCount(); 
	}
	
	
	function find($table,$query=[],$opts=[]){
		$pQuery = $this->queryPrepare($query);
		
		
		
		$mongoQuery = new \MongoDB\Driver\Query($pQuery);
		//print "<pre>";
		//print_r($mongoQuery);
		
		$rows   = $this->mongoManager->executeQuery($this->database.".".$table, $mongoQuery);
		
		foreach ($rows as $doc) 
		{
			$r = (array)$doc;
			if (isset($r['_id']))
			{
				$r['uid'] = (string)$r['_id'];
				unset($r['_id']);
			}
			
			yield $r;
		}
	}
	
	
	
	
	
	
	
	function get($table,$query){
		if ($query=="" || $query==0)
		{
			return [];
		}
		else
		{
			$res = $this->find($table,$query,['first'=>true]);
			$r = $res->current();
			if (is_null($r)){$r=false;}
			return $r;
		}
	}
	
	
	function command($command,$opts=[]){
		$cmd = strtolower(trim($command));
		if ($cmd=="count")
		{
			return $this->cmdCount($opts);
		}
		elseif ($cmd=="meta")
		{
			return false;
		}
		else
		{
			return false;
		}
	}
	
	
	// === PRIVATE COMMANDS ===
	
	private function cmdCount($opts){	
		$table = $opts['table'];
		$query = $opts['query'];
		
		$mongoQuery = new \MongoDB\Driver\Query($query); //$eq !!!!!!!
		$command = new \MongoDB\Driver\Command(["count" => $table, "query"=>$mongoQuery ]);
		$result = $this->mongoManager->executeCommand($this->database, $command);
		return (int)$result->toArray()[0]->n;;
	}
	
	private function cmdMeta($opts){
		$table = $opts['table'];
	
		return [];
	}
	
	
	
	
	
	private function queryPrepare($q){
		if ($q==""){$q=[];}
		
		if (is_array($q))
		{
			if(isset($q['uid']))
			{
				$oid = new \MongoDB\BSON\ObjectId($q['uid']);
				$q['_id'] = $oid;
				unset($q['uid']);
			}
			
			return $q;
		}
		else
		{
			return ['_id' => new \MongoDB\BSON\ObjectId($q)];
		}
	}
	
	private function dataPrepare($data){
		
		
		
		/* foreach($data as $key=>$val)
		{
			$data[$key] = (string)$val;
		} */
		
		
		
		return $data;
	}
	
	
	
	
	function rawQuery($table,$query=[],$opts=[]){
		//print $query;
		//$bson = \MongoDB\BSON\fromJSON($query);
		$mongoQuery = new \MongoDB\Driver\Query($query);
		
		
		//print "<pre>";
		//print_r($mongoQuery);
		
		$rows   = $this->mongoManager->executeQuery($this->database.".".$table, $mongoQuery);
		
		foreach ($rows as $doc) 
		{
			$r = (array)$doc;
			if (isset($r['_id']))
			{
				$r['uid'] = (string)$r['_id'];
				unset($r['_id']);
			}
			
			yield $r;
		}
	}
	
	
	
	/* function addEx($table,$data,$multiInsert=false)
	{		
		$bulk = new \MongoDB\Driver\BulkWrite();
		
		$ret=0;
		if (!$multiInsert)
		{
			print "singleInsert!";
			$pdata = $this->dataPrepare($data);
			print_r($pdata);
			$insertedId = $bulk->insert($pdata);
			$ret = (string)$insertedId;
		}
		else
		{
			//print "multiInsert!";
			//foreach($data as $item)
			//{
			//	$pdata = $this->dataPrepare($item);
			//	print_r($pdata);
			//	$bulk->insert($this->dataPrepare($pdata));
			//}
		}
		
		$writeConcern = new \MongoDB\Driver\WriteConcern(\MongoDB\Driver\WriteConcern::MAJORITY, 100);
		$result = $this->mongoManager->executeBulkWrite($this->database.".".$table, $bulk, $writeConcern); 
		
		if ($multiInsert)
		{
			$ret = $result->getInsertedCount();
		}
		 
		return $ret;
	} */
	
	

	
	
}

?>