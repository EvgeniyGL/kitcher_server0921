<?php
namespace Snowflake\Data;

class Manager
{
	public static $providers=[];
	
	public static function get($providerName)
	{
		$providerName = trim(strtolower($providerName));
		if (!isset(self::$providers[$providerName]))
		{
			require("db.config.php");
			
			$providerClass = "\\Snowflake\\Data\\".ucfirst($providerName)."Provider";
			
			self::$providers[$providerName] = new $providerClass(
				$db[$providerName]['host'],
				$db[$providerName]['base'],
				$db[$providerName]['user'],
				$db[$providerName]['pass'],
			);
		}
		
		return self::$providers[$providerName];
	}
	
	
	
	
	
	
	
	
	
	
	
}




?>