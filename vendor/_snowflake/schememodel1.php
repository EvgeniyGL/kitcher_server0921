<?php
namespace Snowflake;

class SchemeModel
{
	private $dbType="";
	
	private $pdo;
	private $app;
	//private $view;
	private $mariaDB=false;
	
	public $scheme;
	
	private $db;
	
	private $data;
	
	public $title;
	
	public $schemeFields=[];
	
	public $path="";
	
	private $opts=[];
	
	public $name="";
	
	function __construct($app,$modelName="",$opts=[])
	{
		$this->app = $app;
		//$this->view = $view;
		
		$this->name = $modelName;
		$this->opts = $opts;
			
		if (!isset($app->dataproviders['mysql']))
		{
			die("SchemeModel needs dataprovider:`mysql`");
		}
		else
		{
			$this->db = $app->dataproviders['mysql'];
		}
	
		if ($modelName=="")
		{
			$modelName = get_class($this);
		}
		
		
		$this->loadModelScheme($modelName);
		
		if ($this->dbType=="mysql")
		{
		
			$this->data = $this->db->with($this->scheme['dbtable']);
			
			$nocheck = $this->opts['nocheck']??false;
			if ($nocheck)
			{
				$this->install();
			}
			else
			{
				$this->checkTableExists();
			}
	
		}
		
		
		
	}
	
	
	function checkTableExists($dieOnError=false)
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		if ($this->meta()==false)
		{
			print_r($this->meta());
			die("Table for model `{$this->title}` doesn't exist!<hr>Try <a href='/install'>install</a>");
			
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function loadModelScheme($schemeName)
	{
		$file = strtolower($schemeName).".model.scheme";
		$path_a = "models/".strtolower($schemeName)."/";
		$path_b = "models/";
		
		$schemeFile="";
		if (file_exists($path_a.$file))
		{
			$this->path = $path_a;
			$schemeFile=$path_a.$file;
		}
		elseif (file_exists($path_b.$file))
		{
			$this->path = $path_b;
			$schemeFile=$path_b.$file;
		}
		else
		{
			die("SchemeModel({$schemeName}) can't find model scheme:{$schemeFile}");
		}
		
		
		$schemetext = file_get_contents($schemeFile);
		$lines = explode("\n",$schemetext);
		$aroot="";
		foreach($lines as $line)
		{
			$cmm = explode("//",$line);
			$line = $cmm[0];
			
			if (trim($line)!="")
			{
				$trimline = strtolower(trim($line));
				$items = explode(":",$trimline,2);
				if (substr($line,0,1)=="\t")
				{
					$key = trim($items[0]);
					$this->scheme[$aroot][$key] = trim($items[1]??"");
				}
				elseif (substr($line,0,6)=="dbtype")
				{
					$this->dbType = trim(explode(":",$line)[1]??"");
				}
				else
				{
					$aroot=trim($items[0]);
					if (isset($items[1]))
					{
						$this->scheme[$aroot] = trim($items[1]);
					}
					else
					{
						$this->scheme[$aroot] = [];
					}
					
				}
			}
		}
		
		
		
		$this->title = $this->scheme['title']??$this->scheme['dbtable'];
		
		$this->schemeFields = $this->scheme['tablefields'];
		
	
	}
	
	
	function isMaria()
	{
		$sql = "SELECT COUNT(*) as `cnt` FROM information_schema.ALL_PLUGINS WHERE PLUGIN_NAME='Aria'";
		$res = $this->db->sql($sql);
		$row = $res->fetch();
		$cnt = $row['cnt']??0;
		return (bool)$cnt;
	}
	
	function install()
	{
		$this->mariaDB = $this->isMaria();
		
		
		$sql = "CREATE TABLE IF NOT EXISTS `{$this->scheme['dbtable']}` (";
			$cm="";
			$PK = "";
			foreach($this->scheme['tablefields'] as $field=>$opts)
			{
				$opts = $this->decodeOpts($opts);
				
				if (substr($opts['type'],0,4)=="JSON")
				{
					$field = $field.".";
				}
				
				$sql.= $cm."\n`{$field}` {$opts['type']} ";
				
				if (isset($opts['primary_key']) && $opts['primary_key']==true)
				{
					$PK = ",\n PRIMARY KEY (`{$field}`)";
				}
				
				
				$cm = ",";
			}
			
		$sql.=$PK;
		$sql.= ")";
		

		
		$this->db->sql($sql);
		
		return "<pre>{$sql}</pre>";
	}
	
	
	function decodeOpts($opts)
	{
		$opts = trim($opts);
		
		$mysqltype="";
		
		if (substr($opts,0,1)=="$")	// one2many reference to another table (foreign key by id)
		{
			$refTable = trim(substr($opts,1));
			return [
				'type'		=> "bigint DEFAULT '0' COMMENT '".'$'.$refTable."'",
				'reftable'  => $refTable,
			];
		}
		elseif (substr($opts,0,1)=="*")	// reference list to another tables (users#1,nomenclature#127,sales?date=01.01.2020)
		{
			$refObjects = trim(substr($opts,1));
			if ($refObjects==""){$refObjects="";}
			return [
				'type'		=> "text COMMENT '".'*'.$refObjects."'",
				'refobjects'  => $refObjects,
			];
		}
		elseif (substr($opts,0,1)=="{")	// json object
		{
			$opts = substr($opts,1);
			if (substr($opts,-1,1)=="}")
			{
				$opts = substr($opts,0,-1);
			}
			
			return [
				'type'		=> "JSON COMMENT '{{$opts}}'",
				'jsonfields'  => $opts,
			];
		}
		else
		{
			$opts = explode(" ",$opts);
			
			$res=[];
			$add_ai="";
			
			if (in_array("ai",$opts)){$res['auto_increment']=true; $res['primary_key']=true; $add_ai = " AUTO_INCREMENT"; }
			if (in_array("pk",$opts)){$res['primary_key']=true;}
			
			
			$msql = $this->mysqlType($opts[0]);
			
			$res['type'] = $msql['mysqltype'].$add_ai;
			
			return $res;
		}
		
		
	}
	
	function mysqlType($type)
	{
		$type = strtolower(trim($type));
		
		$pts = explode("=",$type);
		
		$type = $pts[0];
		$default = trim($pts[1]??"");
		
		$resType="";
		$resDefault="";
		$resTrigger="";
		
		if (substr($type,0,6)=="string")
		{
			$size = (int)substr($type,6);
			if ($size==0){$size=100;}
			
			$mtype = "varchar({$size})";
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			
			$resType = $mtype;
		}
		elseif (in_array($type,['ai','int','bigint','smallint']))
		{
			if ($type=='ai')
			{
				$mtype = "bigint";
			}
			else
			{
				$mtype = $type;
			}
			
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			$resType = $mtype;
		}
		elseif (in_array($type,['html']))
		{
			$mtype = "text";
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			$resType = $mtype;
		}
		elseif ($type=='phone')
		{
			$mtype = "varchar(50)";
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			$resType = $mtype;
		}
		elseif ($type=='email')
		{
			$mtype = "varchar(100)";
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			$resType = $mtype;
		}
		elseif (in_array($type,['coordinates','point']))
		{
			$mtype = "POINT";
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			$resType = $mtype;
		}
		elseif ($type=='file')
		{
			$mtype = "varchar(200)";
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			$resType = $mtype;
		}
		elseif ($type=='files')
		{
			$mtype = "text";
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			$resType = $mtype;
		}
		elseif ($type=='password')
		{
			$mtype = "varchar(50)";
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			$resType = $mtype;
		}
		elseif (in_array($type,['guid','uuid','uid']))
		{
			$mtype = "varchar(50)";
			//if ($default!=""){$resDefault=" NOT NULL DEFAULT '{$default}'";}
			$resType = $mtype;
		}
		elseif ($type=='orderposition')
		{
			$mtype = 'int';
			$resType = 'int';
		}
		elseif (substr($type,0,1)=='[')
		{
			$mtype = 'tinyint';
			$resType = $mtype;
			$default = 0;
		}
		elseif ($type=='data')
		{
			$mtype = 'text';
			$resType = $mtype;
		}
		else
		{
			$resType = $type;
		}
		
		
		if ($default!="")
		{
			//print "( $default )";
			
			
			if (substr($default,0,1)=="(")
			{
				//todo procs and triggers!
				$resDefault = "";
				
				$dtrig = substr($default,1);
				if (substr($dtrig,-1,1)==")") {$dtrig = substr($dtrig,0,-1);}
				
				$tps = explode(">",$dtrig);
				
				if (count($tps)==1)
				{
					$tFunc = strtolower(trim($tps[0]));
					$tCond = "";
				}
				else
				{
					$tCond = strtolower(trim($tps[0]));
					$tFunc = strtolower(trim($tps[1]));
				}
				
				if ($tFunc=="now")
				{
					if ($this->mariaDB==true)
					{
						if ($tCond=="")
						{
							$resDefault = " NOT NULL DEFAULT CURRENT_TIMESTAMP";
						}
						elseif ($tCond=="onedit")
						{
							$resDefault = " NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP";
						}
					}
				}
				elseif ($tFunc=="uuid" || $tFunc=="guid" )
				{
					if ($this->mariaDB==true)
					{
						$resDefault = " NOT NULL DEFAULT UUID()";
					}
				}
				
			}
			elseif (substr($default,0,1)=="[")
			{
				$resDefault = " NOT NULL DEFAULT 0";
			}
			else
			{
				$resDefault = " NOT NULL DEFAULT '{$default}'";
			}
		}
		
		return [
			'mysqltype' => $resType.$resDefault,
			'trigger'	=> $resTrigger,
		];
		
		
		
		
	}
	
	
	function childs($idx,$reverse=false,$limit="1000")
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		if ($reverse==true)
		{
			$filter = "pid={$idx} ORDER by `id` DESC";
		}
		else
		{
			$filter = "pid={$idx} ORDER by `id` ASC";
		}
		
		if ($limit!="")
		{
			$filter.= " LIMIT ".$limit;
		}
		
		return $this->db->find($filter);
	}
	
	
	function childsOf($idx=0,$opts=[])
	{
		
		
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		if (isset($this->schemeFields['order']))
		{
			$filter = "pid={$idx} ORDER by `order` ASC";
		}
		else
		{
			
			if (isset($opts['filesort']))
			{
				if ($idx==0)
				{
					$filter = "pid={$idx} ORDER by type,id ASC";
				}
				else
				{
					$filter = "pid={$idx} ORDER by id ".$opts['filesort'];
				}
			}
			else
			{
				$filter = "pid={$idx} ORDER by type,id ASC";
			}
		}
		
		return $this->db->find($filter);
	}
	
	function paginatedList($pageNum,$pageSize=50,$where="")
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		$filter="";
		
		if ($where!="")
		{
			$filter.=" WHERE ".$where." ";
		}
		
		
		if (isset($this->schemeFields['order']))
		{
			$filter.= " ORDER BY `order` ASC";
		}
		else
		{
			$filter.= " ORDER BY id ASC";
		}
		
		return $this->db->find($filter);
	}
	

	
	function all($sortDesc=false,$limit=1000)
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		if ($sortDesc)
		{
			$filter = " ORDER BY id DESC";
		}
		else
		{
			$filter = " ORDER BY id ASC";
		}
		
		$limit = (int)$limit;
		if ($limit>0)
		{
			$filter.= " LIMIT ".$limit;
		}
		
		
		return $this->db->find($filter);
	}
	
	function sql($sql)
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		return $this->db->sql($sql);
	}
	
	function meta()
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		$meta = $this->db->meta();
		if ($meta==false)
		{
			return false;
		}
		else
		{
			$data = [];
			foreach($this->db->meta() as $col=>$type){
				$data[$col] = $this->schemeFields[$col]??"";
			}
			return $data;
		}
	}
	
	
	function get($idx=0)
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		if ($idx==0)
		{
			$data=[];
			$meta = $this->db->meta();
			//print_r($meta);
			
			$data['id'] = 0;
			
			if (isset($meta['pid']))
			{
				$data['pid'] = 0;
			}
			
			if (isset($meta['type']))
			{
				$data['type'] = 0;
			}
			
			return $data;
		}
		else
		{
			return $this->db->get($idx);
		}
	}
	
	function getParents($idx,$reverseOrder=false)
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		$data = [];
		
		$obj = $this->get($idx);
		if ($obj['id']>0)
		{
			$data[] = $obj;
		}
		while($obj['id']!=0)
		{
			$pid = $obj['pid']??0;

			$obj = $this->get($pid);
			
			
			if ($obj['id']>0)
			{
				$data[] = $obj;
			}
		}
		
		if ($reverseOrder)
		{
			return $data;
		}
		else
		{
			return array_reverse($data);
		}
		
		
	}
	
	
	function add($data) 
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		unset($data['created']);
		unset($data['updated']);
		
		if (isset($this->schemeFields['created']))
		{
			$data['created'] = "~NOW()";
		}
		
		if (isset($this->schemeFields['updated']))
		{
			$data['updated'] = "~NOW()";
		}
		
		if (isset($this->schemeFields['uid']))
		{
			$data['uid'] = "~UUID()";
		}
		
		
		unset($data['order']);
		
		unset($data['files']); //summernote problem
		
		//print_r($data);
		
		foreach($data as $key=>$val)
		{
			if ($val=="")
			{
				unset($data[$key]);
			}
		}
		
		
		if (isset($data['coordinates']))
		{
			$coord = trim($data['coordinates']);
			$coord = str_replace(";",",",$coord);
			$coord = str_replace("  "," ",$coord);
			$coord = str_replace("  "," ",$coord);
			$data['coordinates'] = "^POINT({$coord})";
		}
		
		$id = $this->db->add($data);
		
		if (isset($this->schemeFields['order']))
		{
			if (isset($this->schemeFields['order']))
			{
				$this->db->edit($id,[
					'order'=>$id
				]);
			}
			else
			{
				
			}
			
		}
		
		return $id;
	}
	
	function edit($id,$data)
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		if (isset($data['files']))
		{
			$t = $this->db->get($id);
			$files = trim($t['files']);
			if ($files!="")
			{
				$files.=";";
			}
			
			$files.=$data['files'];
			
			$data['files'] = $files;
		}
		
		
		if (isset($data['coordinates']))
		{
			$coord = trim($data['coordinates']);
			$coord = str_replace(";",",",$coord);
			$coord = str_replace("  "," ",$coord);
			$coord = str_replace("  "," ",$coord);
			$data['coordinates'] = "^POINT({$coord})";
		}
		
		//print "<pre style='border:1px solid red;'>";
		//print_r($this->schemeFields);
		//print "</pre>";
		
		if (isset($this->schemeFields['updated']))
		{
			$data['updated'] = "~NOW()";
		}
		
		unset($data['addfiles']);
		
		//print "<pre style='border:1px solid green;'>";
		//print_r($data);
		//print "</pre>";
		
		$this->db->edit($id,$data);
	}
	
	function kill($id)
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		$this->db->kill($id);
	}
	
	
	function orderMove($id,$method)
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		$a = $this->db->get($id);
		
		$a_id  = $a['id'];
		$a_pos = $a['order'];
		
		if ($method=='up')
		{
			$filter = "WHERE `order`<{$a_pos} ORDER BY `order` DESC LIMIT 1";
		}
		else
		{
			$filter = "WHERE `order`>{$a_pos} ORDER BY `order` ASC LIMIT 1";
		}
		
		$b = $this->db->findOne($filter);
		if ($b!=null)
		{
			$b_id  = $b['id'];
			$b_pos = $b['order'];
			
			$this->db->edit($a_id,[
				'order'=>$b_pos,
			]);
			
			$this->db->edit($b_id,[
				'order'=>$a_pos,
			]);
			
			print $a_id.";".$b_id;
		}
		else
		{
			print "NO";
		}
		
	}
	
	
	function search($txt)
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		if (is_numeric($txt))
		{
			$filter = "WHERE id={$txt}";
		}
		else
		{
			$filter = "WHERE name LIKE '%{$txt}%' ORDER BY id DESC LIMIT 500";
		}
		
		return $this->db->find($filter);
	}
	
	function find($filter)
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		return $this->db->find($filter);
	}
	
	function findOne($filter)
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		return $this->db->findOne($filter);
	}
	
	
	function where($where)
	{
		return $this->db->where($this->scheme['dbtable'],$where);
	}
	
	function countWhere($where="")
	{
		return $this->db->countWhere($this->scheme['dbtable'],$where);
	}
	
}


?>