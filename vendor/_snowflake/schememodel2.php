<?php
namespace Snowflake;

class SchemeModel2
{
	//private $standAlone=true;
	//private $pdo;
	//private $app=null;
	//private $view;
	//private $mariaDB=false;
	//public $schemeObject;
	
	//private $data;
	//public $schemeFields=[];
	//public $path="";
	//private $opts=[];
	//public $name="";
	
	
	public $scheme;
	public $provider;
	
	function __construct($scheme,$provider)
	{		
		$this->scheme = $scheme;
		$this->provider = $provider;
	}
	
	
	function install()
	{
		if ($this->scheme->dbtype=="mysql")
		{
			//$this->mariaDB = $this->isMaria();		
			$sql = "CREATE TABLE IF NOT EXISTS `{$this->scheme->dbtable}` (";
				$cm="";
				$PK = "";
				foreach($this->scheme->fields as $field=>$opts)
				{
					$opts = \Snowflake\Schemeutils::decodeOpts($opts);
					
					if (substr($opts['type'],0,4)=="JSON")
					{
						$field = $field.".";
					}
					
					$sql.= $cm."\n`{$field}` {$opts['type']} ";
					
					if (isset($opts['primary_key']) && $opts['primary_key']==true)
					{
						$PK = ",\n PRIMARY KEY (`{$field}`)";
					}
					
					
					$cm = ",";
				}
				
			$sql.=$PK;
			$sql.= ")";
			
			//print $sql;
			
			$this->provider->sql($sql);
			
			return "<pre>{$sql}</pre>";
		}
	}
	
	
	public function __get($name)
	{	
		return $this->scheme->{$name};
	}
	
	
	
	/* function checkTableExists($dieOnError=false)
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		if ($this->meta()==false)
		{
			print_r($this->meta());
			die("Table for model `{$this->title}` doesn't exist!<hr>Try <a href='/install'>install</a>");
			
			return false;
		}
		else
		{
			return true;
		}
	} */
	
	/* function loadModelScheme($schemeName)
	{
		$file = strtolower($schemeName).".model.scheme";
		$path_a = "models/".strtolower($schemeName)."/";
		$path_b = "models/";
		
		$schemeFile="";
		if (file_exists($path_a.$file))
		{
			$this->path = $path_a;
			$schemeFile=$path_a.$file;
		}
		elseif (file_exists($path_b.$file))
		{
			$this->path = $path_b;
			$schemeFile=$path_b.$file;
		}
		else
		{
			die("SchemeModel({$schemeName}) can't find model scheme:{$schemeFile}");
		}
		
		
		$schemetext = file_get_contents($schemeFile);
		$lines = explode("\n",$schemetext);
		$aroot="";
		foreach($lines as $line)
		{
			$cmm = explode("//",$line);
			$line = $cmm[0];
			
			if (trim($line)!="")
			{
				$trimline = strtolower(trim($line));
				$items = explode(":",$trimline,2);
				if (substr($line,0,1)=="\t")
				{
					$key = trim($items[0]);
					$this->scheme[$aroot][$key] = trim($items[1]??"");
				}
				else
				{
					$aroot=trim($items[0]);
					if (isset($items[1]))
					{
						$this->scheme[$aroot] = trim($items[1]);
					}
					else
					{
						$this->scheme[$aroot] = [];
					}
					
				}
			}
		}
		
		$this->title = $this->scheme['title']??$this->scheme['dbtable'];
		
		$this->scheme->fields = $this->scheme['tablefields'];
		
	
	} */
	
	
	/* function isMaria()
	{
		$sql = "SELECT COUNT(*) as `cnt` FROM information_schema.ALL_PLUGINS WHERE PLUGIN_NAME='Aria'";
		$res = $this->db->sql($sql);
		$row = $res->fetch();
		$cnt = $row['cnt']??0;
		return (bool)$cnt;
	} */
	
	
	
	

	function childs($idx,$reverse=false,$limit="1000")
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		if ($reverse==true)
		{
			$filter = "pid={$idx} ORDER by `id` DESC";
		}
		else
		{
			$filter = "pid={$idx} ORDER by `id` ASC";
		}
		
		if ($limit!="")
		{
			$filter.= " LIMIT ".$limit;
		}
		
		return $this->db->find($filter);
	}
	
	
	function childsOf($idx=0,$opts=[])
	{
		
		
		//$this->data = $this->db->with($this->scheme['dbtable']);
		
		if (isset($this->scheme->fields['order']))
		{
			$filter = "pid={$idx} ORDER by `order` ASC";
		}
		else
		{
			
			if (isset($opts['filesort']))
			{
				if ($idx==0)
				{
					$filter = "pid={$idx} ORDER by type,id ASC";
				}
				else
				{
					$filter = "pid={$idx} ORDER by id ".$opts['filesort'];
				}
			}
			else
			{
				$filter = "pid={$idx} ORDER by type,id ASC";
			}
		}
		
		return $this->provider->find($filter);
	}
	
	function paginatedList($pageNum,$pageSize=50,$where="")
	{
		/* $this->data = $this->db->with($this->scheme['dbtable']);
		
		$filter="";
		
		if ($where!="")
		{
			$filter.=" WHERE ".$where." ";
		}
		
		
		if (isset($this->scheme->fields['order']))
		{
			$filter.= " ORDER BY `order` ASC";
		}
		else
		{
			$filter.= " ORDER BY id ASC";
		} */
		
		//return $this->db->find($filter);
		
		return $this->provider->find($this->scheme->dbtable,[]); //!!!!! TODO
	}
	

	
	function all($sortDesc=false,$limit=1000)
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		if ($sortDesc)
		{
			$filter = " ORDER BY id DESC";
		}
		else
		{
			$filter = " ORDER BY id ASC";
		}
		
		$limit = (int)$limit;
		if ($limit>0)
		{
			$filter.= " LIMIT ".$limit;
		}
		
		
		return $this->db->find($filter);
	}
	
	/* function sql($sql)
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		return $this->db->sql($sql);
	} */
	
	/* function meta()
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		 $meta = $this->db->meta();
		if ($meta==false)
		{
			return false;
		}
		else
		{
			$data = [];
			foreach($this->db->meta() as $col=>$type){
				$data[$col] = $this->scheme->fields[$col]??"";
			}
			return $data;
		} 
		
		
	} */
	
	
	function get($idx)
	{
		return $this->provider->get($this->scheme->dbtable,$idx);
	}
	
	function getParents($idx,$reverseOrder=false)
	{
		print "*****";
		
		
		/* $this->data = $this->db->with($this->scheme['dbtable']);
		
		$data = [];
		
		$obj = $this->get($idx);
		if ($obj['id']>0)
		{
			$data[] = $obj;
		}
		while($obj['id']!=0)
		{
			$pid = $obj['pid']??0;
			$obj = $this->get($pid);
			
			if ($obj['id']>0)
			{
				$data[] = $obj;
			}
		}
		
		if ($reverseOrder)
		{
			return $data;
		}
		else
		{
			return array_reverse($data);
		} */
		
		
		
		/* $data = [];
		print "(".$idx.")";
		
		$obj = $this->get($idx);
		if ($obj['id']>0)
		{
			$data[] = $obj;
		}
		
		while($obj['id']!=0)
		{
			$pid = $obj['pid']??0;
			$obj = $this->get($pid);
			
			if ($obj['id']>0)
			{
				$data[] = $obj;
			}
		}
		
		if ($reverseOrder)
		{
			return $data;
		}
		else
		{
			return array_reverse($data);
		} */
		
		
	}
	
	
	function add($data) 
	{
		unset($data['created']);
		unset($data['updated']);
		
		if ($this->scheme->dbtype=="mysql")
		{
			if (isset($this->scheme->fields['created'])) 	{	$data['created'] = "~NOW()";	}
			if (isset($this->scheme->fields['updated']))	{	$data['updated'] = "~NOW()";	}
			if (isset($this->scheme->fields['uid']))		{	$data['uid'] = "~UUID()";		}
		}
		if ($this->scheme->dbtype=="mongodb")
		{
			$datetime = date("Y-m-d H:i:s");
			if (isset($this->scheme->fields['created'])) 	{	$data['created'] = $datetime;	}
			if (isset($this->scheme->fields['updated']))	{	$data['updated'] = $datetime;	}
			//if (isset($this->scheme->fields['uid']))		{	$data['uid'] = "~UUID()";		}
		}
		
		
		unset($data['order']);
		unset($data['files']); //summernote problem
		
		
		//print_r($data);
		
		foreach($data as $key=>$val)
		{
			if ($val==="")
			{
				unset($data[$key]);
			}
		}
		
		
		if (isset($data['coordinates']))
		{
			$coord = trim($data['coordinates']);
			$coord = str_replace(";",",",$coord);
			$coord = str_replace("  "," ",$coord);
			$coord = str_replace("  "," ",$coord);
			
			if ($this->provider->relative===true)
			{
				$data['coordinates'] = "^POINT({$coord})";
			}
			else
			{
				$co = explode(",",$coord);
				
				$x = trim($co[0]);
				$y = trim($co[1]);
								
				/* $data['coordinates'] = [
					'x'=>(double)$x,
					'y'=>(double)$y,
				]; */
				
				$data['coordinates'] = [
					'type'=>'Point',
					'coordinates'=>[
						'x'=>(double)$x,
						'y'=>(double)$y,
					]
				];
				
				
			}
		}
		
		
		if (isset($data['pid']) || isset($data['puid']))
		{
			$parId = $data['pid']??$data['puid'];
			print "<HR>{ $parId }";
			$parentObj = $this->provider->get($this->scheme->dbtable ,$parId );
			print_r($parentObj);
			print "<HR>";
			
			if (!isset($data['puid']))
			{
				$data['puid'] = $parentObj['uid']??'0';
			}
			elseif (!isset($data['pid']))
			{
				$data['pid'] = $parentObj['id']??'0';
			}
		}
		
		
		if ($this->provider->relative===false)
		{
			unset($data['pid']);
		}
		
		
		if (isset($data['password'])){
			$data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
		}
		
		//print "2";
		//print_r($data);
		//print "<HR>";
		$id = $this->provider->add($this->scheme->dbtable,$data);
		//\Snowflake\Net::sendto("127.0.0.1:7777",$this->scheme->dbtable."~".json_encode($data));
		
		if (isset($this->scheme->fields['order']))
		{
			if (isset($this->scheme->fields['order']))
			{
				$this->provider->edit($this->scheme->dbtable,$id,[
					'order'=>$id
				]);
			}
			else
			{
				
			}
			
		}
		
		return $id;
	}
	
	function edit($id,$data)
	{
		if (isset($data['files']))
		{
			$t = $this->provider->get($id);
			$files = trim($t['files']);
			if ($files!="")
			{
				$files.=";";
			}
			
			$files.=$data['files'];
			
			$data['files'] = $files;
		}
		
		
		if (isset($data['coordinates']))
		{
			$coord = trim($data['coordinates']);
			$coord = str_replace(";",",",$coord);
			$coord = str_replace("  "," ",$coord);
			$coord = str_replace("  "," ",$coord);
			
			if ($this->provider->relative===true)
			{
				$data['coordinates'] = "^POINT({$coord})";
			}
			else
			{
				$co = explode(",",$coord);
				
				$x = trim($co[0]);
				$y = trim($co[1]);
								
				$data['coordinates'] = [
					'x'=>(double)$x,
					'y'=>(double)$y,
				];
			}
		}
		
		
		if (isset($this->scheme->fields['updated']))
		{
			$data['updated'] = "~NOW()";
		}
		
		if (isset($data['password'])){
			$data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
		}
		
		unset($data['addfiles']);
		
		//print "<pre style='border:1px solid green;'>";
		//print_r($data);
		//print "</pre>";
		print $id;
		
		$this->provider->edit($this->scheme->dbtable,$id,$data);
	}
	
	function kill($id)
	{
		$this->provider->kill($this->scheme->dbtable,$id);
	}
	
	
	function orderMove($id,$method)
	{
		$this->data = $this->db->with($this->scheme['dbtable']);
		
		$a = $this->db->get($id);
		
		$a_id  = $a['id'];
		$a_pos = $a['order'];
		
		if ($method=='up')
		{
			$filter = "WHERE `order`<{$a_pos} ORDER BY `order` DESC LIMIT 1";
		}
		else
		{
			$filter = "WHERE `order`>{$a_pos} ORDER BY `order` ASC LIMIT 1";
		}
		
		$b = $this->db->findOne($filter);
		if ($b!=null)
		{
			$b_id  = $b['id'];
			$b_pos = $b['order'];
			
			$this->db->edit($a_id,[
				'order'=>$b_pos,
			]);
			
			$this->db->edit($b_id,[
				'order'=>$a_pos,
			]);
			
			print $a_id.";".$b_id;
		}
		else
		{
			print "NO";
		}
		
	}
	
	
	/* function search($txt)
	{
		//$this->data = $this->db->with($this->scheme['dbtable']);
		
		if (is_numeric($txt))
		{
			$filter = "WHERE id={$txt}";
		}
		else
		{
			$filter = "WHERE name LIKE '%{$txt}%' ORDER BY id DESC LIMIT 500";
		}
		
		return $this->provider->find($this->scheme->dbtable,$filter);
	} */
	
	function find($filter)
	{
		return $this->provider->find($this->scheme->dbtable,$filter);
	}
	
	function rawQuery($query)
	{
		return $this->provider->rawQuery($this->scheme->dbtable,$query);
	}
	
	/* function findOne($filter)
	{
		//$this->data = $this->db->with($this->scheme['dbtable']);
		
		return $this->db->findOne($filter);
	} */
	
	
	function where($where)
	{
		return $this->db->where($this->scheme['dbtable'],$where);
	}
	
	function countWhere($where="")
	{
		return $this->db->countWhere($this->scheme['dbtable'],$where);
	}
	
}


?>