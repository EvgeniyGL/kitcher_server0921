<?php
namespace Snowflake;

class Qwidget
{
	public static function __callStatic($widget, $args) 
	{
		$widget = strtolower($widget);
		$params = $args[0]??[];
		return \Snowflake\Qwidgets\{$widget}::build($params);
    }
}

?>