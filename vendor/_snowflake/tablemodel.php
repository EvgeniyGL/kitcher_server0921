<?php
namespace Snowflake;

abstract class TableModel
{
	private $app;
	private $view;
	private $data;
	
	function __construct($app,$view)
	{
		$this->app = $app;
		$this->view = $view;
		
		$this->data = new \Snowflake\Data\MysqlProvider($app->pdo);
	}
	
	function index()
	{
		
	}
	
	function add()
	{
		
	}
	
	function remove()
	{
		
	}
	
	function find()
	{
		
	}
	
	
}


?>