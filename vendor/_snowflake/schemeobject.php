<?php
namespace Snowflake;

class SchemeObject
{
	private $name;
	private $path;
	private $vals = [];
	private $location = "models/";
	

	function __construct($schemeName,$location="")
	{	
		$this->name = $schemeName;

		if ($location!="")
		{
			$this->location  = $location;
		}
		
		if (substr($this->location,-1,1)!="/")
		{
			$this->location.="/";
		}
		
	
		$file = strtolower($schemeName).".model.scheme";
		$path_a = $this->location.strtolower($schemeName)."/";
		$path_b = $this->location;
		
		$schemeFile="";
		if (file_exists($path_a.$file))
		{
			$this->path = $path_a;
			$schemeFile = $path_a.$file;
		}
		elseif (file_exists($path_b.$file))
		{
			$this->path = $path_b;
			$schemeFile = $path_b.$file;
		}
		else
		{
			die("SchemeModel({$schemeName}) can't find model scheme:{$schemeFile} in:{$this->path}");
		}
		
	
		$lines = explode("\n",file_get_contents($schemeFile));
		$rootKey="";
		foreach($lines as $line)
		{
			$cmm = explode("//",$line);
			$line = $cmm[0];
			
			if (trim($line)!="")
			{
				$trimline = strtolower(trim($line));
				$items = explode(":",$trimline,2);
				if (substr($line,0,1)=="\t")
				{
					$key = trim($items[0]);
					$this->vals[$rootKey][$key] = trim($items[1]??"");
				}
				else
				{
					$rootKey=trim($items[0]);
					if (isset($items[1]))
					{
						$this->vals[$rootKey] = trim($items[1]??"");
					}
					else
					{
						$this->vals[$rootKey] = [];
					}
					
				}
			}
		}
	}
	
	public function __get($name)
	{
		$name = strtolower(trim($name));
		
		if (isset($this->vals[$name]))
		{
			return $this->vals[$name];
		}
		elseif (isset($this->{$name}))
		{
			return $this->{$name};
		}
		else
		{
			return false;
		}
	}

}

?>