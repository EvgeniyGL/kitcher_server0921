<?php
namespace Snowflake\Controller;

class SchemeApi
{
	private $scheme;
	private $provider;
	private $model;
	private $baseUrlIndex;
	
	
	function __construct($baseUrlIndex=0)
	{
		$this->baseUrlIndex = $baseUrlIndex;
		$this->scheme 	= \Snowflake\SchemeManager::get( \Snowflake\Request::url($this->baseUrlIndex) );
		$this->provider = \Snowflake\Data\Manager::get($this->scheme->dbtype);
		$this->model = new \Snowflake\SchemeModel2($this->scheme,$this->provider);
	}
	
	function run()
	{
		$proc = strtolower(trim(\Snowflake\Request::url($this->baseUrlIndex+1)));
		if ($proc==""){$proc="default";}
		
		$method = "action".ucfirst($proc);
		
		if (method_exists($this,$method))
		{
			if (isset($_GET['xml']))
			{
				header ("Content-Type:text/xml");
				print \Snowflake\Xml::fromArray($this->$method(),\Snowflake\Request::url($this->baseUrlIndex));
			}
			elseif (isset($_GET['html']))
			{
				print "<pre>";
					print_r($this->$method());
				print "</pre>";
			}
			else
			{
				header("Content-type: application/json; charset=utf-8");
				print json_encode($this->$method());
			}
		}
		else
		{
			die("ERROR!!!");
		}
	}
	
	
	
	function actionAdd()
	{
		$ret=[];
		$data = $_POST;
		if (count($data)>0)
		{
			$id = $this->model->add($data);
			$ret['id'] = $id;
		}
		
		print_r($ret);
	}
	
	function actionGet()
	{
		$id = strtolower(trim(\Snowflake\Request::url($this->baseUrlIndex+2)));
		$obj = $this->model->get($id);
		return $obj;
	}
	
	function actionEdit()
	{
		$id = strtolower(trim(\Snowflake\Request::url($this->baseUrlIndex+2)));
		$data = $_POST;
		$this->model->edit($id,$data);
		return true;
	}
	
	function actionKill()
	{
		$id = strtolower(trim(\Snowflake\Request::url($this->baseUrlIndex+2)));
		$this->model->kill($id);
		return true;
	}
	
	function actionIndex()
	{
		$ret=[];
		$id = strtolower(trim(\Snowflake\Request::url($this->baseUrlIndex+2)));
		$gen = $this->model->find(['pid'=>$id]);
		foreach($gen as $obj)
		{
			$ret[] = $obj;
		}
		
		return $ret;
	}
	
	
	
	

	
	
	
	
	
}
?>