<?php
namespace Snowflake\Widget;

class Paginator
{
	static function pagination($rowsCnt,$currentPage=1,$itemPerPage=10) //page numeration begins from 1 (not from 0)
	{
		$pagesCnt = (int)ceil($rowsCnt/$itemPerPage);
		
		if ($currentPage<1) {$currentPage=1;}
		
		$prevPage=$currentPage-1;
		if ($prevPage>1){$prevHref="href='?page={$prevPage}'";} else {$prevHref="";}
		
		$nextPage=$currentPage+1;
		if ($nextPage>$pagesCnt) {$nextPage=$pagesCnt;}
		
		
		$prevDisabled = "";
		$nextDisabled = "";
		
		
		print "<div class='sfwidget_pagination'>";
			if ($prevPage==$currentPage){$prevDisabled = "disabled";}
			print "<a {$prevHref} {$prevDisabled}>❮</a>";
			
			$fromA = 1;
			$toB   = $pagesCnt;
			
			//if ($fromA<$currentPage-1)
				
			if ($fromA<($currentPage-4)){$fromA=$currentPage-4;}
			if ($toB>($currentPage+4)){$toB=$currentPage+4;}
			
			if ($fromA<5){$toB+=5-$fromA;}
			//if ($toB>($pagesCnt-4)){$fromA+=($pagesCnt-4);}
			
			//if ($fromA<1){$fromA=1;}
			//if ($toB>$pagesCnt){$toB=$pagesCnt;$fromA=$pagesCnt-9;}
			
			for($i=$fromA; $i<=$toB; $i++)
			{
				$num=$i;
				//if ($num<=1){$num="";}
				
				if ($currentPage==$i)
				{
					$current="current";
					$href= "";
				} 
				else
				{
					$current = "";
					$href= "href='?page={$num}'";
				}			
				
				print "<a {$href} {$current}>{$i}</a>";
			}
			if ($nextPage==$currentPage)
			{
				$prevDisabled = "disabled";
				$nextHref="";
			}
			else
			{
				$nextHref="href='?page={$nextPage}'";
			}
			print "<a {$nextHref} {$nextDisabled}>❯</a>";
		print "</div>";
	}
}

?>