<?php
namespace Snowflake;

class SfLib
{

	static function readConfig($path)
	{
		if (file_exists($path))
		{
			$data = [];
			$file = file_get_contents($path);
			$lines = explode("\n",$file);
			$root="";
			foreach($lines as $line)
			{
				$cmm = explode("//",$line);
				$line = $cmm[0];		
				if (trim($line)!="")
				{
					$trimline = strtolower(trim($line));
					$items = explode(":",$trimline);
					if (substr($line,0,1)=="\t")
					{
						$key = trim($items[0]);
						$data[$root][$key] = trim($items[1]??"");
					}
					else
					{
						$root=trim($items[0]);
						if (isset($items[1]))
						{
							$data[$root] = trim($items[1]);
						}
						else
						{
							$data[$root] = [];
						}
						
					}
				}
			}
					
			return $data;
		}
		else
		{
			return false;
		}
	}
	
	
	
	static function readFormView($path)
	{
		if (file_exists($path))
		{
			$data = [];
			$file = file_get_contents($path);
			$lines = explode("\n",$file);
			$root="DEFAULT";
			$colID=0;
			$i=0;
			$colWidth="";
			foreach($lines as $line)
			{
				$line = trim($line);
				if ($line!="")
				{
					if (substr($line,0,2)=="::")
					{
						$root=trim(substr($line,2));
						$colID=0;
					}
					elseif (substr(trim($line),0,3)=="===")
					{
						$colWidth = trim(str_replace("=","",$line));			
						$colID++;
						$i=0;
					}
					else
					{
						$items = explode("|",$line);
						
						foreach($items as $item)
						{
							$kv = explode(":",trim($item));
							if (count($kv)==1)
							{
								$key = trim($kv[0]);
								$val = trim($kv[0]);
							}
							else
							{
								$key = trim($kv[0]);
								$val = trim($kv[1]);
							}
							
							
							$cw = "";
							if ($colWidth!="")
							{
								$cw = "~".$colWidth;
							}
							$data[$root]['col'.$colID.$cw]['row'.$i][$key] = $val;
							
							
						}
						$i++;
					}
					
				}
			}
					
			return $data;
		}
		else
		{
			return false;
		}
	}
	
	
	
	static function datetime2date($dt)
	{
		$dt = explode(" ",$dt);
		$d = explode("-",$dt[0]);
		return  $d[2].".".$d[1].".".$d[0];
	}
	
	
	static function datetime2human($dt,$withTime=false,$genitiveСase=false)
	{
		$ret="";
		$months = [' ','jan','feb','mar','apr','may','jun','jul','aug','sep','oct','now','dec'];
		if ($genitiveСase)
		{
			$months = [' ','january','february','march','april','may','june','july','august','september','october','november','december'];
		}
		
		$dt = explode(" ",$dt);
		$d = explode("-",$dt[0]);
		$m = (int)$d[1];
		
		$day = (int)$d[2];

		$ret = $months[$m]." ".$day.", ".$d[0]."";
		
		$time = $dt[1]??"";
		if ($withTime && $time!="")
		{
			$t = explode(":",$time);
			
			$ret.=" ".$t[0].":".$t[1];
			
		}
		
		return $ret;
	}
	
	static function html2text($html,$saveBreaks=false)
	{
		if ($saveBreaks)
		{
			$html = str_replace("<br>","\n ",$html);
			$html = str_replace("<BR>","\n ",$html);
		}
		$html = str_replace("<"," <",$html);
		$html = trim(strip_tags($html));
		$html = preg_replace('/\s+/', ' ', $html);
		return $html;
	}
	
	
	static function antiInjection($str)
	{
		$badWords = ['select','where','union','join','delete','insert','update','create','drop'];
		$badSimbs = ['--','"',"'","`","#","="];
	
		$safe=true;
		$t = trim(strtolower($str));
		foreach($badWords as $word)
		{
			if (strpos($str,$word)>-1)
			{
				$safe=false;
				//print "?";
			}
		}
		
		
		if ($safe==false)
		{
			return "";
		}
		else
		{
			foreach($badSimbs as $sym)
			{
				$str = str_replace($sym,"",$str);
			}
		
			return $str;
		}
	}
	
	
	
	
	
	
	static function sfSafeNumber($txt)
	{
		$r="";
		
		$len = strlen($txt);
		for($i=0; $i<$len; $i++)
		{
			$t = substr($txt,$i,1);
			if (in_array($t,[0,1,2,3,4,5,6,7,8,9]))
			{
				$r.=$t;
			}
		}
		
		return $r;
	}
	
	static function sfSafeString($txt)
	{
		
		
		return $txt;
	}

	
	static function sfMap2Str($map,$keyValueDelimiter=":",$itemDelimiter=",")
	{
		$r=[];
		foreach($map as $key=>$val)
		{
			$r[] = $key.$keyValueDelimiter.$val;
		}
		
		return implode($itemDelimiter,$r);
	}
	
	
	
}

?>