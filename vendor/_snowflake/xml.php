<?php
namespace Snowflake;

class Xml
{
	static function fromArray($data,$node="root")
	{
		$xml = new \SimpleXMLElement("<?xml version=\"1.0\"?><{$node}></{$node}>");
		self::array_to_xml($data,$xml);
		return $xml->asXml();
	}
	
	private static function array_to_xml($array, &$xml_user_info) {
		foreach($array as $key => $value) {
			if(is_array($value)) {
				if(!is_numeric($key)){
					$subnode = $xml_user_info->addChild("$key");
					self::array_to_xml($value, $subnode);
				}else{
					$subnode = $xml_user_info->addChild("item");
					self::array_to_xml($value, $subnode);
				}
			}else {
				$xml_user_info->addChild("$key",htmlspecialchars("$value"));
			}
		}
	}
}


?>