<?php
	error_reporting(0);

	set_error_handler("onError");	
	register_shutdown_function("onFatalError");
	
	function onFatalError(){
		$err = error_get_last();
		$msg = $err['message']??"";
		if ($msg!=""){
			$stack="==";
			if (strpos($msg,"Stack trace:")>0){
				$parts = explode("Stack trace:",$msg);
				$msg = $parts[0];
				$stack = "Stack trace:".$parts[1];
			}
			
			debugScreen(0,$msg,$err['file']??"",$err['line']??"",null,$stack);
		}
	}
	
	function onError($level,$message,$file,$line) 
	{
		debugScreen($level,$message,$file,$line,[]);
	}
	
	function debugScreen($level,$message,$file,$line,$context,$stackTrace=""){
		
		
		$rootpath = $_SERVER['DOCUMENT_ROOT'];
		$rootpathWin = str_replace("/","\\",$_SERVER['DOCUMENT_ROOT']);
		
		$message = str_replace($rootpath,"",$message);
		$message = str_replace($rootpathWin,"",$message);
		
		$stackTrace = str_replace($rootpath,"",$stackTrace);
		$stackTrace = str_replace($rootpathWin,"",$stackTrace);
		
		
		style();
		print "<pre style='background:#333;color:#eee;padding:10px;'>";
			print "<B>ERROR in [<span style='color:red'>".$file."</span> @ <span style='color:red'>".$line."</span>] </B>";
			print "<BR><BR><div style='font-size:14px;'>".$message."</div><BR><BR>";

			print "<div style='background:#555;padding:20px;line-height:30px;'>";
			if ($stackTrace==""){
				debug_print_backtrace();
			}else{
				print $stackTrace;
			}
			print "</div>";
		die();
	}
	
	function style(){
		print "<style>
			body{background:#aaa;padding:10%;}
			* {font-size:16px;font-family:arial;}
		</style>";
	}


?>