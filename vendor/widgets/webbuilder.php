<?php
	namespace widgets;
	
	use Path;

	class Webbuilder{
		
		static function build($widgetName,$data=[]){
			if (method_exists("Datawidget",)){
				self::$widgetName($data);
			}else{
				return "Widget not found: ".$widgetName;
			}
		}
		
		static function blockeditor($data=[]){
			$out="";
			$out.="<div class='blockeditor'>";
				$out.="<div id='lbe_document_root'>";
					//$out.=print_r($data,true);
					$srlData = serialize($data);
					$out.="<iframe id='lbe_document_frame' src='/rpc/blocks_host?plain=1&srldata={$srlData}'></iframe>";
					$out.="<div class='lbe_block_devider' onclick='lbe_add(this)'>+</div>";
				$out.="</div>";
			$out.="</div>";
			return $out;
		}
		
		static function blocks_menu($blockid){
			print "<img src='/assets/icons/form_edit32.png' style='cursor:pointer' onclick=''>";
		}
		
		static function blocks($data=[]){
			print "<div id='webbuilder_blocks' class='webbuilder_blocks sortable_table'>";
			if (is_array($data)){
				foreach($data as $block){
					//$blockName = $block['block'];
					$srlData = serialize($block);
					print "<table class='webbuilder_block_container'><tr>";
					print "<td><iframe class='autosizedframe' src='/rpc/blocks_host?plain=1&srldata={$srlData}'></iframe></td>";
					print "<td class='webbuilder_block_menu'>";
					self::blocks_menu(1);
					print "</td>";
					print "</tr></table>";
				}
			}else{
				print "!!";
			}
			print "<div id='webbuilder_block_add' class='lbe_block_devider' onclick='lbe_add(this)'>+</div>";
			print "</div>";
		}
		
		
		static function blocks_test($data=[]){
			print "test:".print_r($data);
		}
		
		
	}
?>