<?php
	namespace Widgets;
	
	class Widget{
		
		public static function __callStatic($name, $args)
		{
			print $name;
			print $args;
			
			return "!!!!!!!!";
		}
		
		
		static function byType(string $type,array $meta, array $data,array $options){
			$method = strtolower($type);
			if (method_exists(get_called_class(),$method)){
				return self::$method($meta,$data,$options);
			}else{
				return self::unknown($meta,['value'=>print_r($meta,true)],$options);
			}
		}
		
		
		static function string(array $meta, array $data,array $options){
			$field = $meta['field']??"";
			$val = $data['value']??"";
			
			$class = $options['class']??"";
			
			return "<input class='control formControl {$class}' name='{$field}' value='{$val}'>";
		}
		
		
		static function password(array $meta, array $data,array $options){
			$field = $meta['field']??"";
			$val = $data['value']??"";
			
			$class = $options['class']??"";
			
			return "<input class='control formControl {$class}' name='{$field}' type='password' value='' placeholder='********'>";
		}
		
		
		
		static function html(array $meta, array $data,array $options){
			$field = $meta['field']??"";
			$val = $data['value']??"";
			
			$class = $options['class']??"";
			
			return "<textarea class='control formControl htmleditor {$class}' name='{$field}'>{$val}</textarea>";
			
		}
		
		
		static function text(array $meta, array $data,array $options){
			$field = $meta['field']??"";
			$val = $data['value']??"";
			
			$class = $options['class']??"";
			
			return "<textarea class='control formControl {$class}' name='{$field}'>{$val}</textarea>";
		}
		
		
		static function int(array $meta, array $data,array $options){
			$field = $meta['field']??"";
			$val = $data['value']??"...";
			
			$class = $options['class']??"";
			
			return "<input class='control formControl {$class}' name='{$field}' type='number' step=1 min=0 value='{$val}'>";
		}
		
		static function float(array $meta, array $data,array $options){
			$field = $meta['field']??"";
			$val = $data['value']??"...";
			
			$class = $options['class']??"";
			
			return "<input class='control formControl {$class}' name='{$field}' type='number' step='0.01' min=0 value='{$val}'>";
		}
		
		
		
		
		static function file(array $meta, array $data,array $options){
			$field = $meta['field']??"";
			$val = $data['value']??"...";
					
			$randindex=rand(111111,999999);
			$html = "<div class='input_file_wrapper' style='width:100%;'>";
				$ext = strtolower(pathinfo($val, PATHINFO_EXTENSION));
				if (in_array($ext,[ 'jpg','jpeg','gif','png','webp', ])){
					$html.= "<img src='/media/{$val}' style='max-width:100%; max-height:400px; cursor:pointer; border:1px solid #eeeeee;' toggle={$randindex} title='{$val}' onclick='toggle({$randindex});'>";
				}elseif (in_array($ext,[ 'mp4' ])){
					$html.= "<video controls='controls' src='/media/{$val}' style='max-width:100%; max-height:400px; cursor:pointer; border:1px solid #eeeeee;' toggle={$randindex} title='{$val}' contextmenu='edit' onclick='toggle({$randindex});'></video>";
					$html.= "<menu type='context' id='edit'><menuitem onclick='toggle({$randindex});'>Заменить файл...</menuitem></menu>";
				}else{
					$html.= "<div class='control formControl' style='background:white;' toggle={$randindex} onclick='toggle({$randindex});'>{$val}</div>";
				}
			
				$html.= "<input id='{$field}' name='{$field}' class='control formControl' type='file' value='{$val}' toggle={$randindex} style='display:none;' oncontextmenu=\"javascript:toggle({$randindex});return false;\">";
			$html.= "</div>";
			return $html;
		}
		
		
		static function bool(array $meta, array $data,array $options){
			$field = $meta['field']??"";
			$val = $data['value']??"...";
			
			$default = $meta['default']??"";
			
			//$class = $options['class']??"";
			
			$checked="";
			if ($val==1 || ($val=="" && $default==1)){
				$checked="checked='checked'";
			}
			
			$title = $meta['title']??$field;

			return "<div class='checkbox'><input id='{$field}' name='{$field}' class='control' type='checkbox' {$checked}/><label class='formFieldLabel' for='{$field}'>{$title}</label></div>";
		}
		
		
		static function reference(array $meta, array $data,array $options){
			$meta['multiple'] = false;
			return self::references($meta, $data, $options);
		}
		
		static function references(array $meta, array $data,array $options){
			$field = $meta['field']??"";
			$val = $data['value']??"...";
			$db =  $options['db']??false;
			
			$class = $options['class']??"";
			$multiple = $meta['multiple']??false;
			$refTable = $meta['ref'];
			if (substr($refTable,0,1)=="$"){
				$refTable = substr($refTable,1);
			}
			
			
			if ($multiple){
				if (substr($val,0,1)=="["){
					$val = json_decode($val);
				}
				$html = "<select name='{$field}[]' class='control formControl select2' multiple='multiple'>";
			}else{
				$html = "<select name='{$field}' class='control formControl select2'>";	
			}
			
			
			$type = $meta['widget']??$meta['type']??"";
			if ( substr($type,0,2)=="$[" || substr($type,0,3)=="$$["){
				////$html.= self::getOptionsForList($type,$val,$multiple);
			}else{
				$html.= self::getOptionsForDictionary($refTable,$val,$field,$db,$multiple);
			}			
			$html.= "</select>";

			return $html;
		}
		
		
		static function set(array $meta, array $data,array $options){
			$field = $meta['field']??"";
			$val = $data['value']??"...";
			
			$default = $meta['set'][array_key_first($meta['set'])];
			
			$html = "<select name='{$field}' class='formControl'>";
			$i=0;
			foreach($meta['set'] as $key=>$item)
			{
				$sel = "";
				if ($val==""){
					if ($default==$key) {$sel = "selected='selected'";}
				}else{
					if ($val==$key) {$sel = "selected='selected'";}
				}
				$html.= "<option value='{$key}' {$sel}>{$item}</option>";
			}
			$html.= "</select>";			
			return $html;
		}
		
		
		static function unknown(array $meta, array $data,array $options){
			$field = $meta['field']??"";
			$val = $data['value']??"";
			
			$class = $options['class']??"";
			
			return "<textarea class='control formControl {$class}' name='{$field}' style='width:100%;font-size:11px;'>{$val}</textarea>";
		}
		
		
		
		
		static function refobj2plain($obj){
			$arr = [];
			foreach($obj as $k=>$v){
				if (strpos($k,".")===false){
					$pair = explode(".",$k,2);
					$arr[$pair[1]] = $v;
				}else{
					$arr[$k] = $v;
				}
			}
			
			return $arr;
		}
		
		
		
		
		static function getOptionsForDictionary($modelName,$id,$key,$db,$multiple=false)
		{
			$html = "";
			$selected="";
			
			//$html.="<option>".print_r($id,true)."</option>";
			
			$ids=[];
			if (is_array($id)){
				$ids = $id;
			}else{
				if ($id==0){
					$selected = "selected='selected'";
				}else{
					$ids = [$id];	
				}
			}
			
			//$html.="<option>".implode("+",$ids)."</option>";
			
			if ($multiple===false){
				$html.="<option value='0' {$selected}>Не выбрано...</option>";
			}
			

			$res = $db->$modelName->find([]);

			
			foreach($res as $row){
				$id = $row['id'];
				$title = $row['name']??$id;
				
				$selected="";
				if (in_array($id,$ids)){
					$selected = "selected='selected'";
				}
					
				$html.="<option value='{$id}' {$selected} title='#{$id}'>".$title."</option>";
			}
			
			return $html;
		}
		
		
		
		//static function getOptionsForList($type,$id=0,$multiple)
		static function getOptionsForList($type,$id,$multiple)
		{
			$html="";
			
			$listStr = trim(substr($type,2));
			if (substr($listStr,-1,1)=="]")
			{
				$listStr = substr($listStr,0,-1);
			}
			
			$arr=[];
			$list = explode(",",$listStr);
			$i=0;
			foreach($list as $item)
			{
				$kv = explode(":",$item);
				if (count($kv)==1)
				{
					$arr[$i] = $kv[0];
				}
				else
				{
					$arr[$kv[0]] = $kv[1];
				}
				
				$i++;
			}
			
			
			foreach($arr as $key=>$val)
			{
				if ($key==$id)
				{
					$html.= "<option value='{$key}' selected='selected'>{$val}</option>";
				}
				else
				{
					$html.= "<option value='{$key}'>{$val}</option>";
				}
			}
			
			
			return $html;
		}
	
		
		
	}

?>