<?php
namespace widgets;

class Subtable{
	
	static function decodeReference(array $obj){
		$arr = [];
		$ref = "";
		
		foreach($obj as $k=>$v){
			$pair = explode(".",$k,2);
			if ($ref==""){
				$ref = $pair[0];
			}
			
			$arr[$pair[1]] = $v;
		}
		
		return (object)[
			'ref'=>$ref,
			'data'=>$arr
		];
	}
	
	static function table($context){
		$db = $context['db']??null;
		$key = $context['key']??"";
		$scheme = $context['scheme']??[];
		$object = $context['object']??[];
		
		//print_r($key);
		
		if ( empty($object) ){
			print "Недоступно до сохранения документа!";
		}else{
			if (strpos($key,"_")!==false){
				$subtableName = $key;
				$pair = explode("_",$key);
				$key = $pair[1];
				
				$subtable = $scheme;
				
				$fullview = false;
				//print_r($subtable);
				
			}else{
				$subtable = $scheme['subtables'][$key]??false;
				
				$subtableName = $scheme['scheme']."_".$key;	
				$fullview = true;
				
			}
			
			
			
			if ($subtable!==false){		
				$haveOrder = isset($subtable['fields']['_order']);		
				$parent_object_id = $object['id'];
				$opt = [];
				
				if ($haveOrder){ $opt['orderby']='_order';	}
				
				$gen = $db->$subtableName->find([
					'_id'=>$parent_object_id
				],$opt);
				
			
				if ($fullview){print "<div style='border:1px solid #ddd;background:white;margin-top:15px;'>";}
					print "<table class='widgetTable subtable' table='{$subtableName}'>";
					
						print "<thead><tr>";
							print "<th width=30><div class='material-icons' style='color:green;cursor:pointer;' onclick='subtableForm(`{$subtableName}`,`{$parent_object_id}`)'>add</div></th>";
							
							$colCnt=0;
							foreach($subtable['fields'] as $k=>$v){
								if (!in_array($k,["_id","_order","id"])){
									$title = $subtable['fields'][$k]['title']??$k;
									print "<th>".print_r($title,true)."</th>";
									$colCnt++;
								}
							}
							
							if ($haveOrder){
								print "<th width=30></th>";
								$colCnt++;
							}
							
							
							print "<th width=30></th>";
							$colCnt++;
							
							
							
						print "</tr></thead>";
						print "<tbody class='sortable_table' table='{$subtableName}' parent_object_id='{$parent_object_id}'>";	
					
					
						
						$i=0;
						foreach($gen as $row){
							$rowId = $row['id']??"";
							print "<tr _id='{$rowId}'>";
								print "<td width=30><div class='material-icons' style='color:blue;cursor:pointer;' onclick='subtableForm(`{$subtableName}`,`{$parent_object_id}`,`{$rowId}`)'>description</div></td>";
								
								foreach($row as $k=>$v){
									if (!in_array($k,["_id","_order","id"])){
										
										if (is_array($v)){
											$refObj = self::decodeReference($v);
											//print_r($refObj);
											$name = $refObj->data['name']??$refObj->data['id'];
											$id = (string)$refObj->data['id']??"";
											if ($id==""){$id=0;}
											
											//$link = "<a href='/{$refObj->ref}/{$id}' target='_blank' style='font-style:italic;'>{$name}</a>";
											
											print "<td key='{$k}' value='{$id}'>".$name."</td>";
										}else{
											
											
											$ext = strtolower(substr($v,-4,4));
											if (in_array($ext,[ '.jpg','.png','.gif','webm','jpeg'])){
												print "<td key='{$k}' value='{$v}'>";
												print "<img src='/media/{$v}' style='width:120px;'>";
												print "</td>";
											}else{
												print "<td key='{$k}'>";
												print $v;
												print "</td>";												
											}
											
											
											
											
										}
										
										
									}
								}
								
								print "<td><div class='material-icons' style='color:red;cursor:pointer;' onclick='subtableRemoveItem(`{$subtableName}`,`{$parent_object_id}`,`{$rowId}`)'>highlight_off</div></td>";
								
								if ($haveOrder){
									print "<td><div class='material-icons' style='color:gray;cursor:move;'>open_with</div></td>";
								}
								
							print "</tr>";
							$i++;
						}
						
						if ($i==0){
							print "<tr><td colspan='{$colCnt}' style='text-align:center;'>Нет данных...</td></tr>";
						}
						
					print "</tbody></table>";
					
					print "<div class='subtableForm' table='{$subtableName}' parent_object_id='{$parent_object_id}' style='display:none;'>";
						//print "<form id='subtableForm_{$subtableName}'>";
						print "<div class='subtableForm_header'>";
							print "<div class='subtableForm_title'>{$subtableName}</div>";
							print "<div class='subtableForm_menu' style='cursor:pointer;background:green;opacity:0.5;color:white;' onclick='subtableFormSave(`{$subtableName}`,`{$parent_object_id}`)'><span class='material-icons'>done</span></div>";
							print "<div class='subtableForm_menu' style='cursor:pointer;background:red;opacity:0.5; color:white;' onclick='subtableForm(`{$subtableName}`,``,``)'><span class='material-icons'>close</span></div>";
						print "</div>";
						print "<div class='subtableForm_body'>";
							print "<input type='hidden' name='id' value=''>";
							foreach($subtable['fields'] as $field=>$meta){
								if (substr($field,0,1)!="_" && $field!="id"){
									$meta['field'] = $field;
									
									$title = $meta['title']??$field;
									
									print "<div class='formFieldLabel'>{$title}</div>";
									$widgetType = $meta['type']??$meta['widget']??"string";
									//print $widgetType;
									if (isset($meta['ref'])){
										$widgetType = "reference";
									}
									
									//print_r($widgetType);
									print "<div class='formField'>".Widget::byType($widgetType,$meta,[
										'value'=>''
									],[
										'class'=>'subtableField',
										'db'=>$db,
										
									])."</div>";
								}
							}
						print "</div>";
						//print "</form>";
					print "</div>";
					
				if ($fullview){print "</div>";}
				
				
			}
			
		}
	}
	
	
}

?>