<?php
namespace Mech;

use Mech\Widget;

class Mech
{
	var $mech=[];
	
	function __construct($file){
		
		if (substr($file,-5,5)==".mech"){
			$file = substr($file,0,-5);
		}
		
		
		//if (strpos($file,"/")===false){
		//	$file = "schemes/".$file;
		//}
		
		$path = "schemes/".$file.".mech";
		if (file_exists($path)){
			$this->mech = $this->read($path);
		}else{
			$pair = explode("_",$file);
			$path = "schemes/".$pair[0].".mech";
			if (file_exists($path)){
				$mainScheme = $this->read($path);
				if (isset($mainScheme['subtables'][$pair[1]])){
					$this->mech = $mainScheme['subtables'][$pair[1]];
				}else{
					die("File not found(or no subtable in main scheme):".$file);
				}
			}else{
				die("File not found:".$file);
			}
			
			
		}
		
	}
	
	function __get($prop){
		return $this->mech[$prop]??"";
	}
	
	function read($file){
		$json = trim(file_get_contents($file));
		$json = $this->jsonFixNames($json);
		$data = json_decode($json,true);
		
		if (is_null($data)){
			die("BAD JSON: ".$file);
		}
		
		$driver = strtolower($data['driver']??"");
		
		foreach($data['fields'] as $k=>$v){
			if (!is_array($v)){
				$data['fields'][$k] = $this->fixField($v,$driver);
			}
		}
		
		
		if ( isset($data['subtables']) && count($data['subtables'])>0){
			foreach($data['subtables'] as $stab=>$tmeta){
				foreach($tmeta['fields'] as $fld=>$fmeta){
					if (!is_array($fmeta)){
						$data['subtables'][$stab]['fields'][$fld] = $this->fixField($fmeta,$driver);
					}
				}
			}
		}
		
		//print "<pre>";
		//	print_r($data);
		//print "</pre><hr>";
		
		return $data;
	}
	
	function jsonFixNames($json){
		$json = preg_replace('/(\w+):/i', '"\1":', $json);
		$json = str_replace('$"','"$',$json);
		return $json;
	}

	function fixField($f,$driver){
		if (is_numeric($f)){
			if (strpos($f,".")!==false){
				$f=[
					"type"=>"float",
					"default"=>$f
				];
			}else{
				$f=[
					"type"=>"int",
					"default"=>$f
				];
			}
		}elseif ($f=="auto" && $driver=="mysql"){
			$f=[
				"type"=>"bigint",
				"key"=>"primary",
				"default"=>"autoincrement"
			];
		}elseif(substr($f,0,2)=="$$"){
			$ref = substr($f,2);
			
			$f=[
				"type"=>"text COMMENT '$$".$ref."'",
				"widget"=>"references",
				"ref"=>$ref,
				"multiple"=>true,
			];
		}elseif(substr($f,0,1)=="$"){
			$ref = substr($f,1);
			
			$f=[
				"type"=>"bigint COMMENT '$".$ref."'",
				"widget"=>"reference",
				"ref"=>$ref,
				"multiple"=>false,
			];
		}elseif(substr($f,0,5)=="list$"){
			$ref = substr($f,5);
			
			$f=[
				"type"=>"reflist",
				"ref"=>$ref,
			];
		}elseif($f=="true" || $f=="false"){
			$f=[
				"type"=>"bool",
				"default"=>$f
			];
		}elseif(substr($f,0,1)=="[" && substr($f,-1,1)=="]"){
			$f = explode(";",substr($f,1,-1));
			
			$set = [];
			$first="";
			foreach($f as $fv){
				$pair = explode("=",$fv,2);
				if (count($pair)==2){
					$set[$pair[0]] = $pair[1];
					if ($first==""){$first=$pair[0];}
				}else{
					$set[] = $pair[0];
					if ($first==""){$first=$pair[0];}
				}
			}
			
			$f=[
				"type"=>"int",
				"set"=>$set,
				"default"=>$first
			];
		}else{
			if ($f=="string"){
				$f=[
					"type"=>"string",
					"default"=>""
				];
			}else{
				$f=[
					"type"=>"string",
					"default"=>$f
				];
			}
			
		}
	
		return $f;
	}
	
	
	function formFields($form="form"){
		if (isset($this->mech['view'][$form])){
			$fields = [];
			foreach($this->mech['view'][$form] as $ffield=>$fdata){
				$fields[$ffield] = $this->mech['fields'][$ffield] + $fdata;
			}
		}else{
			$fields = $this->mech['fields'];
		}
		
		//print "<pre>";
		//	print_r($fields);
		//print "</pre><hr>";
		
		foreach($fields as $field=>$data){
			$title = $data['title']??$field;
			yield $title=>Widget::byType($field,$data);
		}
	}
	
	function printForm(array $opt=[]){
		$form=$opt['form']??"form";
		print "<form";
			if (isset($opt['action'])) 	{print " action='".$opt['action']."'";}
			if (isset($opt['method'])) 	{print " method=".$opt['method']."'";}
			if (isset($opt['enctype'])) {print " enctype=".$opt['enctype']."'";}
		print ">";
		
		print "<table>";
			foreach($this->formFields($form) as $title=>$html){
				print "<tr>";
					print "<td>{$title}</td>";
					print "<td>{$html}</td>";
				print "</tr>";
			}
		print "</table>";
		print "</form>";
	}

}


?>