<?php
namespace Mech;

class Widget{
	
	static function byType($name,$data,$val=""){
		$widget = $data['type']??"string";

		if (method_exists(__CLASS__,$widget)){
			return self::$widget($name,$data,$val);
		}else{
			return self::string($name,$data,$val);
		}
	}
	
	
	static function string($name,$data,$val){
		$t = $data['type']??"";
		
		return "<input class='form_control' placeholder='{$name}:{$t}'>";
	}
	
	static function text($name,$data,$val){
		return "<textarea  class='form_control' placeholder='{$name}:{$t}'></textarea>";
	}
	
	static function set($name,$data,$val){
		$html = "<select class='form_control'>";
			$html.= "<option value=''>Не выбрано</option>";
		$html.= "</select>";
		
		return $html;
	}
	
	static function ref($name,$data,$val){
		$html = "<select class='form_control'>";
			$html.= "<option value=''>Не выбрано</option>";
		$html.= "</select>";
		
		return $html;
	}
	
	
}


?>