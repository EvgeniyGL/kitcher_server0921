<?php
namespace Norm;

class Schema{
	
	static function read($path){
		$path = "../schemes/users.scheme";
		$file = file_get_contents($path);
		$lines = explode("\n",$file);
		$arr=[];
		
		$k="";

		foreach($lines as $line){
			$tl = trim($line);
			
			if ($tl==""){
				$k=[];
				$level=0;
			}elseif(substr($tl,0,1)=="#"){
				//comment
				$k=[];
				$level=0;
			}elseif(substr($line,0,1)!="\t"){
				$t = self::parseLine($line);
				$k=$t->key;
				
				if ($t->val==""){
					$arr[$k] = [];
				}else{
					$arr[$k] = $t->val;
				}				
			}else{
				$t = self::parseLine($line);
				$arr[$k][$t->key] = $t->val;
			}
		}
		
		return $arr;
	}
	
	static function parseLine($line){
		$line = trim($line);
		$kv = explode(":",$line,2);
		$key = $kv[0];
		$vals = $kv[1]??"";
		
		return (object)[
			'key'=>$key,
			'val'=>$vals,
		];
		
	}
	
}

?>