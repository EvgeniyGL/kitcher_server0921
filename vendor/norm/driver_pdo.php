<?php
namespace Norm;

use Norm\Query;

class Driver_pdo extends Driver{
	private $pdo;
	private $db;
	private $driver;
	
	private $errmsg="";
	
	private $autoDereferencing=0;
	
	function __construct($config){
		
		$this->driver = strtolower($config['driver']);
		$host = $config['host'];
		$user = $config['user'];
		$password = $config['password'];
		$this->db = $config['database'];
		
		$this->autoDereferencing = (int)($config['dereferencing']??0); 
		
		try {
			if ($this->driver=="sqlite"){
				$this->pdo = new \PDO("sqlite:".$host);
			}elseif($this->driver=="mysql" || $this->driver=="mariadb"){
				$this->pdo = new \PDO("{$this->driver}:host={$host};dbname={$this->db};charset=UTF8", $user, $password,array(\PDO::ATTR_PERSISTENT => TRUE));
				$this->pdo->exec("set names utf8");
				
				
			}
			else{
				$this->pdo = new \PDO("{$this->driver}:host={$host};dbname={$this->db};charset=UTF8", $user, $password,array(\PDO::ATTR_PERSISTENT => TRUE));
				$this->pdo->exec("set names utf8");
			}
			
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
		
	}
	
	function conditionBuilder($key,$val,$opts=[]){
		$table = $opts['_TABLE']??"";
		
		$field = trim($key);
		$operand = "=";
		$value = $val;
		$comma = "AND";
		$israw = false;
		
		if (is_null($val)){
			$operand = " IS NULL";	
			
			if (strpos($field," ")>0){
				$fld = explode(" ",$field);
				
				if (in_array(strtoupper($fld[0]),['AND','OR'])){
					$comma = strtoupper($fld[0]);
					array_shift($fld);
				}
				
				$field = $fld[0];
				
			}else{
				$operand = " IS NULL";	
				
			}
			
			
		}else{
			if (strpos($field," ")>0){
				$fld = explode(" ",$field);
				
				if (in_array(strtoupper($fld[0]),['AND','OR'])){
					$comma = strtoupper($fld[0]);
					array_shift($fld);
				}
				
				
				if (count($fld)==2){
					$field = $fld[0];
					
					if (in_array($fld[1],['<','>','<=','>=','<>','=','!='])){
						$operand=$fld[1];
					}else{
						$op = strtoupper($fld[1]);
						if($op=="IN"){
							$israw=true;
							$operand=" IN ";
							if (is_array($value)){
								$value = "('".implode("','",$value)."')";
							}else{
								$value = "(".$value.")";
							}
						}elseif($op=="NOTIN" || $op=="NOT_IN"){
							$israw=true;
							$operand=" NOT IN ";
							if (is_array($value)){
								$value = "('".implode("','",$value)."')";
							}else{
								$value = "(".$value.")";
							}
						}elseif($op=="LIKE" || $op=="%LIKE%"){
							$operand=" LIKE ";
							$value = "%".$value."%";
						}elseif($op=="%LIKE"){
							$operand=" LIKE ";
							$value = "%".$value;
						}elseif($op=="LIKE%"){
							$operand=" LIKE ";
							$value = $value."%";
						}elseif($op=="NOTLIKE" || $op=="%NOTLIKE%" || $op=="%NOT_LIKE%" ){
							$operand=" NOT LIKE ";
							$value = "%".$value."%";
						}elseif($op=="%NOTLIKE" || $op=="%NOT_LIKE"){
							$operand=" NOT LIKE ";
							$value = "%".$value;
						}elseif($op=="NOTLIKE%" || $op=="NOT_LIKE%"){
							$operand=" NOT LIKE ";
							$value = $value."%";
						}
						
					}	
				}
			}
		}
		
		
		if ($table!=""){
			$field = $table."`.`".$field;
		}
		
		return (object)[
			'field'=>$field,
			'operand'=>$operand,
			'value'=>$value,
			'comma'=>$comma,
			'israw'=>$israw,
		];
	}
	
	
	function fixPlaceholder($p){
		
		$p = str_replace(" ","_",$p);
		$p = str_replace("=","eq",$p);
		$p = str_replace("==","eq",$p);
		$p = str_replace("!=","neq",$p);
		$p = str_replace("<>","neq",$p);
		$p = str_replace("<","lt",$p);
		$p = str_replace("<=","lte",$p);
		$p = str_replace(">=","qte",$p);
		
		return $p;
	}
	
	
	
	function filterBuilder($query, array $opts=[]){
		
		$leftjoins = $opts['_LEFTJOIN']??[];
		$table = $opts['_TABLE'];
		
		$useJoins=false;
		if (count($leftjoins)>0){
			$useJoins=true;
		}
		
		$where="";
		$vals=[];
		
		if(is_numeric($query)){
			$query = [ 'id'=>$query	];
		}elseif (!is_array($query) && Utils::isUUID($query)){
			$query = [ 'uid'=>$query ];
		}	
			
		if (is_array($query) && count($query)>0){
			$where.= "WHERE ";
			$cm="";
			$idx=0;
			$isfirst=true;
			foreach($query as $key=>$val){
				$condition = $this->conditionBuilder($key,$val,$opts);
				$operand 	= $condition->operand;
				$value  = $condition->value;
				$field  = $condition->field;
				$israw  = $condition->israw;
				//$comma 	= $condition->comma;

				
				
				if (is_array($value)){
					if ($operand=="="){
						$operand = "IN";
						$value = "(".implode(",",$value).")";
					}elseif ($operand=="<>" || $operand=="!="){
						$operand = "NOT IN";
						$value = "(".implode(",",$value).")";
					}
				}
				//print_r($operand);
				//print_r($value);
				
				if ($isfirst){
					$cm="";
				}else{
					$cm=" ".$condition->comma." ";
				}
				
				if ($useJoins){
					//print "!";
					if (strpos($field,".")>0){
						list($tab,$field) = explode(".",$field);
						
						if (strpos($tab,".")){
							$tab = explode(".",$tab)[0];
						}
						
						$jtab = $leftjoins[$tab];
						if (strpos($jtab,".")){
							$jtab = explode(".",$leftjoins[$tab])[0];
						}
						
						$tabAlias = $tab."|".$jtab;						
					}else{
						$tab = $table;
						$tabAlias = $table;
						$field = $field;
					}
					
					$placeholder = $tab."_".$field;
					
					if ($israw){
						$suff="";
						if (isset($vals[$placeholder])){
							$idx++;
							$suff = $idx;
						}
						$vals[$placeholder.$suff] = $value;
						$where.=$cm."`{$tabAlias}`.`{$field}` {$operand} :{$placeholder}{$suff}";
					}else{
						
						$where.=$cm."`{$tabAlias}`.`{$field}` {$operand} ".$value;
					}
				}else{
					if ($israw){
						$suff="";
						if (isset($vals[$field])){
							$idx++;
							$suff = $idx;
						}
						//$vals[$field] = $value;
						//$where.=$cm."{$field}` {$operand} :{$field}{$suff}";
						$where.=$cm."`{$field}` {$operand} {$value}";
						
					}else{
						$fxKey = $this->fixPlaceholder($key);
						$where.=$cm."`{$field}` {$operand} :".$fxKey;
						
						
						$vals[$fxKey] = $value;
					}
					
				}
				
				$isfirst=false;
			}
		}else{
			//object???
			$where = "";
		}
		
		

		if (count($opts)>0){
			$options=[];
			foreach($opts as $k=>$v){
				$options[trim(strtolower($k))] = $v;
			}
			
			if (isset($options['groupby'])){
				$where.= " GROUP BY ".$options['groupby'];
			}
			
			if (isset($options['orderby'])){
				$where.= " ORDER BY ";
				if (is_array($options['orderby'])){
					$cm="";
					foreach($options['orderby'] as $k=>$v){
						$where.= $cm."`{$table}`.`".$k."` ".strtoupper($v);
						$cm=",";
					}
				}else{
					$where.= "`{$table}`.".$options['orderby'];
				}
				
			}
			
			if (isset($options['limit'])){
				$where.= " LIMIT ".(int)$options['limit'];
				
				if (isset($options['offset'])){
					$where.= " OFFSET ".(int)$options['offset'];
				}
			}
		}
		
		
		$where =  str_replace("``","`",$where);
		
		return (object)[
			'where' =>$where,
			'vals'=>$vals,
		];
	}
	
	
	function add($table, $query){
		if (is_object($query)){
			$query = (object)$query;
		}
		
		$tbl = "`{$table}`";
		if ($this->db!=""){
			$tbl="`{$this->db}`.".$tbl;
		}
		
		$fields=[];
		$placeholders=[];	
		$values=[];	
		
		foreach($query as $field=>$val)
		{
			if (substr($field,-1,1)=="~"){ // RAW! TODO:allow only commands!!! now() ...
				//$sql.= $cm."`".trim(substr($key,0,-1))."`=".$val;
				$fields[] = trim(substr($field,0,-1));
				$placeholders[$field] = $val;
			}else{
				//$sql.= $cm."`{$key}`=:_".$key;
				//$values["_".$key] = $val;
				if (substr($field,-1,1)=="."){
					$field = substr($field,0,-1);
				}
				
				$fields[] = $field;
				$placeholders[] = ":".$field;
				$values[$field] = $val;
			}
			
			
		}
		
		$sql = "INSERT INTO {$tbl} (".implode(",",$fields).") VALUES(".implode(",",$placeholders).")";
		//print $sql;
		$sth = $this->pdo->prepare($sql);
		if ($sth->execute($values)){
			return (int)$this->pdo->lastInsertId();
		}else{
			$this->errmsg = $sth->errorInfo()[2]??"";
			return 0;
		}
		
	}
	
	
	function get($table, $query, $opts=[]){
		if ($query==""){
			return false;
		}else{
			$opts['limit'] = 1;
			$res = $this->find($table,$query,$opts);
			
			if (is_array($res)){
				return $res[0];
			}else{
				return $res->current();	
			}
			
		}
	}
	
	
	function find($table, $query=[], array $opts=[]){	
		$dereferencing = (int)($opts['dereferencing']??$this->autoDereferencing);
		
		$tbl = "`{$table}`";
		if ($this->db!=""){
			$tbl="`{$this->db}`.".$tbl;
		}
		
		$opts['_TABLE'] = $table;
		
		$columns = $opts['_COLUMNS']??"*";
		unset($opts['_COLUMNS']);
		
		$leftjoins = $opts['_LEFTJOIN']??[];
		
		if ($dereferencing>0 && count($leftjoins)==0){
			//print $table;
			$metas = $this->_meta($table);
			foreach($metas as $meta){
				//print "<pre>";	print_r($meta);		print "</pre>";
				//$comment = $meta['comment']??"";
				//if (substr($comment,0,1)=="$"){
				if (isset($meta['ref'])){
					//print $comment;
					
					//$leftjoins[$meta['field']] = trim(substr($comment,1));
					
					$ref = $meta['ref'];
					//print $ref."<BR>";
					if (substr($ref,0,2)=="$$"){// !!!
						//$ref = substr($ref,2);
					}elseif (substr($ref,0,1)=="$"){
						$ref = substr($ref,1);
						//$leftjoins[$meta['field']] = $ref;
					}else{
						$leftjoins[$meta['field']] = $ref;
					}
					
					
				}
			}
		}
		
		
		
		$q = $this->filterBuilder($query,$opts);
		
		
		//print $q->where."<BR>";
		
		unset($opts['_LEFTJOIN']);

		if (count($leftjoins)==0){
			$useJoins=false;
			$sql = "SELECT {$columns} FROM {$tbl} ".$q->where;
		}else{
			$useJoins=true;
			$jsql="";
			$cols="`{$table}`.*";
			foreach($leftjoins as $k=>$v){
				$tf = explode(".",$v);
				$jtable = $tf[0];
				$jfield = $tf[1]??"id";
				
				//$jtables[]=$jtable;
				$cols.=", `{$jtable}`.*";
				//print $v."<BR>";
				$jsql.= " \nLEFT JOIN `{$jtable}` as `{$k}|{$jtable}` ON `{$table}`.`{$k}`=`{$k}|{$jtable}`.`{$jfield}`";
			}
			$sql = "SELECT * FROM `{$table}`{$jsql} ".$q->where;
		}
		
		$meta=[];
		
		//TODO: OPTION BUFFERED	
		//$sth = $this->pdo->prepare($sql,[\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false]); //array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false)
		//$sth = $this->pdo->prepare($sql,[\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true]); //array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true)
	
		//print $sql."<BR>";
		//print_r($q->vals);
		
	
		$sth = $this->pdo->prepare($sql);
		$res = $sth->execute($q->vals);
		
		$colCount = $sth->columnCount();
		for($i=0; $i<$colCount; $i++){
		  $meta[] = $sth->getColumnMeta($i);
		}
		
		
		$result = [];
		if ($res!=false){
			if ($useJoins){
				$sth->setFetchMode(\PDO::FETCH_NUM);
			}else{
				$sth->setFetchMode(\PDO::FETCH_ASSOC);
			}
			
		
			
			while ( $row = $sth->fetch() ){
				
				//yield $row;
				if ($useJoins){
					$nrow=[];
					//$sub="";
					for($i=0; $i<$colCount; $i++){
						//print "~~";
						
						if ($meta[$i]['table']==$table){
							//print "+".$table;
							if (isset($leftjoins[$meta[$i]['name']]) && !isset($nrow[$meta[$i]['name']])){
								$nrow[$meta[$i]['name']] = [];
							}else{
								$nrow[$meta[$i]['name']] = $row[$i];
							}
						}else{
							//print "-".$meta[$i]['table'];
							if (strpos($meta[$i]['table'],"|")){
								$pr = explode("|",$meta[$i]['table'],2);
								
								$fsub = $pr[0]??"a";
								$ftab = $pr[1]??"a";
								//print $fsub."<BR>";
								$fname = $ftab.".".$meta[$i]['name'];
								$nrow[$fsub][$fname] = $row[$i];
							}
						}
						
					}

					//print "<pre>";
					//print_r($nrow);
					
					yield $nrow;
					//$result[] = $nrow;
					
				}else{
					
					yield $row;
					//$result[] = $row;
				}
				
			}
		}else{
			print "!!!";
		}
		
		//return $result;
	}
	
	
	function edit($table, $query, $data){
		//print_r($query);
		$tbl = "`{$table}`";
		if ($this->db!=""){
			$tbl="`{$this->db}`.".$tbl;
		}
		
		$opts['_TABLE'] = $table;
		
		$q = $this->filterBuilder($query,$opts);
		$values=$q->vals;	
	
		$sql = "UPDATE {$tbl} SET ";
		$cm="";
		foreach($data as $key=>$val){
			
			if (substr($key,-1,1)=="~"){ // RAW! TODO:allow only commands!!! now() ...
				$sql.= $cm."`".trim(substr($key,0,-1))."`=".$val;
			}else{
				$sql.= $cm."`{$key}`=:_".$key;
				$values["_".$key] = $val;
			}
			
			$cm=",";
		}
		
		
		
		$sql.= " ".$q->where;
		
		//print $sql;
		//print_r($values);
		$sth = $this->pdo->prepare($sql);
		
		
		
		$sth->execute($values);
		print $sth->errorInfo()[2]??"";
		return $sth->rowCount();
	}
	
	
	function kill($table, $query){
		$tbl = "`{$table}`";
		if ($this->db!=""){
			$tbl="`{$this->db}`.".$tbl;
		}
		
		
		//$query['_TABLE'] = $tbl;
		//print_r($query);
		$q = $this->filterBuilder($query,['_TABLE'=>$tbl]);
		$sth = $this->pdo->prepare("DELETE FROM {$tbl} ".$q->where);
		$sth->execute($q->vals);
		return $sth->rowCount(); 
	}
	
	
	function count($table, $query=[]){
		$tbl = "`{$table}`";
		if ($this->db!=""){
			$tbl="`{$this->db}`.".$tbl;
		}
		$q = $this->filterBuilder($query,[]);

		$sql = "SELECT COUNT(*) as `cnt` FROM {$tbl} ".$q->where;
		
		$sth = $this->pdo->prepare($sql);
		$res = $sth->execute($q->vals);
		$sth->setFetchMode(\PDO::FETCH_ASSOC);
		$row = $sth->fetch();
		return $row['cnt'];
	}
	
	
	function nativeType($type){
		$type = trim(strtolower($type));
		if (in_array($type,["string","password"])){
			return "varchar(250)";
		}
		elseif (in_array($type,["file","image","photo"])){
			return "varchar(254)";
		}
		elseif (in_array($type,["files","html"])){
			return "TEXT";
		}
		else{
			return $type;
		}
	}
	
	function create($table,$schema=null){
		//print "<pre>";
		//print_r($schema);
		//print "</pre>";
		
		
		//$sql = "CREATE TABLE `{$table}` (";
		$sql = "CREATE TABLE IF NOT EXISTS `{$table}` (";
		$keys="";
		$cm="";
		foreach($schema as $field=>$meta){
			$type = $this->nativeType($meta['type']);
					
			$default = strtolower($meta['default']??"");
			if ($default=="auto"){
				$type = "INTEGER";
			}
			
			$sql.= $cm."`{$field}` {$type}";
			
			$alreadyPK=false;
			if ($default!="")
			{
				if ($default=="autoincrement" || $default=="auto"){
					if ($this->driver=="sqlite"){
						$sql.= " PRIMARY KEY AUTOINCREMENT NOT NULL";	
						$alreadyPK=true;
					}else{
						$sql.= " NOT NULL AUTO_INCREMENT ";	
					}
				}else{
					$sql.= " DEFAULT '{$default}'";
				}
			}
			
			$key = strtolower($meta['key']??"");
			//print "<BR>".$key."<BR>";
			if ($key!="")
			{
				if ($key=="primary" || $key=="pk"){
					if (!$alreadyPK){
						$keys.= ", PRIMARY KEY (`{$field}`)";
					}
				}elseif($key=="unique"){
					if ($this->driver=="sqlite"){
						$keys.= ", UNIQUE(`{$field}`)";
					}else{
						$keys.= ", UNIQUE KEY `{$field}` (`{$field}`)";	
					}
					
				}elseif($key=="key"){
					$keys.= ", KEY `{$field}` (`{$field}`)";
				}
				
			}
			
			
			$cm=" , ";
		}
		
		$sql.=$keys.")";

		print "<HR>".$sql."<HR>";
		
		return $this->pdo->query($sql);
	}
	
	
	function list($table,$parent="",$opts=[]){
		
		if (isset($opts['hasParentField'])){
			$hasParentField = (bool)$opts['hasParentField'];
		}else{
			$hasParentField=false;
			$items = $this->_query("SHOW COLUMNS FROM `{$table}`");
			foreach($items as $item){
				if ($item['Field']=="_parent"){
					$hasParentField=true;
					break;
				}
			}
		}
		
		if ($parent==""){
			if ($hasParentField){
				return $this->find($table,[
					'_parent'=>0,
					'OR _parent'=>NULL,
					
				],$opts);
			}else{
				return $this->find($table,[],$opts);
			}
		}else{
			if ($hasParentField){
				return $this->find($table,[
					'_parent'=>$parent
				],$opts);
			}else{
				return null;
			}
		}
	}
	
	
	function index(){
		
		
		
		
		if ($this->driver=="mysql" || $this->driver=="mariadb"){
			$sql = "SELECT TABLE_NAME AS `name`,TABLE_TYPE AS `type`,TABLE_COMMENT AS `comment` FROM `information_schema`.`TABLES` WHERE `TABLE_SCHEMA`='{$this->db}'";
		}elseif ($this->driver=="sqlite"){
			$sql = "SELECT name FROM sqlite_master WHERE type ='table' AND name NOT LIKE 'sqlite_%';";
		}else{
			$sql = "SHOW TABLES";
		}
		
		//$sql = "SELECT * from test";
		
		$sth = $this->pdo->prepare($sql);
		$res = $sth->execute();
		$sth->setFetchMode(\PDO::FETCH_ASSOC);
		
		$arr=[];
		while ( $row = $sth->fetch() ){
			$row['options'] = [];
			$arr[$row['name']] = $row;
		}
		
		return $arr;
	}
	
	function _exec($command,array $data=[]){
		$sth = $this->pdo->prepare($command);
		$res = $sth->execute($data);
		
		$cmd = strtoupper(trim(explode(" ",ltrim($command),2)[0]));
		if($cmd=="INSERT"){
			return (int)$this->pdo->lastInsertId();
		}elseif($cmd=="UPDATE" || $cmd=="DELETE" ){
			return $sth->rowCount(); 
		}else{
			return;
		}
	}
	
	function _query($query,array $data=[]){
		$cmd = strtoupper(trim(explode(" ",ltrim($query),2)[0]));
		if ($cmd=="SELECT" || $cmd=="SHOW"){
			//print $query;
			
			$sth = $this->pdo->prepare($query);
			$res = $sth->execute($data);
			if ($res!=false){
				$sth->setFetchMode(\PDO::FETCH_ASSOC);
				while ( $row = $sth->fetch() ){
					foreach($row as $key=>$val)	{
						if (substr($key,-1,1)==".")	{
							$row[$key] = (array)json_decode($val);
						}
					}
					yield $row;
				}
			}
		}else{
			return;
		}
	}
	
	
	function parseMetaComment($c){
		$r=[];
		
		$items = explode(";",$c);
		foreach($items as $item){
			if (strpos($item,":")>0){
				$ab = explode(":",$item);
				$r[trim(strtolower($ab[0]))] = trim($ab[1]??"");
			}else{
				$item = trim($item);
				$pfx = substr($item,0,1);
				if ($pfx=="$"){
					$r['ref'] = substr($item,1);
				}
			}
		}
		
		return $r;
	}
	
	function _meta($table){
		
		if ($this->driver=="sqlite"){
			$items = $this->_query("PRAGMA table_info({$table})");
			foreach($items as $item){
				yield [
					'field' => $item['name'],
					'type' => $item['type'],
					'null' => $item['notnull']==1?"NO":"YES",
					'key' => $item['pk'],
					'default' => $item['dflt_value'],
					//'extra' => $item['Extra'],
					//'comment' => $item['Comment'],
				]+$this->parseMetaComment($item['Comment']);
			}
		}else{
			$items = $this->_query("SHOW FULL COLUMNS FROM `{$table}`");
			foreach($items as $item){
				yield [
					'field' => $item['Field'],
					'type' => $item['Type'],
					'null' => $item['Null'],
					'key' => $item['Key'],
					'default' => $item['Default'],
					'extra' => $item['Extra'],
					'comment' => $item['Comment'],
				]+$this->parseMetaComment($item['Comment']);
			}
		}
	}
	
	function lastError(){
		return $this->errmsg;
	}
	
}

?>