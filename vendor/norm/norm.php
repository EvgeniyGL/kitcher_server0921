<?php
namespace Norm;

use Norm\Utils;
use Norm\Table;
use Norm\Provider;

class Norm{
	private static $instance=null;
	private $dbconfig=[];
	private $providers=[];
	private $default="";

	private $mysqlmemProvider=null;
	
	private $tables;

	function __construct($config,$mech=null){
		//if (is_null(self::$instance)){
		//	self::$instance = $this;
		//}
		
		$this->dbconfig = $config['db'];
		$this->default = array_key_first($this->dbconfig);
		
		foreach($this->dbconfig as $prov=>$cfg){
			$drv = $cfg['driver']??"";
			if ( ($drv=='mysql' || $drv=="mariadb") && isset($cfg['memstorage']) ){
				$this->mysqlmemProvider = $this->db($prov);
			}
		}
		
		if ($mech){
			$this->getTables();
			//TODO: if Mech is defined - get tables info from schemes (may be with compilation)
		}else{
			$this->getTables();
		}
		
		
	}
	
	
	function mem($key,$val=null){
		if (!is_null($this->mysqlmemProvider)){
			$sqlcreate = "CREATE TABLE IF NOT EXISTS `norm_mem_kvs`(`key` VARCHAR(32) NOT NULL,`val` VARCHAR(4096) NOT NULL DEFAULT '',`timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP(),	PRIMARY KEY (`key`)) COLLATE='utf8mb4_unicode_ci' ENGINE=MEMORY MAX_ROWS=100;";
			$this->mysqlmemProvider->_exec($sqlcreate);
			
			if (is_null($val)){
				return $this->mem_get($key);
			}else{
				$this->mem_set($key,$val);
			}
		}
	}
	
	function mem_get($key){
		$res = $this->mysqlmemProvider->_query("SELECT * FROM `norm_mem_kvs` WHERE `key`='{$key}'");
		$row = $res->current();
		if (is_null($row)){
			return null;
		}else{
			return unserialize($row['val']);
		}
	}
	
	function mem_set($key,$val){
		
		
		if (is_null($val)){
			$sql = "DELETE FROM `norm_mem_kvs` WHERE `key`=:key";
			$this->mysqlmemProvider->_exec($sql,[
				'key'=>$key
			]);
		}else{
			$sqlcreate = "CREATE TABLE IF NOT EXISTS `norm_mem_kvs`(`key` VARCHAR(32) NOT NULL,`val` VARCHAR(4096) NOT NULL DEFAULT '',`timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP(),	PRIMARY KEY (`key`)) COLLATE='utf8mb4_unicode_ci' ENGINE=MEMORY MAX_ROWS=100;";
			$sql = "INSERT INTO `norm_mem_kvs`(`key`,`val`) VALUES('{$key}', :val) ON DUPLICATE KEY UPDATE `val`=:val,`timestamp`=CURRENT_TIMESTAMP()";
			$this->mysqlmemProvider->_exec($sqlcreate.$sql,[
				'val'=>serialize($val)
			]);
		}
	}
	
	
	function getTables(){
		$tables=null;
		
		if (!is_null($this->mysqlmemProvider)){
			$tables = $this->mem("db_indexes");
		}
		
		if (is_null($tables)){
			foreach($this->dbconfig as $db=>$cfg){
				
				$index = $this->db($db)->index();
				foreach($index as $tbl=>$meta){
					$tablName = strtolower($tbl);
					if (isset($tables[$tablName])){
						$tablName = $tablName."_".strtolower($cfg['driver']);
					}
					
					$meta['driver'] = $cfg['driver'];
					$meta['provider'] = $db;
					$tables[$tablName] = $meta;
				}
				
			}
		}
		
		if (!is_null($this->mysqlmemProvider)){
			$this->mem("db_indexes",$tables);
		}
				
		$this->tables = $tables;
	}
	
	function warmup0(){
		$indexes=[];
		
		
		$myprovider = $this->db("mysql");
		
		$provider = $this->db("mongo");
		

		$indexes["mysql"] = $myprovider->index();
		$indexes["mongo"] = $provider->index();
		
		foreach($this->dbconfig as $name=>$cfg){
			$provider = $this->db($name);
			$indexes[$name] = $provider->index();
		}
			
			
		
		return $indexes;
	}
	
	
	static function init($dbConfig){
		if (is_null(self::$instance)){
			self::$instance = new Norm($dbConfig);
		}
		
		return self::$instance;
	}

	
	function __get($arg){ // $nrm->sometable (with default database)
		$arg = strtolower($arg);
		
		if (isset($this->tables[$arg])){
			$provider = $this->tables[$arg]['provider'];
			return $this->db($provider)->$arg;
		}else{
			throw new \Exception("NO such table or collection in index: {$arg}");
			//die("NO such table or collection in index: {$arg}");
		}
	}
	
	function __invoke($arg){ // $nrm(somedb)->sometable (selected database)
		return $this->db($arg);
	}
	
	function __call($tableName,$args){ // $nrm->users("id","name","COUNT(*)");
		
		if (count($args)==0){
			$args[0]="*";
		}
		
		
		return $this->db($this->default)->$tableName($args);
	}
	
	function db($dbName){
		
		if (!isset($this->providers[$dbName])){
			
			if (isset($this->dbconfig[$dbName])){
				
				$this->providers[$dbName] = new Provider($this->dbconfig[$dbName]);

			}else{
				die("NO SUCH CONNECTION:".$dbName);
			}
		}
		
		return $this->providers[$dbName];
	}
	
	function index($dbName=""){
		if ($dbName==""){
			$dbName = $this->default;
		}
		return $this->db($dbName)->index();
	}
	
	function __toString(){
		return implode(",",array_keys($this->dbconfig));
	}
	
	function _install($scheme){
		//print "<pre>";		print_r($scheme);		print "</pre>";
		
		if (!isset($this->tables[$scheme['scheme']])){
			$mech = $scheme;
			//print "<pre>";		print_r($mech);		print "</pre>";
			
			$driver = $mech['driver']??$this->dbconfig[array_key_first($this->dbconfig)]['driver'];
			$schemeName = $mech['scheme']??"";
			$initialdata = $mech['initialdata']??"";
			$subtables = $mech['subtables']??[];
			
			
			//print $driver;
			
			if ($driver=="mongo"){
				
				$this->mem_set("db_indexes",null);
				$r = $this->db("mongo")->create($schemeName);
				if ($r==true && $initialdata!=""){
					if (is_array($initialdata)){
						foreach($initialdata as $data){
							$this->db("mongo")->$schemeName->add($data);
						}
					}else{
						$this->db("mongo")->$schemeName->add($initialdata);
					}
				}
			}elseif ($driver=="mysql"){
				
				$this->mem_set("db_indexes",null);
				$r = $this->db("mysql")->create($schemeName,$mech['fields']);
				if ($r){
					
					if ($initialdata!=""){
						if (is_array($initialdata)){
							foreach($initialdata as $data){
								$this->db("mysql")->$schemeName->add($data);
							}
						}else{
							$this->db("mysql")->$schemeName->add($initialdata);
						}
					}
					
					if (count($subtables)>0){
						foreach($subtables as $tbl=>$tmeta){
							$stblName = $schemeName."_".$tbl;
							
							/* print $stblName."<BR>";
							print "<pre>";
								print_r($tmeta);
							print "</pre>"; */
							
					
							
							$this->db("mysql")->create($stblName,$tmeta['fields']);
						}
					}
					
					
				}
			}elseif ($driver=="sqlite"){
				$r = $this->db($driver)->create($schemeName,$mech['fields']);
				if ($r){
					
					if ($initialdata!=""){
						if (is_array($initialdata)){
							foreach($initialdata as $data){
								$this->db($driver)->$schemeName->add($data);
							}
						}else{
							$this->db($driver)->$schemeName->add($initialdata);
						}
					}
					
					if (count($subtables)>0){
						foreach($subtables as $tbl=>$tmeta){
							$stblName = $schemeName."_".$tbl;
							
							
							/* print $stblName."<BR>";
							print "<pre>";
								print_r($tmeta);
							print "</pre>"; */
							
					
							
							$this->db($driver)->create($stblName,$tmeta['fields']);
						}
					}
					
					
				}
			}else{
				print "!!!!";
			}
		}
		
		
	}
}

?>