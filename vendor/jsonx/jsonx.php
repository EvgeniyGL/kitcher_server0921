<?php
namespace jsonx;

class JsonX{
	
	static function parse($json){
		$json = self::jsonFixNames($json);
		return json_decode($json,true);
	}
	
	static function fromFile($path){
		return self::parse(file_get_contents($path));
	}
	
	static function jsonFixNames($json){
		$json = preg_replace('/(\w+):/i', '"\1":', $json);
		$json = str_replace('$"','"$',$json);
		return $json;
	}
}

?>